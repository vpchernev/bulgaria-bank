﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bulgaria_Bank.Services.Data.Exceptions
{
    public class DuplicateEntityException : Exception
    {
        public DuplicateEntityException(string message) : base(message)
        {

        }
    }
}
