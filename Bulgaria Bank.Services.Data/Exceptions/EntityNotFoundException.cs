﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bulgaria_Bank.Services.Data.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException(string message) : base(message)
        {

        }
    }
}
