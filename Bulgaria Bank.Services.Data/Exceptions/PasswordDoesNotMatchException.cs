﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bulgaria_Bank.Services.Data.Exceptions
{
    public class PasswordDoesNotMatchException : Exception
    {
        public PasswordDoesNotMatchException(string message) : base(message)
        {

        }
    }
}
