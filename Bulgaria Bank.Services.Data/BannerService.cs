﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Services.Data.Contracts;
using Bulgaria_Bank.Services.Data.Exceptions;
using Bulgaria_Bank.Services.Data.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data
{
    public class BannerService : IBannerService
    {
        private readonly DatabaseContext _context;

        public BannerService(DatabaseContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
        
        public async Task<Banner> AddAsync(DateTime startDate, string path, string linkUrl, string bannerName, double daysToBeActive)
        {
            var yesterday = DateTime.Today.AddDays(-1);
            
            if (startDate == null)
            {
                throw new ArgumentNullException(Constants.StartDate ,Constants.Entity_Cannot_Be_Null_Form);
            }

            if (startDate <= yesterday)
            {
                throw new ArgumentException(Constants.Start_Date_Incorrect);
            }

            if (linkUrl == null)
            {
                throw new ArgumentNullException(String.Format(Constants.Entity_Cannot_Be_Null_Form,Constants.Link));
            }

            if (bannerName == null)
            {
                throw new ArgumentNullException(String.Format(Constants.BannerName , Constants.Entity_Cannot_Be_Null_Form));
            }

            if (await _context.Banners.AnyAsync(x=>x.BannerName == bannerName))
            {
                throw new DuplicateEntityException(String.Format(Constants.Entity_Already_Exists_Form,Constants.Banner));
            }

           
            
            var banner = new Banner
            {
                ImagePath=path,
                BannerName = bannerName,
                LinkUrl = linkUrl,
                StartDate = startDate,
                EndDate = startDate.AddDays(daysToBeActive),
                
            };

            await _context.Banners.AddAsync(banner);
            await _context.SaveChangesAsync();

            return banner;
        }

        public async Task<Banner> GetRandomAsync()
        {
            var todayDate = DateTime.Now;
            
            Random rand = new Random();
            int toSkip = rand.Next(0, await _context.Banners.CountAsync(x => x.EndDate > todayDate && x.StartDate<=todayDate));

            var banner = await _context.Banners.Where(x => x.EndDate > todayDate && x.StartDate<=todayDate).Skip(toSkip).Take(1).FirstOrDefaultAsync();

            return banner;
        }

        public async Task<Banner> RemoveAsync(Banner banner)
        {
            if(banner == null)
            {
                throw new ArgumentNullException(String.Format(Constants.Id_Cannot_Be_Null_Form,Constants.Banner));
            }

             _context.Remove(banner);
            await _context.SaveChangesAsync();

            return banner;
        }

        public async Task<string> SaveFileAsync(IFormFile file)
        {
            if (file == null || file.Length == 0)
            {
                throw new ArgumentNullException("File not selected.");
            }

            var path = Path.Combine(
                  Directory.GetCurrentDirectory(), "wwwroot/BannersImage",
                  file.FileName);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            return file.FileName;
        }

        public async Task<int> GetPageCount(int onePageBanners)
        {
            var count = await _context.Banners.CountAsync();

            var pages = (count - 1) / onePageBanners + 1;

            return pages;
        }

        public async Task<ICollection<Banner>> GetListByPageAsync(int currentPage)
        {

            List<Banner> banners;

            if (currentPage == 1)
            {
                banners = await _context
                                    .Banners
                                    .OrderBy(c => c.BannerName)
                                    .Take(5).ToListAsync();

            }
            else
            {
                banners = await _context
                                    .Banners
                                    .OrderBy(c => c.BannerName)
                                    .Skip((currentPage - 1) * 5)
                                    .Take(5).ToListAsync();
            }
            return banners;
        }

        public async Task<Banner> GetAsync (string bannerId)
        {
            if (bannerId == null)
            {
                throw new ArgumentNullException(String.Format(Constants.Entity_Cannot_Be_Null_Form, Constants.Banner_Id));
            }

            var banner = await _context.Banners.FirstOrDefaultAsync(b => b.Id == bannerId);

            if (banner == null)
            {
                throw new ArgumentNullException(String.Format(Constants.Entity_Not_Found_Form, Constants.Banner));
            }

            return banner;
        }
    }
}
