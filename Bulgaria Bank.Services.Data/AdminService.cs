﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models.Identity;
using Bulgaria_Bank.Services.Data.Contracts;
using Bulgaria_Bank.Services.Data.Exceptions;
using Bulgaria_Bank.Services.Data.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data
{
    public class AdminService : IAdminService
    {
        private readonly DatabaseContext _context;
        private readonly ICryptoWrapper _passwordHash;

        public AdminService(DatabaseContext context, ICryptoWrapper passwordHash)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _passwordHash = passwordHash ?? throw new ArgumentNullException(nameof(passwordHash));
        }

        public async Task<Admin> AuthenticateAsync(string userName, string password)
        {
            var admin = await _context.Admins.Include(r => r.Role).FirstOrDefaultAsync(a => a.Username == userName);

            if (admin == null)
            {
                throw new EntityNotFoundException(String.Format(Constants.Entity_Not_Found_Form, userName));
            }

            if (!_passwordHash.VerifyHashedPassword(admin?.PasswordHash, password))
            {
                throw new EntityNotFoundException(Constants.Wrong_Password);
            }

            return admin;
        }
    }
}
