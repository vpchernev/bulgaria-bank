﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Services.Data.Contracts;
using Bulgaria_Bank.Services.Data.Exceptions;
using Bulgaria_Bank.Services.Data.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data
{
    public class ClientService : IClientService
    {
        private DatabaseContext _context;

        public ClientService(DatabaseContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));

        }

        public async Task<Client> CreateAsync(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(String.Format(Constants.Entity_Cannot_Be_Null_Form,Constants.Name));
            }


            if (await _context.Clients.AnyAsync(c => c.Name == name))
            {
                throw new DuplicateEntityException(String.Format(Constants.Entity_Already_Exists_Form, name ));
            }

            var newClient = new Client
            {
                Name = name,
                CreatedOn = DateTime.Now
            };

            await _context.Clients.AddAsync(newClient);
            await _context.SaveChangesAsync();

            return newClient;

        }

        public async Task<IEnumerable<string>> ListAllAsync(Expression<Func<string, bool>> expression = null)
        {
            var output = await _context.Clients.Select(c => c.Name).Where(expression).ToListAsync();

            return output;
        }

        public async Task<Client> FindAsync(string clientId)
        {
            if(clientId==null)
            {
                throw new ArgumentNullException(String.Format(Constants.Id_Cannot_Be_Null_Form,Constants.Client));
            }

            var client = await _context.Clients.FirstOrDefaultAsync(x => x.Id == clientId);

            if(client==null)
            {
                throw new EntityNotFoundException(String.Format(Constants.Entity_Not_Found_Form,Constants.Client.ToLower()));
            }

            return client;
        }

        public async Task<Client> AddAccountAsync(Client client, Account account)
        {
            if (account == null)
            {
                throw new EntityNotFoundException(String.Format(Constants.Entity_Cannot_Be_Null_Form, Constants.Account));
            }

           
            if (client == null)
            {
                throw new EntityNotFoundException(String.Format(Constants.Entity_Cannot_Be_Null_Form, Constants.Client));
            }

            client.Accounts.Add(account);

            await _context.SaveChangesAsync();

            return client;
        }

        public async Task<Client> FindByNameAsync(string clientName)
        {
            if (clientName == null)
            {
                throw new ArgumentNullException(String.Format(Constants.Id_Cannot_Be_Null_Form, Constants.Client));
            }
            var client = await _context.Clients.Include(c => c.Accounts).FirstOrDefaultAsync(c => c.Name == clientName);

            if (client == null)
            {
                throw new EntityNotFoundException(String.Format(Constants.Entity_Not_Found_Form, Constants.Client.ToLower()));
            }

            return client;
        }

        public async Task<ICollection<Client>> ListAllAsync()
        {
            var clientList = await _context.Clients.ToListAsync();

            return clientList;
        }

        public async Task<int> GetPageCountAsync(int onePageClients)
        {
            var count = await _context.Clients.CountAsync();

            var pages = (count - 1) / onePageClients + 1;

            return pages;
        }

        public async Task<ICollection<Client>> GetClientsAsync(int currentPage)
        {

            List<Client> clients;

            if (currentPage == 1)
            {
                clients = await _context
                                    .Clients
                                    .Include(c => c.Accounts)
                                    .OrderBy(c => c.Name)
                                    .Take(5).ToListAsync();

            }
            else
            {
                clients = await _context
                                    .Clients
                                    .Include(c => c.Accounts)
                                    .OrderBy(c => c.Name)
                                    .Skip((currentPage - 1) * 5)
                                    .Take(5).ToListAsync();
            }
            return clients;
        }
    }
}
