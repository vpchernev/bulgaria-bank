﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Data.Models.Common;
using Bulgaria_Bank.Services.Data.Contracts;
using Bulgaria_Bank.Services.Data.Exceptions;
using Bulgaria_Bank.Services.Data.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data
{
    public class TransactionService : ITransactionService
    {
        private readonly DatabaseContext _context;

        public TransactionService(DatabaseContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<Transaction> SaveTransactionAsync(
                                                            Account senderAccount,
                                                            Account receiverAccount,
                                                            string description,
                                                            decimal amount)
        {
            var statusCode = TransactionStatus.Saved;

            var transaction = new Transaction
            {
                SenderId = senderAccount.Id,
                ReceiverId = receiverAccount.Id,
                CreatedOn = DateTime.Now,
                Description = description,
                Amount = amount,
                Status = statusCode
            };


            await _context.Transactions.AddAsync(transaction);
            await _context.SaveChangesAsync();

            return transaction;
        }

        public async Task<Transaction> SendSavedTransactionAsync(string id)
        {
            var savedTransaction = await GetAsync(id);

            var senderAccount = savedTransaction.SenderAccount;
            var receiverAccount = savedTransaction.ReceiverAccount;

            if (senderAccount.CurrentBalance < savedTransaction.Amount)
            {
                throw new ArgumentOutOfRangeException(Constants.Insufficient_Funds);
            }

            senderAccount.CurrentBalance = senderAccount.CurrentBalance - savedTransaction.Amount;
            receiverAccount.CurrentBalance = receiverAccount.CurrentBalance + savedTransaction.Amount;

            savedTransaction.Status = TransactionStatus.Sent;
            savedTransaction.CreatedOn = DateTime.Now;

            _context.Accounts.Update(senderAccount);
            _context.Accounts.Update(receiverAccount);
            _context.Transactions.Update(savedTransaction);

            await _context.SaveChangesAsync();

            return savedTransaction;
        }

        public async Task<Transaction> SendNewTransactionAsync(
                                                            Account senderAccount,
                                                            Account receiverAccount,
                                                            string description,
                                                            decimal amount)
        {
            if (description == null)
            {
                throw new ArgumentNullException(String.Format(Constants.Entity_Cannot_Be_Null_Form, Constants.Description));
            }

            if (amount <= 0)
            {
                throw new ArgumentOutOfRangeException("Amount cannot be 0 or negative.");
            }

            if (amount > senderAccount.CurrentBalance)
            {
                throw new ArgumentOutOfRangeException("Amount cannot exceed the current balance of the account.");
            }

            var statusCode = TransactionStatus.Sent;

            var transaction = new Transaction
            {
                SenderId = senderAccount.Id,
                SenderAccount = senderAccount,
                ReceiverId = receiverAccount.Id,
                ReceiverAccount = receiverAccount,
                CreatedOn = DateTime.Now,
                Description = description,
                Amount = amount,
                Status = statusCode
            };

            senderAccount.CurrentBalance = senderAccount.CurrentBalance - transaction.Amount;
            receiverAccount.CurrentBalance = receiverAccount.CurrentBalance + transaction.Amount;

            await _context.Transactions.AddAsync(transaction);
            _context.Accounts.Update(senderAccount);
            _context.Accounts.Update(receiverAccount);
            await _context.SaveChangesAsync();

            return transaction;
        }

        public async Task<Transaction> EditSavedTransactionAsync(Transaction transaction)
        {
            _context.Transactions.Update(transaction);
            await _context.SaveChangesAsync();

            return transaction;
        }

        public async Task<Transaction> GetAsync(string id)
        {
            if (id == null)
            {
                throw new ArgumentNullException(String.Format(Constants.Id_Cannot_Be_Null_Form, Constants.Transaction));
            }

            var transaction = await _context.Transactions
                                                        .Include(x => x.SenderAccount)
                                                          .ThenInclude(x => x.Client)
                                                        .Include(x => x.SenderAccount)
                                                            .ThenInclude(x => x.UserAccounts)
                                                        .Include(x => x.ReceiverAccount)
                                                          .ThenInclude(x => x.Client)
                                                        .FirstOrDefaultAsync(x => x.Id == id);

            if (transaction == null)
            {
                throw new EntityNotFoundException(String.Format(Constants.Entity_Not_Found_Form, Constants.Transaction.ToLower()));
            }

            return transaction;
        }

        private IQueryable<Transaction> GetReceivedByAccountIds(ICollection<string> accountIds)
        {

            return _context.Transactions
                    .Include(x => x.SenderAccount)
                        .ThenInclude(x => x.Client)
                    .Include(x => x.ReceiverAccount)
                        .ThenInclude(x => x.Client)
                    .Where(x => (accountIds.Contains(x.ReceiverId) && x.Status.ToString() != "Saved"))
                    .AsQueryable();
        }

        private IQueryable<Transaction> GetSentByAccountIds(ICollection<string> accountIds)
        {
            return _context.Transactions
                    .Include(x => x.SenderAccount)
                        .ThenInclude(x => x.Client)
                    .Include(x => x.ReceiverAccount)
                        .ThenInclude(x => x.Client)
                    .Where(x => accountIds.Contains(x.SenderId))
                    .AsQueryable();
        }
       
        public async Task<PaginatedList<Transaction>> GetTransactionsPagesAsync(
            ICollection<string> accountIds,
            int? currentPage,
            string sortOrder,
            string searchString,
            string currentFilter)
        {
            var receivedTransactions = GetReceivedByAccountIds(accountIds);
            var sentTransactions = GetSentByAccountIds(accountIds);

            var allTransactions = receivedTransactions.Concat(sentTransactions);

            if (!String.IsNullOrEmpty(searchString))
            {
                allTransactions = allTransactions.Where(t => t.Description.ToLower().Contains(searchString.ToLower())
                                       || t.Status.ToString().ToLower().Contains(searchString.ToLower())
                                       || t.ReceiverAccount.AccountNumber.ToString().Contains(searchString.ToLower())
                                       || t.SenderAccount.AccountNumber.ToString().Contains(searchString.ToLower())
                                       || t.Amount.ToString().Contains(searchString.ToLower()));
            }

            switch (sortOrder)
            {
                case "SenderAccountNumber_Desc":
                    allTransactions = allTransactions.OrderByDescending(t => t.SenderAccount.AccountNumber);
                    break;
                case "SenderAccountNumber":
                    allTransactions = allTransactions.OrderBy(t => t.SenderAccount.AccountNumber);
                    break;
                case "SenderClientName_Desc":
                    allTransactions = allTransactions.OrderByDescending(t => t.SenderAccount.Client.Name);
                    break;
                case "SenderClientName":
                    allTransactions = allTransactions.OrderBy(t => t.SenderAccount.Client.Name);
                    break;
                case "ReceiverAccountNumber_Desc":
                    allTransactions = allTransactions.OrderByDescending(t => t.ReceiverAccount.AccountNumber);
                    break;
                case "ReceiverAccountNumber":
                    allTransactions = allTransactions.OrderBy(t => t.ReceiverAccount.AccountNumber);
                    break;
                case "ReceiverClientName_Desc":
                    allTransactions = allTransactions.OrderByDescending(t => t.ReceiverAccount.Client.Name);
                    break;
                case "ReceiverClientName":
                    allTransactions = allTransactions.OrderBy(t => t.ReceiverAccount.Client.Name);
                    break;
                case "CreatedOn":
                    allTransactions = allTransactions.OrderBy(t => t.CreatedOn);
                    break;
                case "CreatedOn_Desc":
                    allTransactions = allTransactions.OrderByDescending(t => t.CreatedOn);
                    break;
                case "Status_Desc":
                    allTransactions = allTransactions.OrderByDescending(t => t.Status);
                    break;
                case "Status":
                    allTransactions = allTransactions.OrderBy(t => t.Status);
                    break;
                case "Amount_Desc":
                    allTransactions = allTransactions.OrderByDescending(t => t.Amount);
                    break;
                case "Amount":
                    allTransactions = allTransactions.OrderBy(t => t.Amount);
                    break;
                case "Description_Desc":
                    allTransactions = allTransactions.OrderBy(t => t.Description);
                    break;
                case "Description":
                    allTransactions = allTransactions.OrderByDescending(t => t.Description);
                    break;
                default:
                    allTransactions = allTransactions
                        .OrderByDescending(t => t.CreatedOn)
                        ;
                    break;
            }

            return await PaginatedList<Transaction>.CreateAsync(allTransactions, currentPage ?? 1, 5);
        }
    }
}
