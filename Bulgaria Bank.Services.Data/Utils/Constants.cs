﻿namespace Bulgaria_Bank.Services.Data.Utils
{
    public static class Constants
    {
        //Exception Messages
        public const string Id_Cannot_Be_Null_Form = "{0} Id cannot be null.";
        public const string Entity_Cannot_Be_Null_Form = "{0} cannot be null.";
        public const string Entity_Not_Found_Form = "Requested {0} does not exist.";
        public const string Entity_Already_Exists_Form = "{0} already exists.";
        public const string Entity_Incorect_Value = "{0} money start balance, cant be negative or bigger than decimal max value!";
        public const string Entity_Already_Exists_Form_In = "{0} already exists in this {1}.";
        public const string Entity_Does_Not_Exists_Form_In = "{0} does not exists in this {1}.";
        public const string File_Does_Not_Exist = "No such file.";
        public const string Insufficient_Funds = "Insufficient funds.";
        public const string Wrong_Password = "Entered password is invalid.";
        public const string Entity_Not_Equal = "{0} and {1} are not equal.";
        public const string Entity_Incorrect_Length = "{0} {1} incorrect length.";
        public const string Start_Date_Incorrect = "Start date cant be in the past.";




        //Entities and other keywords

        public const string Account = "Account";
        public const string Amount = "Amount";
        public const string Banner = "Banner";
        public const string BannerName = "Banner name";
        public const string Banner_Id = "Banner Id";
        public const string ConfirmPassword = "Confirm password";
        public const string Client = "Client";
        public const string User = "User";
        public const string FirstName = "First name";
        public const string LastName = "Last name";
        public const string Name = "Name";
        public const string Link = "Url";
        public const string Transaction = "Transaction";
        public const string Sender = "Sender";
        public const string StartDate = "Start date";
        public const string Receiver = "Receiver";
        public const string Description = "Description";
        public const string Username = "Username";
        public const string Password = "Password";
       
        

        public const int Client_Name_Min_Length = 2;
        public const int Client_Name_Max_Length = 35;

    }
}
