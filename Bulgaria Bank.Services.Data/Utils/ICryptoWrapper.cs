﻿namespace Bulgaria_Bank.Services.Data.Utils
{
    public interface ICryptoWrapper
    {
        string HashPassword(string password);

        bool VerifyHashedPassword(string hashedPassword, string password);
    }
}
