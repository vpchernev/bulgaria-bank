﻿using System.Web.Helpers;

namespace Bulgaria_Bank.Services.Data.Utils
{
    public class CryptoWrapper : ICryptoWrapper
    {
        public string HashPassword(string password)
        {
            return Crypto.HashPassword(password);
        }
        
        public bool VerifyHashedPassword(string hashedPassword, string password)
        {
             return Crypto.VerifyHashedPassword(hashedPassword,password);
        }
    }
}
