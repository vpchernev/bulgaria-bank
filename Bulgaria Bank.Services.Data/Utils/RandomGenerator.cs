﻿using System;
using System.Text;

namespace Bulgaria_Bank.Services.Data.Utils
{
    public static class RandomGenerator
    {
        public static string GenerateRandom()
        {
            var ran = new Random();
            var result = new StringBuilder();

            for(int i=0; i<=9; i++)
            {
                result.Append((char)('0' + ran.Next(10)));
            }

            return result.ToString();
        }
    }
}
