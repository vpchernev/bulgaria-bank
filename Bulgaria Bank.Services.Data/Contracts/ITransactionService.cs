﻿using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Services.Data.Utils;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Bulgaria_Bank.Services.Data.Contracts
{
    public interface ITransactionService
    {
        Task<Transaction> SaveTransactionAsync(
                                               Account senderAccount,
                                               Account receiverAccount,
                                               string description,
                                               decimal amount);

        Task<Transaction> SendSavedTransactionAsync(string id);

        Task<Transaction> SendNewTransactionAsync(
                                                  Account senderAccount,
                                                  Account receiverAccount,
                                                  string description,
                                                  decimal amount);

        Task<Transaction> EditSavedTransactionAsync(Transaction transaction);

        Task<Transaction> GetAsync(string id);

        Task<PaginatedList<Transaction>> GetTransactionsPagesAsync(
            ICollection<string> accountIds,
            int? currentPage,
            string sortOrder,
            string searchString,
            string currentFilter);
    }
}
