﻿using Bulgaria_Bank.Data.Models.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.Contracts
{
    public interface IAdminService
    {
        Task<Admin> AuthenticateAsync(string userName, string password);

    }
}
