﻿using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Data.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.Contracts
{
    public interface IAccountService
    {
        Task<Account> CreateAsync(Client client, decimal amount);

        Task<Account> GetAsync(string Id);

        Task<ICollection<Account>> GetListByUserAsync(string userId);

        int GetAccountPages(int onePageAccounts, int count);

        Task<int> GetUsersCountAsync(string accountId);

        Task<ICollection<Account>> GetListAsync(int currentPage, string accountName);

        Task<Account> GetByNumberAsync(string accountNumber);

        Task<Account> GetWithAllUsersAsync(string accountNumber);

        Task<Account> AttachUserAsync(Account account, User user);

        Task<Account> UnattachUserAsync(Account account, User user);

        Task<Account> GetWithAllTablesAsync(string accountNumber);

        Task<ICollection<Account>> GetAccountsAsync();

        Task<IEnumerable<string>> ListAllNumbersAsync(string clientName, Expression<Func<string, bool>> expression = null);
    }
}
