﻿using Bulgaria_Bank.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.Contracts
{
    public interface IClientService
    {
        Task<Client> CreateAsync(string name);

        Task<Client> FindAsync(string clientId);

        Task<Client> FindByNameAsync(string clientName);

        Task<Client> AddAccountAsync(Client client, Account account);

        Task<ICollection<Client>> ListAllAsync();

        Task<int> GetPageCountAsync(int onePageClients);

        Task<ICollection<Client>> GetClientsAsync(int currentPage);

        Task<IEnumerable<string>> ListAllAsync(Expression<Func<string, bool>> expression = null);
    }
}
