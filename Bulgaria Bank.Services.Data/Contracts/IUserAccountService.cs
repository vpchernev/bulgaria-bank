﻿using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Data.Models.Identity;
using Bulgaria_Bank.Services.Data.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.Contracts
{
    public interface IUserAccountService
    {
        Task<UserAccount> RenameAsync(UserAccount userAccount, string newName);

        Task<UserAccount> GetAsync(string userId, string accountId);

        Task<PaginatedList<UserAccount>> GetUserAccountsPagesAsync(
            string userId,
            int? currentPage,
            string sortOrder,
            string searchString,
            string currentFilter);
    }
}
