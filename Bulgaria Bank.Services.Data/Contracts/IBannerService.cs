﻿using Bulgaria_Bank.Data.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.Contracts
{
    public interface IBannerService
    {
        Task<Banner> AddAsync(DateTime startDate, string path, string linkUrl, string bannerName, double daysToBeActive);

        Task<Banner> GetRandomAsync();

        Task<Banner> RemoveAsync(Banner banner);

        Task<string> SaveFileAsync(IFormFile file);

        Task<int> GetPageCount(int onePageBanners);

        Task<ICollection<Banner>> GetListByPageAsync(int currentPage);

        Task<Banner> GetAsync(string bannerId);
    }
}
