﻿using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Data.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.Contracts
{
    public interface IUserService
    {
        Task<User> CreateAsync(string username, string password, string confirmPassword, string firstName, string lastname);

        Task<User> GetAsync(string userId);

        Task<User> AuthenticateAsync(string username, string passwordHash);

        Task<ICollection<User>> GetListByPageAsync(int currentPage, string accountId);

        int GetUserPages(int onePageClients, int count);

        Task<User> GetByUsernameAsync(string username);

        Task<IEnumerable<string>> ListAllavailableUsersAsync(string accountId, Expression<Func<string, bool>> expression = null);

    }
}
