﻿using Bulgaria_Bank.Data.Models.Identity;
using Microsoft.AspNetCore.Authentication;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.Contracts
{
    public interface IClaimService
    {
        List<Claim> CreateUserClaims(User user);

        List<Claim> CreateAdminClaims(Admin admin);

        ClaimsIdentity CreateClaimsIdentity(ICollection<Claim> claims);

        AuthenticationProperties CreateAuthenticationProperties();
    }
}
