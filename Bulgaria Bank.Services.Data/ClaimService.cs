﻿using Bulgaria_Bank.Data.Models.Identity;
using Bulgaria_Bank.Services.Data.Contracts;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data
{
    public class ClaimService : IClaimService
    {
        public List<Claim> CreateUserClaims(User user)
        {
            var claims = new List<Claim>
            {
                    new Claim(ClaimTypes.NameIdentifier, user.Id),
                    new Claim(ClaimTypes.Name, user.FirstName + " " + user.LastName),
                    new Claim(ClaimTypes.Role, "User")
            };

            return claims;
        }

        public List<Claim> CreateAdminClaims(Admin admin)
        {
            var claims = new List<Claim>
            {
                    new Claim(ClaimTypes.NameIdentifier, admin.Id),
                    new Claim(ClaimTypes.Name, admin.Username),
                    new Claim(ClaimTypes.Role, "Administrator")
            };

            return claims;
        }

        public ClaimsIdentity CreateClaimsIdentity(ICollection<Claim> claims)
        {
            var claimsIdentity = new ClaimsIdentity(
                    claims, CookieAuthenticationDefaults.AuthenticationScheme);

            return claimsIdentity;
        }

        public AuthenticationProperties CreateAuthenticationProperties()
        {
            var authProperty = new AuthenticationProperties()
            {
                IsPersistent = true,
                ExpiresUtc = DateTime.UtcNow.AddMinutes(20)
            };

            return authProperty;
        }
    }
}
