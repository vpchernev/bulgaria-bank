﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Services.Data.Contracts;
using Bulgaria_Bank.Services.Data.Exceptions;
using Bulgaria_Bank.Services.Data.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data
{
    public class UserAccountService : IUserAccountService
    {
        private DatabaseContext _context;

        public UserAccountService(DatabaseContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<UserAccount> RenameAsync(UserAccount userAccount, string newName)
        {
            if (newName == null)
            {
                throw new ArgumentNullException(String.Format(Constants.Entity_Cannot_Be_Null_Form, Constants.Name));
            }

            userAccount.Nickname = newName;

            _context.UserAccounts.Update(userAccount);
            await _context.SaveChangesAsync();

            return userAccount;
        }

        public async Task<UserAccount> GetAsync(string userId, string accountId)
        {
            var userAccount = await _context.UserAccounts
                                    .Include(x => x.Account)
                                    .Include(x => x.User)
                                    .FirstOrDefaultAsync(x => x.AccountId == accountId && x.UserId == userId);

            if (userAccount == null)
            {
                throw new EntityNotFoundException(String.Format(Constants.Entity_Not_Found_Form, Constants.Account.ToLower()));
            }

            return userAccount;
        }

        private IQueryable<UserAccount> GetAllAccountsForUserAsync(string userId)
        {
            var userAccount = _context.UserAccounts
                                    .Include(x => x.Account)
                                        .ThenInclude(x=>x.Client)
                                    .Include(x => x.User)
                                    .Where(x => x.UserId == userId)
                                    .AsQueryable();

            return userAccount;
        }
        
        public async Task<PaginatedList<UserAccount>> GetUserAccountsPagesAsync(
            string userId,
            int? currentPage,
            string sortOrder,
            string searchString,
            string currentFilter)
        {
            

            var userAccounts = GetAllAccountsForUserAsync(userId);

            if (!String.IsNullOrEmpty(searchString))
            {
                userAccounts = userAccounts.Where(t => t.Nickname.ToLower().Contains(searchString.ToLower())
                                       || t.Account.AccountNumber.ToString().ToLower().Contains(searchString.ToLower())
                                       || t.Account.CurrentBalance.ToString().Contains(searchString.ToLower()));
            }

            switch (sortOrder)
            {
                case "Nickname":
                    userAccounts = userAccounts.OrderBy(t => t.Nickname);
                    break;
                case "AccountNumber":
                    userAccounts = userAccounts.OrderBy(t => t.Account.AccountNumber);
                    break;
                case "AccountNumber_Desc":
                    userAccounts = userAccounts.OrderByDescending(t => t.Account.AccountNumber);
                    break;
                case "CurrentBalance":
                    userAccounts = userAccounts.OrderBy(t => t.Account.CurrentBalance).ThenBy(x => x.Nickname);
                    break;
                case "CurrentBalance_Desc":
                    userAccounts = userAccounts.OrderByDescending(t => t.Account.CurrentBalance).ThenBy(x => x.Nickname);
                    break;
                case "ClientName":
                    userAccounts = userAccounts.OrderBy(t => t.Account.Client.Name);
                    break;
                case "ClientName_Desc":
                    userAccounts = userAccounts.OrderByDescending(t => t.Account.Client.Name);
                    break;
                default:
                    userAccounts = userAccounts
                        .OrderByDescending(t => t.Nickname);
                    break;
            }

            return await PaginatedList<UserAccount>.CreateAsync(userAccounts, currentPage ?? 1, 5);
        }
    }
}
