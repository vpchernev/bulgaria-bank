﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Data.Models.Identity;
using Bulgaria_Bank.Services.Data.Contracts;
using Bulgaria_Bank.Services.Data.Exceptions;
using Bulgaria_Bank.Services.Data.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data
{
    public class AccountService : IAccountService
    {
        private DatabaseContext _context;

        public AccountService(DatabaseContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<Account> CreateAsync(Client client, decimal amount)
        {
            if (client == null)
            {
                throw new ArgumentNullException(String.Format(Constants.Id_Cannot_Be_Null_Form, Constants.Client));
            }

            if (amount < 0 || amount > decimal.MaxValue)
            {
                throw new ArgumentNullException(String.Format(Constants.Entity_Incorect_Value, Constants.Account));
            }

            string newAccountNumber;

            do
            {
                newAccountNumber = RandomGenerator.GenerateRandom();
            }
            while (await _context.Accounts.AnyAsync(x => x.AccountNumber == newAccountNumber));

            var account = new Account
            {
                AccountNumber = newAccountNumber,
                ClientId = client.Id,
                CurrentBalance = amount,
                CreatedOn = DateTime.Now
            };

            client.Accounts.Add(account);

            await _context.SaveChangesAsync();

            return account;
        }

        public async Task<ICollection<Account>> GetListByUserAsync(string userId)
        {
            var userAccountList = await _context.UserAccounts
                .Where(a => a.UserId == userId)
                .Select(a => a.Account)
                    .Include(x => x.UserAccounts)
                .ToListAsync();

            return userAccountList;
        }

        public int GetAccountPages(int onePageAccounts, int count)
        {

            var pages = (count - 1) / onePageAccounts + 1;

            return pages;
        }

        public async Task<ICollection<Account>> GetListAsync(int currentPage, string clientId)
        {
            List<Account> clients;

            if (currentPage == 1)
            {
                clients = await _context
                                    .Accounts
                                    .Include(a => a.UserAccounts)
                                    .Where(a => a.Client.Id == clientId)
                                    .OrderByDescending(c => c.CreatedOn)
                                    .Take(5).ToListAsync();

            }
            else
            {
                clients = await _context
                                    .Accounts
                                    .Include(a => a.UserAccounts)
                                    .Where(a => a.Client.Id == clientId)
                                    .OrderByDescending(c => c.CreatedOn)
                                    .Skip((currentPage - 1) * 5)
                                    .Take(5).ToListAsync();
            }
            return clients;
        }

        public async Task<Account> GetAsync(string id)
        {
            var account = await _context.Accounts
                .Include(x => x.UserAccounts)
                    .ThenInclude(ua => ua.User)
                .Include(x => x.ReceiverTransactions)
                    .ThenInclude(x => x.ReceiverAccount)
                .Include(x => x.Client)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (account == null)
            {
                throw new EntityNotFoundException(String.Format(Constants.Entity_Not_Found_Form, Constants.Account.ToLower()));
            }

            return account;
        }

        public async Task<Account> GetWithAllUsersAsync(string accountNumber)
        {
            var account = await _context.Accounts.Include(au => au.UserAccounts).FirstOrDefaultAsync(a => a.AccountNumber == accountNumber);

            if (account == null)
            {
                throw new EntityNotFoundException(String.Format(Constants.Entity_Not_Found_Form, Constants.Account.ToLower()));
            }

            return account;
        }

        public async Task<int> GetUsersCountAsync(string accountId)
        {
            var accountUsersCount = await _context.UserAccounts.Include(ua => ua.Account).Where(ua => ua.AccountId == accountId).CountAsync();

            return accountUsersCount;
        }

        public async Task<Account> GetByNumberAsync(string accountNumber)
        {
            var account = await _context.Accounts.FirstOrDefaultAsync(a => a.AccountNumber == accountNumber);

            if (account == null)
            {
                throw new EntityNotFoundException(String.Format(Constants.Entity_Not_Found_Form, Constants.Account.ToLower()));
            }

            return account;
        }

        public async Task<Account> AttachUserAsync(Account account, User user)
        {
            if (account == null)
            {
                throw new EntityNotFoundException(String.Format(Constants.Entity_Not_Found_Form, Constants.Account.ToLower()));
            }

            if (user == null)
            {
                throw new EntityNotFoundException(String.Format(Constants.Entity_Not_Found_Form, Constants.User.ToLower()));
            }

            if (account.UserAccounts.Any(a => a.User == user))
            {
                throw new DuplicateEntityException(String.Format(Constants.Entity_Already_Exists_Form_In, Constants.User.ToLower(), Constants.Account.ToLower()));
            }

            UserAccount accountUser = new UserAccount()
            {
                User = user,
                UserId = user.Id,
                Account = account,
                AccountId = account.Id,
                Nickname = account.AccountNumber
            };



            _context.UserAccounts.Add(accountUser);
            await _context.SaveChangesAsync();

            return (account);

        }

        public async Task<Account> UnattachUserAsync(Account account, User user)
        {
            if (account == null)
            {
                throw new EntityNotFoundException(String.Format(Constants.Entity_Not_Found_Form, Constants.Account.ToLower()));
            }

            if (user == null)
            {
                throw new EntityNotFoundException(String.Format(Constants.Entity_Not_Found_Form, Constants.User.ToLower()));
            }

            var userAccount = account.UserAccounts.FirstOrDefault(a => a.User == user);

            if (userAccount == null)
            {
                throw new EntityNotFoundException(String.Format(Constants.Entity_Does_Not_Exists_Form_In, Constants.User.ToLower(), Constants.Account.ToLower()));
            }

            _context.UserAccounts.Remove(userAccount);

            await _context.SaveChangesAsync();

            return account;

        }

        public async Task<Account> GetWithAllTablesAsync(string number)
        {
            var account = await _context.Accounts
                .Include(a => a.UserAccounts)
                    .ThenInclude(ua => ua.User)
                .Include(a => a.ReceiverTransactions)
                    .ThenInclude(t => t.ReceiverAccount)
                .Include(x => x.SenderTransactions)
                    .ThenInclude(t => t.SenderAccount)
                .Include(a => a.Client)
                .FirstOrDefaultAsync(a => a.AccountNumber == number);

            if (account == null)
            {
                throw new EntityNotFoundException(String.Format(Constants.Entity_Not_Found_Form, Constants.Account.ToLower()));
            }

            return account;
        }

        public async Task<IEnumerable<string>> ListAllNumbersAsync(string clientName, Expression<Func<string, bool>> expression = null)
        {

            var accountNumbers = await _context.Accounts.Where(a => a.Client.Name == clientName).Select(a => a.AccountNumber).Where(expression).ToListAsync();

            return accountNumbers;
        }

        public async Task<ICollection<Account>> GetAccountsAsync()
        {
            return await _context.Accounts.ToListAsync();
        }

       


    }
}
