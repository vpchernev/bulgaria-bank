﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Data.Models.Identity;
using Bulgaria_Bank.Services.Data.Contracts;
using Bulgaria_Bank.Services.Data.Exceptions;
using Bulgaria_Bank.Services.Data.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bulgaria_Bank.Data.Models.Common;
using System.Linq.Expressions;

namespace Bulgaria_Bank.Services.Data
{
    public class UserService : IUserService
    {
        private DatabaseContext _context;
        private ICryptoWrapper _hashPassword;

        public UserService(DatabaseContext context, ICryptoWrapper hashPassword)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _hashPassword = hashPassword ?? throw new ArgumentNullException(nameof(hashPassword));

        }

        public async Task<User> CreateAsync(string username, string password, string confirmPassword, string firstName, string lastname)
        {
            if (username == null)
            {
                throw new ArgumentNullException(String.Format(Constants.Entity_Cannot_Be_Null_Form,Constants.Username));
            }

            if (password == null)
            {
                throw new ArgumentNullException(String.Format(Constants.Entity_Cannot_Be_Null_Form, Constants.Password));
            }

            if (confirmPassword == null)
            {
                throw new ArgumentNullException(String.Format(Constants.Entity_Cannot_Be_Null_Form, Constants.ConfirmPassword));
            }

            if (firstName == null)
            {
                throw new ArgumentNullException(String.Format(Constants.Entity_Cannot_Be_Null_Form, Constants.FirstName));
            }

            if (firstName == null)
            {
                throw new ArgumentNullException(String.Format(Constants.Entity_Cannot_Be_Null_Form, Constants.LastName));
            }

            if (!password.Equals(confirmPassword))
            {
                throw new ArgumentException(String.Format(Constants.Entity_Not_Equal, Constants.Password, Constants.ConfirmPassword));
            }

            if (await _context.Users.AnyAsync(u => u.Username == username))
            {
                throw new DuplicateEntityException(username);
            }


            var hashedPassword = _hashPassword.HashPassword(password);

            var role = await _context.Roles.FirstOrDefaultAsync(r => r.Name == RoleType.User.ToString());

            var newUser = new User
            {
                Username = username,
                FirstName = firstName,
                LastName = lastname,
                PasswordHash = hashedPassword,
                CreatedOn = DateTime.Now,
                Role = role,
                RoleId = role.Id
            };

            _context.Users.Add(newUser);
            await _context.SaveChangesAsync();

            return newUser;
        }

        public async Task<User> GetAsync(string id)
        {
            if(id==null)
            {
                throw new ArgumentNullException(String.Format(Constants.Id_Cannot_Be_Null_Form,Constants.User));
            }
            
            User user = await _context.Users
                            .Include(u=>u.UserAccounts)
                                .ThenInclude(ua=>ua.Account)
                                    .ThenInclude(a=>a.Client)
                            .Include(u=>u.UserAccounts)
                                .ThenInclude(ua=>ua.Account)
                            .Include(u=>u.Role)
                            .FirstOrDefaultAsync(x => x.Id == id);

            if(user==null)
            {
                throw new EntityNotFoundException(String.Format(Constants.Entity_Not_Found_Form,Constants.User.ToLower()));
            }

            return user;
        }

        public async Task<User> GetByUsernameAsync(string username)
        {
            if (username == null)
            {
                throw new ArgumentNullException(String.Format(Constants.Entity_Cannot_Be_Null_Form, Constants.User));
            }

            User user = await _context.Users.FirstOrDefaultAsync(x => x.Username == username);

            if (user == null)
            {
                throw new EntityNotFoundException(String.Format(Constants.Entity_Not_Found_Form, Constants.User.ToLower()));
            }

            return user;
        }

        public async Task<User> AuthenticateAsync(string userName, string password)
        {
            if (userName == null)
            {
                throw new ArgumentNullException(String.Format(Constants.Entity_Cannot_Be_Null_Form, Constants.Username));
            }

            if (password == null)
            {
                throw new ArgumentNullException(String.Format(Constants.Entity_Cannot_Be_Null_Form, Constants.Password));
            }

            var user = await _context.Users.Include(r => r.Role).FirstOrDefaultAsync(a => a.Username == userName);

            if (user == null)
            {
                throw new EntityNotFoundException(String.Format(Constants.Entity_Not_Found_Form, Constants.Username.ToLower()));
            }

            if (!_hashPassword.VerifyHashedPassword(user.PasswordHash, password))
            {
                throw new PasswordDoesNotMatchException(Constants.Wrong_Password);
            }
      
            return user;
        }

        public int GetUserPages(int onePageClients, int count)
        {

            var pages = (count - 1) / onePageClients + 1;

            return pages;
        }

        public async Task<ICollection<User>> GetListByPageAsync(int currentPage, string accountId)
        {
            List<User> users;

            if (currentPage == 1)
            {
                users = await _context
                                    .UserAccounts
                                    .Include(a => a.Account)
                                    .Include(u => u.User)
                                    .Where(a => a.AccountId == accountId)
                                    .Select(a => a.User)
                                    .OrderByDescending(c => c.CreatedOn)
                                    .Take(5).ToListAsync();

            }
            else
            {
                users = await _context
                                    .UserAccounts
                                    .Include(a => a.Account)
                                    .Include(u => u.User)
                                    .Where(a => a.AccountId == accountId)
                                    .Select(a => a.User)
                                    .OrderByDescending(c => c.CreatedOn)
                                    .Skip((currentPage - 1) * 5)
                                    .Take(5).ToListAsync();
            }
            return users;
        }

        public async Task<IEnumerable<string>> ListAllavailableUsersAsync(string accountId, Expression<Func<string, bool>> expression = null)
        {

            var usersUnavailable = _context.UserAccounts.Where(ua => ua.AccountId == accountId).Select(u => u.User.Username);

            var user = _context.Users.Select(u => u.Username);

            var result = await user.Except(usersUnavailable).Where(expression).ToListAsync();

            return result;
        }
        


    }
}
