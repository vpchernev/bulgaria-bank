# ASP.NET Core MVC Final Project

# Specifications:

* Overview : This project is created for educational purposes as a final stage at __Telerik Academy__.

* Feature name : Bulgaria Bank

* Contributors : [Ventsislav Chernev](https://gitlab.com/vpchernev), [Stiliyan Kostadinov](https://gitlab.com/cidsan)

* The application is online payment portal, where users can access the accounts to which they have access rights with their account balances and make simple payments internally between the accounts registered in the system. 

# Technologies used:

* Core
    * ASP.NET Core 2.2 MVC
    * MS SQL Server
    * Entity Framework 2.2
    * Razor

* Testing
    * MSTest
    * MOQ
    
* Design
    * Bootstrap
    * Atlantis-Lite-Master (created by ThemeKita)

* Other
    * GIT
    * Crypto (System.Web.Helpers)
    * JQuery + Unobtrusive
    * ChartJS
    * Select2
    * AutoComplete
    
# Architecture:

The application is divided into 4 main sections:
    
* Business Services: Contains all the service logic. It consists of single assembly which contains different services for the business logic.

* Data: Contains all the information and configurations that is required for the communication with MS SQL database via Entity Framework. It is consisted of 2 assemblies:

    * Data assemlby: It contains all of the configurations and data to be seeded in the database.
    
    * Models assembly: It contains all the models used for the separate tables. They are used as information containers which are responsible for holding the holding the data received from the database.

* Presentation: Contains web configurations & functionalities responsible for handling all web communication. This component consists of only a single assembly (Web) which implements the core segements of the system such as startup, controllers, views & many more.

* Test: Contains tests for the business and presentation assemblies.

# Configuration:

* The system contains functionality responsible for dataseeding & automatic migration setup.

# Pages:

* Public
    * Login Page: The user's account can only be created via administrator, so the only public part visible is the login page, which consists of two sections - one for banner with hyperlink and one for login credentials.
![login](https://scontent-sof1-1.xx.fbcdn.net/v/t1.15752-9/64262830_1308346282662935_6093605604205527040_n.png?_nc_cat=110&_nc_eui2=AeEzwDjCmVsAvs1IF2MstmgoVFc-qrrkz9MaTV23mVtvTMwXS4TPFO-u1c9AmnAhxzURwT-rf_-yWufWX2hHsqmK7-GOBVvHL89i6n8447S-1Q&_nc_ht=scontent-sof1-1.xx&oh=bf06a7b3e38fc9807c22daf747335131&oe=5DC1564C)

* Private
    * Account Dashboard: After successful login, the user is redirected to the account dashboard, which is a table with information about the bank accounts of the user. Each account has 3 actions attached to it - Rename account, Make Transaction, View Transactions. The sidebar has the two pages, available for the user - Account Dashboard and the transaction page. The user may search for specific account within the table from the search above the it and may sort the accounts by their corresponding headers.
![dashboard](https://scontent-sof1-1.xx.fbcdn.net/v/t1.15752-9/64782313_2805442826170375_7698396202798153728_n.png?_nc_cat=106&_nc_eui2=AeGddjm1EQOM-hrOoq7mR_Clk8sIURfyGxMhSg3Gj_7ylSuYuhuao821j8_Ss8dnDz6WJA6q6Tqyybuiekz_Hpmv5LxDqI0kIHwCpL1g8jaYCA&_nc_ht=scontent-sof1-1.xx&oh=56595d488611fdb6b979ca09afc013d3&oe=5D8C9619)

    * Account Balances: If the user has 2 or more accounts, a new section will appear in the dashboard - a pie chart, which represents the balances of the available accounts of the user.
![dashboard](https://scontent-sof1-1.xx.fbcdn.net/v/t1.15752-9/64215574_416826549047023_103185506954641408_n.png?_nc_cat=106&_nc_eui2=AeGUeAiO-KFh871RipVO2Oxbzr2Rtfik-0gpBw2OO7a7rO5z2jkQNCCMhAKpR6v02zojL6cid3HiRKTpUi03W84nsRjaP8rBtvsG1e6WLdCxxA&_nc_ht=scontent-sof1-1.xx&oh=782722a73e2485b655097043f84d8706&oe=5D925A23)   

    * Rename Account: Brings a pop-up, allowing the user to rename their corresponding account
![rename](https://scontent-sof1-1.xx.fbcdn.net/v/t1.15752-9/64999111_393639384579472_8220982667751981056_n.png?_nc_cat=105&_nc_eui2=AeEubom_rWg6XcS-EDrp3Iig-pc630WjO37Pl3aLo4E2SfJYN86fNE2gvz-qqJHHxaB_mBGdmRu2x2J7gUk8ttcA23pO428PUWdlp3SdvswR8A&_nc_ht=scontent-sof1-1.xx&oh=50668917ce02ab4d2cf0ddc758e25be6&oe=5D7B8957)     
    
    * Make payment: When chosing this action, it will bring a pop-up, giving the user the option to send money or save transactions to be sent later. Sender account will be preloaded with the clicked on account, and the sender should type at least 2 numbers from the receiver account so all the available accounts, matching with the numbers entered will appear as drop-down so the user can browse from. If the transaction is saved, the amount will be reflected in both accounts. If the transaction is saved, it will appear in the transaction page with Status "Saved" and allows two actions.
![make payment](https://scontent-sof1-1.xx.fbcdn.net/v/t1.15752-9/64222654_2374868012593404_1653282759344390144_n.png?_nc_cat=110&_nc_eui2=AeE3sWWiqEbRBza1-P1WoBCIeNQ3_Yh2EmbwAzzWU1NauAU5a__9i8ajyXE1ELmCl8HbtsXiHij1eGcuk9eq4dJqRjeYo5ZPFguI2eiGOHNtOg&_nc_ht=scontent-sof1-1.xx&oh=5cd002e789caa8b017f46ff0aacbd675&oe=5D8ED856)         
   
    * View transactions: Brings you to the page of transactions for the chosen account. The dropdown will be preloaded with the same account. The user may search for transaction with the search option, may sort by clicking the headers of the table and make payment. The transactions indicate if it is incoming (coming to your account from another) or if it is outgoing (you send the money). If the transactions is saved, there will be two actions - "Edit" and "Send".
![transaction page](https://scontent-sof1-1.xx.fbcdn.net/v/t1.15752-9/64527153_605483459860485_4092207602262343680_n.png?_nc_cat=107&_nc_eui2=AeG6ubRhZzkrBQ_ZIufopqNTPSne89DoioiQc__Vpejle796A2FeMRbX7BiBrOhhgpbRJwr5-VjbovOKDFuD_a2nZUJXVPObN4gzFx58mNPHkQ&_nc_ht=scontent-sof1-1.xx&oh=2ad2cc09f49de9786d2e6acc0f379262&oe=5D8AE9F6) 
    
    * Edit transaction: If the user choses the "Edit" option from the saved transactions, it will bring a pop-up that allows the user to edit the transaction. The user may change any information before saving it back.
![edit](https://scontent-sof1-1.xx.fbcdn.net/v/t1.15752-9/64856776_375347086519833_2371211861703000064_n.png?_nc_cat=108&_nc_eui2=AeEcjb41265-yrHWCzTYS8XJKyKbmysibIZFLN_H-dD2V5j73bdGMJyFciklAccCM5EeK3WM_RFvLV3qXCbEjzfRNeDrl1O7lbsHuPB6MV_F-g&_nc_ht=scontent-sof1-1.xx&oh=a147358e38abaf5629112ffa9214567e&oe=5D8547DA)
   
    * Send transaction: A pop-up will appear, with no editable fields, waiting for confirmation from the user to be sent.
![send saved](https://scontent-sof1-1.xx.fbcdn.net/v/t1.15752-9/64313307_1132088633660568_2428300863354175488_n.png?_nc_cat=108&_nc_eui2=AeGth3J64nHL_wxiM5rhItfWB37mGOVpqmJsQyjEzjWpUG1FhftbFTxk4d99KdW57MWK1K15c3FHLvMcG5qSlKYEllZT5fwJkyHYskVD7Pyusw&_nc_ht=scontent-sof1-1.xx&oh=9c83621955c8fac889904f79401332d8&oe=5D8445D2) 

    * Make payment: If this button is clicked it will bring a pop-up with empty fields, allowing the user to send or save a new transaction.The first dropdown is populated with all the available accounts of the user, and the second dropdown will give the accounts available to be received after typing minimum of 2 digits of the receiver's account number.
![new transaction](https://scontent-sof1-1.xx.fbcdn.net/v/t1.15752-9/64575166_324067291860734_7110876919024844800_n.png?_nc_cat=106&_nc_eui2=AeFPrXO2ClpcUrUIaJm6WGTjSjP2i3sWYcLiyrgMNe82bGIyEoDEZNfsrcg-cd6OFccxFcGlRQBvUhZF4f0uldg_fZNu9MpTYMx-F5mnWrVePA&_nc_ht=scontent-sof1-1.xx&oh=65694d76228b2d5de2775e0690320b45&oe=5D89C6D1) 

* Administrative 
    * Login: The login page for the administrator is separate from the normal user's and can only be accessed via the url.
![admin login](https://scontent-sof1-1.xx.fbcdn.net/v/t1.15752-9/64706419_838578626508989_3413862404562354176_n.png?_nc_cat=110&_nc_eui2=AeG8ocfsMQ_Tm0nktYTVRnFpSAGSfxFwULxib3J11RR9q8GkGOt9YEvOaqXLFAy4DHhMk7L-8uiVWv-HdPG6MkosdZ0IkhLRsnvOOxxTvzXfig&_nc_ht=scontent-sof1-1.xx&oh=d9a6a83acedbcd527cb1b88463ccf3fc&oe=5D99200E) 

    * Homepage: The homepage of the admin is Client's dashboard - table representing all the clients in the system's database. The admin is able to go to the accounts of each client via the action button. The admin is able to search for specific existing account in the database via the search menu. The minimum amount of symbols to enter for the search to start working are 3. The sidebar consists of three sections - to see all clients in the database, see all banners in the database and create new clients/accounts/users/banners. Quick access buttons are available above the tables for creation of entities.
![clients](https://scontent-sof1-1.xx.fbcdn.net/v/t1.15752-9/64369125_476123736457170_3828434221593001984_n.png?_nc_cat=110&_nc_eui2=AeHZN4QLARnR-58aRWtkl2yK1o3klZXS7ByWOa6F8D6k4wV_fAiqa71VdioCK32PNmpomdoMsdeKgdk9nbRkkFTq6R_nHUpwogBUWyWTpllKGw&_nc_ht=scontent-sof1-1.xx&oh=ae4da2d3343cf3a552037e4b0ef953d7&oe=5D85BB85) 

    * Accounts: After clicking the action button of an account, the admin can see all of the accounts of the current client. Each account has an action button, leading to the users, currently using it. The admin may search for specific account via the search menu.
![accounts](https://scontent-sof1-1.xx.fbcdn.net/v/t1.15752-9/64248327_354276405288773_2329413300107870208_n.png?_nc_cat=107&_nc_eui2=AeGGpKaCg65eKZQrIJpG5nFkXAblevzBl3x6eoofhJLPjYsHx2-IvrtX_0on-vaKKvFeON7vtLyMQINLzyIhoW98XCM_G3UYsHzcR8P5-ab0ZA&_nc_ht=scontent-sof1-1.xx&oh=90f1237c355fdcb1f48a9ffd3e4bd677&oe=5DC4AF06) 

    * Users: If clicked on the action button, the page will be loaded with the users currently attached to the specified account. The admin may attach more users to the account, via the search menu above, or detach users from the account. Doing so, the unattached user won't be able to use nor see the transactions of the corresponding account
![users](https://scontent-sof1-1.xx.fbcdn.net/v/t1.15752-9/64563757_353034385378606_3645105299204866048_n.png?_nc_cat=111&_nc_eui2=AeEd8Dm5MekpgADjMK5yRU_Ba-QCwFueuvXwskE4ciyEoz7JF1ZmjDjB4bMlgCsjtGU5x5LHD6wPEcmAvO5TVPOlV4CT3jhUoV6i9Bxd716v5Q&_nc_ht=scontent-sof1-1.xx&oh=ec57529607d44c778de794d99a4cfadb&oe=5D81A61B) 

    * Banners: Show all banners allows the administrator to see a table of all banners currently stored in the database. The admin may search for specific banner or remove it from the database from the action button. If doing so, a pop-up will appear for confirmation.
![show banners](https://scontent-sof1-1.xx.fbcdn.net/v/t1.15752-9/64239974_2859940097413107_7935981432696995840_n.png?_nc_cat=101&_nc_eui2=AeFkj8eLEaK27fQ8Ro8zOu9_HUvdPvpwYCNiudMd4BTuq46v0DimKKMJqLiJHk65zDMW4GfaPX58ho_VQAA0e0Z6eg1OvJy-fL03asNuFfNPQA&_nc_ht=scontent-sof1-1.xx&oh=9714e84bed4e2ff52ad78c0730aa82f6&oe=5D8FE5B3) 
![remove banner](https://scontent-sof1-1.xx.fbcdn.net/v/t1.15752-9/64250816_2043428039300434_1231061785948192768_n.png?_nc_cat=110&_nc_eui2=AeHa_x6xPVw4HB2FWWtAeso3QI63Wv2eYCH0Sc6Aax1_sH9Pw82HCUiq6mHE3K5xwrLjBxMWpDIdvmYbXHqex8m9EEokYy9jzWG4nuO9duTJKw&_nc_ht=scontent-sof1-1.xx&oh=5539638616f75e58ba7d314ecb432ee3&oe=5D831A21) 

    * Add new client: This action gives the admin to create a new client in the database.
![add client](https://scontent-sof1-1.xx.fbcdn.net/v/t1.15752-9/64219024_1282624091878289_4486456312653152256_n.png?_nc_cat=104&_nc_eui2=AeGRkicC18oxGYw2Vr9HNATIYltEN2izznv5h4YnY1qU6BZHLlGN-GrAptCSth1RLZT-sShiFjHxYWsSx1LMfWAT09qqC1d9P6nQ3wo0tySI2w&_nc_ht=scontent-sof1-1.xx&oh=81152d42a3e0ef6667f7d87ccee3ec00&oe=5D83B645) 

    * Add new account: The admin may add account to currently registered client. A pop-up will appear, waiting for name of client and with default account balance of 10000. The "amount" field is editable.
![add new account](https://scontent-sof1-1.xx.fbcdn.net/v/t1.15752-9/64563839_906826706335314_9073726163557285888_n.png?_nc_cat=105&_nc_eui2=AeFFuHxipIIgVtxRCmUNuzHUuy9FDjVDyZ3-tPYd_wjfMovk7AbETbuRtNo825TA-COrC57xQAD9HQApFh5NaH49CisFeax_VF78jP_8CeCx2Q&_nc_ht=scontent-sof1-1.xx&oh=1d7e275dc941958c6e15c28c5c3a5897&oe=5D85265B) 

    * Add new banner: Adds new banner to the database. The admin should upload a file, should give it a name, a hyperlink, should enter start data and days to be active before adding the banner. The banner cannot be added with start date older than today.
![add banner](https://scontent-sof1-1.xx.fbcdn.net/v/t1.15752-9/64537930_1048204068712555_2494295939255107584_n.png?_nc_cat=108&_nc_eui2=AeHntD6zBLxReOPtT7hLdhMea5hTS1_R-J0nZzelkAVKu0Ub1RMYLI2waA4E8QOmmrJg-Px7piud0hQAjhwuVkVadGEDr98oPzGbmY6yrXo1Dg&_nc_ht=scontent-sof1-1.xx&oh=e7efeae02e86752fb42736468f700a4e&oe=5D90DFA6) 
    
    * Create user: When creating new user, the administrator should check and fill his First Name, Last Name and the user should add himself the username and password.
![rename](https://scontent-sof1-1.xx.fbcdn.net/v/t1.15752-9/64653383_874199766275316_6937820231263846400_n.png?_nc_cat=104&_nc_eui2=AeFwhpSXH7zIflzWc_VbyzmeBh4LvfEtOIXzw1flo061tyWmirSF96ETKae1MMnbe0vhocjvS1ZDCAzqbTZin3gb5KvDrUnXq59MW1ztd44m2Q&_nc_ht=scontent-sof1-1.xx&oh=2dc178c847c95124884c22edf32b48b3&oe=5DC64F78)