﻿using Bulgaria_Bank.Data.Context;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Configuration
{
    public static class Seeder
    {
        public static async Task SeedDataAsync(IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;

                try
                {
                    await services.GetRequiredService<DatabaseContext>().Database.EnsureCreatedAsync();
                }

                catch (Exception ex)
                {
                    var _logger = services.GetRequiredService<ILogger<Program>>();
                    _logger.LogError(ex, "An error occured while seeding the database.");
                }
            }
        }
    }
}
