﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Bulgaria_Bank.Areas.Identity.Models
{
    public class TransactionViewModel
    {
        public string Id { get; set; }

        public DateTime CreatedOn { get; set; }

        public string CurrentAccountId { get; set; }

        public string CurrentAccountNumber { get; set; }

        public string CurrentAccountNickname { get; set; }

        [Required(ErrorMessage = "Sender's account cannot be empty.")]
        public string SenderId { get; set; }

        public string SenderAccountNumber { get; set; }

        public string SenderClientName { get; set; }

        [Required(ErrorMessage = "Receiver's account field cannot be empty.")]
        public string ReceiverId { get; set; }

        public string ReceiverAccountNumber { get; set; }

        public string ReceiverClientName { get; set; }

        [Required(ErrorMessage = "Description field cannot be empty.")]
        [StringLength(35, MinimumLength = 1, ErrorMessage = "Description should be between 1 and 35 characters long.")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Amount field cannot be empty.")]
        [Range(0.1d, 15000d, ErrorMessage = "Minimum amount is 0,10 and maximum is 15 000,00 for one transaction.")]
        public decimal Amount { get; set; }

        public string Status { get; set; }

        public bool IsIncoming { get; set; }
    }
}
