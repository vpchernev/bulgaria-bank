﻿using System.ComponentModel.DataAnnotations;

namespace Bulgaria_Bank.Areas.Identity.Models
{
    public class UserAccountViewModel
    {

        public string AccountNumber { get; set; }

        public string AccountId { get; set; }

        public string CurrentBalance { get; set; }

        public string ClientName { get; set; }

        [Required(ErrorMessage = "Please enter nickname for account.")]
        public string Nickname { get; set; }
    }
}
