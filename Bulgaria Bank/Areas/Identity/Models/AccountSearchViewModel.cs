﻿using Bulgaria_Bank.Areas.Identity.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Areas.Identity.Models
{
    public class AccountSearchViewModel
    {
        public ICollection<AccountSearchOption> Results { get; set; }
    }
}
