﻿using System.ComponentModel.DataAnnotations;

namespace Bulgaria_Bank.Areas.Identity.Models
{
    public class RenameAccountViewModel
    {
        public string AccountId { get; set; }

        [Required(ErrorMessage = "Nickname cannot be empty.")]
        public string Nickname { get; set; }

    }
}
