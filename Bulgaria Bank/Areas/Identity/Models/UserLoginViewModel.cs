﻿using System.ComponentModel.DataAnnotations;

namespace Bulgaria_Bank.Areas.Identity.Models
{

    public class UserLoginViewModel
    {
        public string Id { get; set; }

        [Required(ErrorMessage ="Username is required.")]
        public string Username { get; set; }

        [Required(ErrorMessage ="Password is required.")]
        [DataType(DataType.Password)]
        public string PasswordHash { get; set; }

        public BannerViewModel Banner { get; set; }
    }
}
