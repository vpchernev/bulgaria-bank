﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Areas.Identity.Models
{
    public class CreateTransactionViewModel
    {
        [Required(ErrorMessage = "Sender's account field cannot be empty.")]
        public string SenderAccountNumber { get; set; }

        public string SenderNickname { get; set; }

        public string SenderClientName { get; set; }

        [Required(ErrorMessage = "Receiver's account field cannot be empty.")]
        public string ReceiverAccountNumber { get; set; }

        public string ReceiverClientName { get; set; }

        [Required(ErrorMessage = "Description field cannot be empty.")]
        [StringLength(35, MinimumLength = 1, ErrorMessage = "Description should be between 1 and 35 characters long.")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Amount field cannot be empty.")]
        [Range(0.1, 15000d, ErrorMessage = "Minimum amount is 0,10 and maximum is 15 000,00 for one transaction.")]
        public decimal Amount { get; set; }
    }
}
