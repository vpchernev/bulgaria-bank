﻿using Bulgaria_Bank.Services.Data.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Areas.Identity.Models
{
    public class PaginatedTransactionsViewModel
    {
        public PaginatedList<TransactionViewModel> PagedTransactions {get; set;}

        public string CurrentAccountId { get; set; }

        public string CurrentAccountNumber { get; set; }

        public string CurrentAccountNickname { get; set; }
    }
}
