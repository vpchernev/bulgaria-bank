﻿using System.ComponentModel.DataAnnotations;

namespace Bulgaria_Bank.Areas.Identity.Models
{
    public class BannerViewModel
    {
        public string ImagePath { get; set; }

        [Url]
        public string LinkUrl { get; set; }
    }
}
