﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Areas.Identity.Models
{
    public class TransactionsPerPageViewModel
    {
        public int? PreviousPage { get; set; }

        public int CurrentPage { get; set; }

        public int? NextPage { get; set; }

        public int TotalPages { get; set; }

        public ICollection<TransactionViewModel> TransactionsList { get; set; }
    }
}
