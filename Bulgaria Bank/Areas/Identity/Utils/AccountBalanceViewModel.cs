﻿namespace Bulgaria_Bank.Areas.Identity.Utils
{
    public class AccountBalanceViewModel
    {
        public string AccountNumber { get; set; }

        public decimal CurrentBalance { get; set; }

        public string Nickname { get; set; }
    }
}
