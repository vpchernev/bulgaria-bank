﻿namespace Bulgaria_Bank.Areas.Identity.Utils
{
    public class AccountSearchOption
    {
        public string Id { get; set; }

        public string Text { get; set; }
    }
}
