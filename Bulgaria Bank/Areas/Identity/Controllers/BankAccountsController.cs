﻿using AutoMapper;
using Bulgaria_Bank.Areas.Identity.Models;
using Bulgaria_Bank.Areas.Identity.Utils;
using Bulgaria_Bank.Services.Data.Contracts;
using Bulgaria_Bank.Services.Data.Exceptions;
using Bulgaria_Bank.Services.Data.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Areas.Identity.Controllers
{
    [Area("Identity")]
    [Route("[area]/[controller]/[action]")]
    [Authorize(Roles = "User")]
    public class BankAccountsController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IAccountService _accountService;
        private readonly ITransactionService _transactionService;
        private readonly IUserAccountService _userAccountService;
        private readonly IBannerService _bannerService;

        public BankAccountsController(IMapper mapper, IUserService userService, IAccountService accountService, ITransactionService transactionService, IUserAccountService userAccountService, IBannerService bannerService)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
            _accountService = accountService ?? throw new ArgumentNullException(nameof(accountService));
            _transactionService = transactionService ?? throw new ArgumentNullException(nameof(transactionService));
            _userAccountService = userAccountService ?? throw new ArgumentNullException(nameof(userAccountService));
            _bannerService = bannerService ?? throw new ArgumentNullException(nameof(bannerService));
        }

        [HttpGet]
        public async Task<IActionResult> Dashboard(
            int? currentPage,
            string sortOrder,
            string searchString,
            string currentFilter)
        {

            var viewModel = await GetDashboardPage(currentPage, sortOrder, searchString, currentFilter);

            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> DashboardPartial(
            int? currentPage,
            string sortOrder,
            string searchString,
            string currentFilter)
        {
            var viewModel = await GetDashboardPage(currentPage, sortOrder, searchString, currentFilter);

            return PartialView("_DashboardPartial", viewModel);
        }

        /*Method for Dashboard*/
        private async Task<PaginatedList<UserAccountViewModel>> GetDashboardPage(
            int? currentPage,
            string sortOrder,
            string searchString,
            string currentFilter)
        {
            ViewData["NicknameSortParam"] = String.IsNullOrEmpty(sortOrder) ? "Nickname" : "";
            ViewData["AccountNumberSortParam"] = sortOrder == "AccountNumber" ? "AccountNumber_Desc" : "AccountNumber";
            ViewData["CurrentBalanceSortParam"] = sortOrder == "CurrentBalance" ? "CurrentBalance_Desc" : "CurrentBalance";
            ViewData["ClientNameSortParam"] = sortOrder == "ClientName" ? "ClientName_Desc" : "ClientName";
            ViewData["CurrentSort"] = sortOrder;

            if (searchString != null)
            {
                currentPage = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;

            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            var userAccounts = await _userAccountService.GetUserAccountsPagesAsync(userId, currentPage, sortOrder, searchString, currentFilter);

            List<UserAccountViewModel> model = new List<UserAccountViewModel>();

            foreach (var item in userAccounts)
            {
                model.Add(_mapper.Map<UserAccountViewModel>(item));
            }

            var listViewModel = new PaginatedList<UserAccountViewModel>(model, userAccounts.Count, userAccounts.PageIndex, userAccounts.PageSize);

            return listViewModel;
        }

        [HttpGet]
        public async Task<IActionResult> RenameAccount(string accountId)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            try
            {
                var account = await _accountService.GetAsync(accountId);

                var userAccount = await _userAccountService.GetAsync(userId, accountId);

                var model = _mapper.Map<RenameAccountViewModel>(userAccount);

                return PartialView("_RenamePartial", model);
            }
            catch (EntityNotFoundException ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RenameAccount(RenameAccountViewModel viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

                    var userAccount = await _userAccountService.GetAsync(userId, viewModel.AccountId);

                    var oldNickname = userAccount.Nickname;

                    userAccount = await _userAccountService.RenameAsync(userAccount, viewModel.Nickname);

                    var model = _mapper.Map<UserAccountViewModel>(userAccount);

                    return Ok($"Account {oldNickname} renamed to {model.Nickname}.");
                }

                var errors = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0);

                var errorMessages = errors.SelectMany(x => x.Select(y => y.ErrorMessage));

                return BadRequest(errorMessages);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAccountBalances()
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            var availableAccounts = await _accountService.GetListByUserAsync(userId);

            var accountNumbers = availableAccounts.Select(x => x.AccountNumber);

            var balances = availableAccounts.Select(x => x.CurrentBalance);

            ICollection<AccountBalanceViewModel> accountBalances = availableAccounts
                                .Select(x => new AccountBalanceViewModel()
                                {
                                    AccountNumber = x.AccountNumber,
                                    CurrentBalance = x.CurrentBalance,
                                    Nickname = x.UserAccounts.FirstOrDefault(ua => ua.AccountId == x.Id && ua.UserId == userId)?.Nickname

                                }).ToList();



            return Json(accountBalances);
        }

        [HttpGet]
        public async Task<IActionResult> GetAvailableSenderAccounts(string term = null)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            var user = await _userService.GetAsync(userId);

            var availableAccounts = await _accountService.GetListByUserAsync(userId);

            var accountNumbers = availableAccounts
                 .Select(x => x.AccountNumber);

            if (!string.IsNullOrEmpty(term))
            {
                availableAccounts = availableAccounts
                 .Where(x => x.UserAccounts.FirstOrDefault().Nickname.Contains(term)).ToList();
            }

            var model = new AccountSearchViewModel()
            {
                Results = availableAccounts.Select(x => new AccountSearchOption
                {
                    Id = x.AccountNumber,
                    Text = x.UserAccounts.FirstOrDefault().Nickname
                }).ToList()
            };

            return Json(model);
        }

        [HttpGet]
        public async Task<IActionResult> GetAvailableUserAccounts(string term = null)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            var user = await _userService.GetAsync(userId);

            var availableAccounts = await _accountService.GetListByUserAsync(userId);

            if (!string.IsNullOrEmpty(term))
            {
                availableAccounts = availableAccounts
                 .Where(x => x.UserAccounts.FirstOrDefault().Nickname.Contains(term)).ToList();
            }

            var model = new AccountSearchViewModel()
            {
                Results = availableAccounts.Select(x => new AccountSearchOption
                {
                    Id = x.Id,
                    Text = x.UserAccounts.FirstOrDefault()?.Nickname
                }).ToList()
            };

            return Json(model);

        }

        [HttpGet]
        public async Task<IActionResult> GetAvailableReceiverAccounts(string term = null)
        {
            var availableAccounts = await _accountService.GetAccountsAsync();

            var accountNumbers = availableAccounts
                 .Select(x => x.AccountNumber);

            if (!string.IsNullOrEmpty(term))
            {
                accountNumbers = accountNumbers
                 .Where(x => x.Contains(term));
            }

            var model = new AccountSearchViewModel()
            {
                Results = accountNumbers.Select(x => new AccountSearchOption
                {
                    Id = x,
                    Text = x

                }).ToList()
            };

            return Json(model);

        }

        public async Task<IActionResult> GetClientName(string accountNumber)
        {
            var account = await _accountService.GetWithAllTablesAsync(accountNumber);

            return Json(account.Client.Name);
        }
    }
}