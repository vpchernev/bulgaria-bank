﻿using AutoMapper;
using Bulgaria_Bank.Areas.Identity.Models;
using Bulgaria_Bank.Data.Models.Identity;
using Bulgaria_Bank.Services.Data.Contracts;
using Bulgaria_Bank.Services.Data.Exceptions;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Areas.Identity.Controllers
{
    [Area("Identity")]
    [Route("[area]/[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly IUserService _userService;
        private readonly IClaimService _claimService;
        private readonly IBannerService _bannerService;
        private readonly IMapper _mapper;
        private readonly IMemoryCache _cache;
        private const string BannerCacheKey = "banner";


        public AccountController(IUserService userService, IClaimService claimService, IBannerService bannerService, IMapper mapper, IMemoryCache cache)
        {
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
            _claimService = claimService ?? throw new ArgumentNullException(nameof(claimService));
            _bannerService = bannerService ?? throw new ArgumentNullException(nameof(claimService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _cache = cache ?? throw new ArgumentNullException(nameof(cache));
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login()
        {
            if (!User.Identity.IsAuthenticated)
            {

                await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

                BannerViewModel cachedBanner;

                if (!_cache.TryGetValue(BannerCacheKey, out cachedBanner))
                {
                    var banner = await _bannerService.GetRandomAsync();

                    if (banner != null)
                    {
                        cachedBanner = _mapper.Map<BannerViewModel>(banner);
                    }

                    var cacheEntryOptions = new MemoryCacheEntryOptions()
                                            .SetSlidingExpiration(TimeSpan.FromSeconds(5));

                    _cache.Set(BannerCacheKey, cachedBanner, cacheEntryOptions);
                }

                var userVm = new UserLoginViewModel();
                
                userVm.Banner = cachedBanner;

                return View(userVm);
            }

            return RedirectToAction("Dashboard", "BankAccounts");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(UserLoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user;

                try
                {
                    user = await _userService.AuthenticateAsync(model.Username, model.PasswordHash);
                    if (user == null)
                    {
                        ViewData["NoUser"] = "User does not exist.";
                    }

                }
                catch (PasswordDoesNotMatchException)
                {
                    ViewData["WrongPassword"] = "Entered password is invalid.";
                    return View(model);
                }
                catch (Exception)
                {
                    ViewData["Exception"] = "Someting went wrong while logging in.";
                    return View(model);
                }



                var claims = _claimService.CreateUserClaims(user);

                var claimIdentity = _claimService.CreateClaimsIdentity(claims);

                var authProperties = _claimService.CreateAuthenticationProperties();

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                                                                      new ClaimsPrincipal(claimIdentity),
                                                                      authProperties);

                return RedirectToAction("Dashboard", "BankAccounts");
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToAction("Login", "Account");
        }
    }
}