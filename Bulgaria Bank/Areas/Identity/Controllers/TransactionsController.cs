﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Bulgaria_Bank.Areas.Identity.Models;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Services.Data.Contracts;
using Bulgaria_Bank.Services.Data.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Bulgaria_Bank.Areas.Identity.Controllers
{
    [Area("Identity")]
    [Route("[area]/[controller]/[action]")]
    [Authorize(Roles = "User")]
    public class TransactionsController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IAccountService _accountService;
        private readonly ITransactionService _transactionService;
        private readonly IUserAccountService _userAccountService;
        private readonly IBannerService _bannerService;

        public TransactionsController(IMapper mapper, IUserService userService, IAccountService accountService, ITransactionService transactionService, IUserAccountService userAccountService, IBannerService bannerService)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
            _accountService = accountService ?? throw new ArgumentNullException(nameof(accountService));
            _transactionService = transactionService ?? throw new ArgumentNullException(nameof(transactionService));
            _userAccountService = userAccountService ?? throw new ArgumentNullException(nameof(userAccountService));
            _bannerService = bannerService ?? throw new ArgumentNullException(nameof(bannerService));
        }

        [HttpGet]
        public async Task<IActionResult> Index(
            int? currentPage,
            string accountId,
            string sortOrder,
            string searchString,
            string currentFilter)
        {
            var viewModel = await GetTransactionPage(currentPage, accountId, sortOrder, searchString, currentFilter);

            return View(viewModel);

        }

        [HttpGet]
        public async Task<IActionResult> IndexPartial(
            int? currentPage,
            string accountId,
            string sortOrder,
            string searchString,
            string currentFilter)
        {
            var viewModel = await GetTransactionPage(currentPage, accountId, sortOrder, searchString, currentFilter);


            return PartialView("_IndexPartial", viewModel);

        }

        [HttpGet]
        public async Task<IActionResult> Edit(string transactionId)
        {
            var transaction = await _transactionService.GetAsync(transactionId);

            if (transaction.Status.ToString() != "Saved")
            {
                return BadRequest("Current transaction is not saved.");
            }

            var model = _mapper.Map<EditTransactionViewModel>(transaction);

            return PartialView("_EditTransactionPartial", model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EditTransactionViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var transaction = await _transactionService.GetAsync(model.Id);

                    var sender = await _accountService.GetWithAllTablesAsync(model.SenderAccountNumber);

                    var receiver = await _accountService.GetWithAllTablesAsync(model.ReceiverAccountNumber);

                    transaction.SenderId = sender.Id;
                    transaction.ReceiverId = receiver.Id;
                    transaction.Description = model.Description;
                    transaction.Amount = model.Amount;

                    var transactionToBeEditted = await _transactionService.EditSavedTransactionAsync(transaction);

                    return Ok($"Transaction editted.");
                }

                var errors = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0);

                var errorMessages = errors.SelectMany(x => x.Select(y => y.ErrorMessage));

                return BadRequest(errorMessages);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Save(string senderAccountNumber, string receiverAccountNumber, string description, decimal amount)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

                    if (userId == null)
                    {
                        throw new ArgumentNullException(String.Format(Constants.Id_Cannot_Be_Null_Form, Constants.User));
                    }

                    var user = await _userService.GetAsync(userId);

                    if (senderAccountNumber == null)
                    {
                        throw new ArgumentNullException(String.Format(Constants.Id_Cannot_Be_Null_Form, Constants.Account));
                    }

                    if (receiverAccountNumber == null)
                    {
                        throw new ArgumentNullException(String.Format(Constants.Id_Cannot_Be_Null_Form, Constants.Account));
                    }

                    if (description == null)
                    {
                        throw new ArgumentNullException(String.Format(Constants.Entity_Cannot_Be_Null_Form, Constants.Description));
                    }

                    if (amount <= 0)
                    {
                        throw new ArgumentOutOfRangeException(String.Format(Constants.Entity_Cannot_Be_Null_Form, Constants.Amount));
                    }

                    var sender = await _accountService.GetWithAllTablesAsync(senderAccountNumber);

                    var receiver = await _accountService.GetByNumberAsync(receiverAccountNumber);

                    var transactionToBeSaved = await _transactionService.SaveTransactionAsync(sender, receiver, description, amount);

                    var model = _mapper.Map<TransactionViewModel>(transactionToBeSaved);

                    return Ok("Transaction saved");
                }

                var errors = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0);

                var errorMessages = errors.SelectMany(x => x.Select(y => y.ErrorMessage));

                return BadRequest(errorMessages);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> MakePayment(string senderAccountNumber)
        {
            if (senderAccountNumber == null)
            {
                return PartialView("_PreloadedPaymentPartial");
            }

            var account = await _accountService.GetWithAllTablesAsync(senderAccountNumber);

            var model = _mapper.Map<CreateTransactionViewModel>(account);

            return PartialView("_PreloadedPaymentPartial", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> MakePayment(CreateTransactionViewModel vm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var senderAccount = await _accountService.GetWithAllTablesAsync(vm.SenderAccountNumber);

                    var receiverAccount = await _accountService.GetWithAllTablesAsync(vm.ReceiverAccountNumber);

                    var transaction = await _transactionService.SendNewTransactionAsync(senderAccount, receiverAccount, vm.Description, vm.Amount);


                    return Ok($"{vm.Amount} sent to \"{receiverAccount.AccountNumber}\" account.");
                }

                var errors = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0);

                var errorMessages = errors.SelectMany(x => x.Select(y => y.ErrorMessage));

                return BadRequest(errorMessages);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> SendSavedTransaction(string id)
        {
            var transaction = await _transactionService.GetAsync(id);

            var model = _mapper.Map<TransactionViewModel>(transaction);

            return PartialView("_SendSavedTransactionPartial", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SendSavedTransaction(TransactionViewModel model)
        {
            try
            {
                var transactionToBeSaved = await _transactionService.SendSavedTransactionAsync(model.Id);

                return Ok($"Transaction succesfully sent from {model.SenderAccountNumber} account to {model.ReceiverAccountNumber} for {model.Amount} amount.");

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private async Task<PaginatedTransactionsViewModel> GetTransactionPage(
            int? currentPage,
            string accountId,
            string sortOrder,
            string searchString,
            string currentFilter)
        {
            ViewData["SenderAccountNumberSortParam"] = sortOrder == "SenderAccountNumber" ? "SenderAccountNumber_Desc" : "SenderAccountNumber";
            ViewData["SenderClientNameSortParam"] = sortOrder == "SenderClientName" ? "SenderClientName_Desc" : "SenderClientName";
            ViewData["ReceiverAccountNumberSortParam"] = sortOrder == "ReceiverAccountNumber" ? "ReceiverAccountNumber_Desc" : "ReceiverAccountNumber";
            ViewData["ReceiverClientNameSortParam"] = sortOrder == "ReceiverClientName" ? "ReceiverClientName_Desc" : "ReceiverClientName";
            ViewData["AmountSortParam"] = sortOrder == "Amount" ? "Amount_Desc" : "Amount";
            ViewData["DescriptionSortParam"] = sortOrder == "Description" ? "Description_Desc" : "Description";
            ViewData["CreatedOnSortParam"] = sortOrder == "CreatedOn" ? "CreatedOn_Desc" : "CreatedOn";
            ViewData["StatusSortParam"] = sortOrder == "Status" ? "Status_Desc" : "Status";
            ViewData["CurrentSort"] = sortOrder;

            if (searchString != null)
            {
                currentPage = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;


            List<string> accountIds = new List<string>();
            Account currentAccount = new Account();


            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var user = await _userService.GetAsync(userId);
            var accountsOfUser = await _accountService.GetListByUserAsync(userId);

            if (accountId == null)
            {
                accountIds = accountsOfUser.Select(x => x.Id).ToList();
            }
            else
            {
                bool isAuthenticated = user.UserAccounts.Any(x => x.AccountId == accountId);

                if (!isAuthenticated)
                {
                    throw new ArgumentException("Not Authorized.");
                }

                accountIds.Add(accountId);
                currentAccount = await _accountService.GetAsync(accountId);
            }

            var userTransactions = await _transactionService.GetTransactionsPagesAsync(accountIds, currentPage, sortOrder, searchString, currentFilter);

            List<TransactionViewModel> model = new List<TransactionViewModel>();
            var hashedIds = new HashSet<string>();

            foreach (var item in userTransactions)
            {
                var mappedItem = _mapper.Map<TransactionViewModel>(item);

                if (accountIds.Contains(item.ReceiverId) && !hashedIds.Contains(item.Id))
                {
                    mappedItem.IsIncoming = true;
                }

                model.Add(mappedItem);
                hashedIds.Add(item.Id);
            }

            var listViewModel = new PaginatedList<TransactionViewModel>(model, userTransactions.Count, userTransactions.PageIndex, userTransactions.PageSize);

            var newViewModel = new PaginatedTransactionsViewModel()
            {
                PagedTransactions = listViewModel,
                CurrentAccountId = currentAccount.Id,
                CurrentAccountNumber = currentAccount.AccountNumber,
                CurrentAccountNickname = currentAccount
                            .UserAccounts
                            .FirstOrDefault(x => x.AccountId == accountId)
                            ?.Nickname
            };

            return newViewModel;
        }
    }
}