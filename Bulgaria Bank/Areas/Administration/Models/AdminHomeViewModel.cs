﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Bulgaria_Bank.Areas.Administration.Models
{
    public class AdminHomeViewModel
    {
        public ClientAddViewModel ClientAddViewModel { get; set; }

        public ClientViewModel ClientViewModel { get; set; }

        [Required]
        public ClientsListViewModel ClientListViewModel { get; set; }

        public AccountAddViewModel AccountAddViewModel { get; set; }

        public AccountViewModel AccountViewModel { get; set; }

        public AccountListViewModel AccountListViewModel { get; set; }

        public UserAddViewModel UserAddViewModel { get; set; }

        public UserViewModel UserViewModel {get; set;}

        public UserListViewModel UserListViewModel { get; set; }

        public BannerViewModel BannerViewModel { get; set; }

        public BannerListViewModel BannerListViewModel { get; set; }

        public int? PreviousPage { get; set; }

        public int CurrentPage { get; set; }

        public int? NextPage { get; set; }

        public int TotalPages { get; set; }

        public string ClientName { get; set; }
    }
}
