﻿using System.ComponentModel.DataAnnotations;

namespace Bulgaria_Bank.Areas.Administration.Models
{
    public class ClientAddViewModel
    {
        [Required]
        [MinLength(3, ErrorMessage ="Client name cant be shorter than 3 symbols")]
        [MaxLength(35, ErrorMessage = "Client name cant be bigger than 3 symbols")]
        public string Name { get; set; }
    }
}
