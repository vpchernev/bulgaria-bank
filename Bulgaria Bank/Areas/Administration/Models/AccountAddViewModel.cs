﻿using System.ComponentModel.DataAnnotations;

namespace Bulgaria_Bank.Areas.Administration.Models
{
    public class AccountAddViewModel
    {

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Ballance must be positive number")]
        public decimal CurrentBalance { get; set; }

        [Required]
        [MinLength(3, ErrorMessage = "Client name cant be shorter than 3 symbols")]
        [MaxLength(35, ErrorMessage = "Client name cant be bigger than 3 symbols")]
        public string Name { get; set; }
    }
}
