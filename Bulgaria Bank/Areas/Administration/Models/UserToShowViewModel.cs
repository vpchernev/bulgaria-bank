﻿namespace Bulgaria_Bank.Areas.Administration.Models
{
    public class UserToShowViewModel
    {
        public string Username { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
