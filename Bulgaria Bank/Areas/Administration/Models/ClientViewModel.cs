﻿using System.ComponentModel.DataAnnotations;

namespace Bulgaria_Bank.Areas.Administration.Models
{
    public class ClientViewModel
    {
        public string Id { get; set; }

        [Required]
        [StringLength(35)]
        public string Name { get; set; }

        public string AcountsCount { get; set; }

        public string TotalAmount { get; set; }
        
    }
}
