﻿
using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace Bulgaria_Bank.Areas.Administration.Models
{
    public class BannerViewModel
    {
        public string Id { get; set; }

        [Required]
        public string Path { get; set; }

        [Required]
        [Url]
        public string LinkUrl { get; set; }
        [Required]
        [Range(0, 31, ErrorMessage = "Day numbers must be positive and less than 32")]
        public double DaysToBeActive { get; set; }

        public IFormFile BannerFile { get; set; }

        [Required]
        [MinLength(3, ErrorMessage = "Banner name cant be shorter than 3 symbols")]
        [MaxLength(35, ErrorMessage = "Banner name cant be bigger than 3 symbols")]
        public string BannerName { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }
    }
}
