﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Areas.Administration.Models
{
    public class UserListViewModel
    {
        public int? PreviousPage { get; set; }

        public int CurrentPage { get; set; }

        public int? NextPage { get; set; }

        public int TotalPages { get; set; }

        public IList<UserToShowViewModel> UsersList { get; set; }

        public AccountViewModel AccountViewModel { get; set; }

    }
}
