﻿using System.Collections.Generic;

namespace Bulgaria_Bank.Areas.Administration.Models
{
    public class ClientsListViewModel
    {
            public int? PreviousPage { get; set; }

            public int CurrentPage { get; set; }

            public int? NextPage { get; set; }

            public int TotalPages { get; set; }

            public IList<ClientViewModel> ClientList { get; set; }
    }
}
