﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Bulgaria_Bank.Areas.Administration.Models
{
    public class AccountViewModel
    {
        public string Id { get; set; }

        [StringLength(10)]
        public string AccountNumber { get; set; }

        [Required]
        [Range(0,double.MaxValue, ErrorMessage ="Ballance must be positive number")]
        public decimal CurrentBalance { get; set; }


        [Required]
        public string Client { get; set; }

        public int Users { get; set; }

        public ICollection<UserViewModel> UsersList { get; set; }
    }
}
