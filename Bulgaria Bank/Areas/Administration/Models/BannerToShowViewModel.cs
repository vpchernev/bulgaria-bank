﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Areas.Administration.Models
{
    public class BannerToShowViewModel
    {
        
        public string Id { get; set; }

        [Required]
        [StringLength(35)]
        public string BannerName { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/mm/yyyy}")]
        public DateTime StartDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/mm/yyyy}")]
        public DateTime EndDate { get; set; }
    }
}

