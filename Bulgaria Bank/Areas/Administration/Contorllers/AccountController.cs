﻿using Bulgaria_Bank.Areas.Administration.Models;
using Bulgaria_Bank.Services.Data.Contracts;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Areas.Administration.Contorllers
{

    [Area("Administration")]
    [Route("[area]/[controller]/[action]")]
    
    public class AccountController : Controller
    {

        private readonly IAdminService _adminService;
        private readonly IClaimService _claimService;

        public AccountController(IAdminService adminService, IClaimService claimService)
        {
            _adminService = adminService ?? throw new ArgumentNullException(nameof(adminService));
            _claimService = claimService ?? throw new ArgumentNullException(nameof(claimService));
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(AdminLoginModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            try
            {
                var admin = await _adminService.AuthenticateAsync(model.UserName, model.PasswordHash);

                var claims = _claimService.CreateAdminClaims(admin);

                var claimIdentity = _claimService.CreateClaimsIdentity(claims);

                var authProperties = _claimService.CreateAuthenticationProperties();

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                                                                      new ClaimsPrincipal(claimIdentity),
                                                                      authProperties);

                return RedirectToAction(
                    actionName: "Index",
                    controllerName: "Home");

            }
            catch (Exception ex)
            {

                BadRequest(ex.Message);
                return View(model);
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Account");
        }
    }
}