﻿using AutoMapper;
using Bulgaria_Bank.Areas.Administration.Models;
using Bulgaria_Bank.Areas.Administration.Utils;
using Bulgaria_Bank.Services.Data.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Areas.Administration.Contorllers
{
    [Area("Administration")]
    [Route("[area]/[controller]/[action]")]
    [Authorize(Roles = "Administrator")]
    public class BannersController : Controller
    {
        private IBannerService _bannerService;
        private IMapper _mapper;

        public BannersController(IBannerService bannerService, IMapper mapper)
        {
            _bannerService = bannerService ?? throw new ArgumentNullException(nameof(bannerService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddBanner(BannerViewModel model)
        {
            try
            {
                var path = await _bannerService.SaveFileAsync(model.BannerFile);
                var banner = await _bannerService.AddAsync(model.StartDate, path, model.LinkUrl, model.BannerName, model.DaysToBeActive);

                return Ok($"Succesfully added banner {model.BannerName} active for: {model.DaysToBeActive} days!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        public async Task<IActionResult> GetBannersList(int? currentPage)
        {
            try
            {

                var page = currentPage ?? 1;

                var allPages = await _bannerService.GetPageCount(5);

                var banners = await _bannerService.GetListByPageAsync(page);

                var banerListVM = new BannerListViewModel()
                {
                    BannersList = banners.Select(_mapper.Map<BannerToShowViewModel>).ToList(),
                    CurrentPage = page,
                    TotalPages = allPages
                };

                if (allPages > page)
                {
                    banerListVM.NextPage = page + 1;
                }

                if (currentPage > 1)
                {
                    banerListVM.PreviousPage = page - 1;
                }

                return PartialView("_BannerTablePartial", banerListVM);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpGet]
        public async Task<IActionResult> RemoveBanner(string bannerId)
        {
            try
            {
                var banner = await _bannerService.GetAsync(bannerId);

                banner = await _bannerService.RemoveAsync(banner);
                
                return Ok($"Succesfully remove banner {banner.BannerName}!");
                
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

    }
}