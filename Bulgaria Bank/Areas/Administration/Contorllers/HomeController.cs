﻿using AutoMapper;
using Bulgaria_Bank.Areas.Administration.Models;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Services.Data.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Bulgaria_Bank.Areas.Administration.Contorllers
{
    [Area("Administration")]
    [Route("[area]/[controller]/[action]")]
    [Authorize(Roles = "Administrator")]
    public class HomeController : Controller
    {
        private readonly IClientService _clientService;
        private readonly IMapper _mapper;
        private readonly IAccountService _accountService;
        private readonly IUserService _userService;

        public HomeController(IClientService clientService, IMapper mapper, IAccountService accountService, IUserService userService)
        {
            _clientService = clientService ?? throw new ArgumentNullException(nameof(clientService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _accountService = accountService ?? throw new ArgumentNullException(nameof(accountService));
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
        }

        public async Task<IActionResult> Index(int? currentPage)
        {
            try
            {

                var page = currentPage ?? 1;

                var allPages = await _clientService.GetPageCountAsync(5);

                var clients = await _clientService.GetClientsAsync(page);

                var vm = clients.Select(_mapper.Map<ClientViewModel>).ToList();

                var clientList = new ClientsListViewModel() { ClientList = vm, CurrentPage = page, TotalPages = allPages };

                var adminHome = new AdminHomeViewModel()
                {
                    ClientListViewModel = clientList,
                };

                if (allPages > page)
                {
                    adminHome.ClientListViewModel.NextPage = page + 1;
                }

                if (currentPage > 1)
                {
                    adminHome.ClientListViewModel.PreviousPage = page - 1;
                }

                return View(adminHome);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }


        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateClient(ClientAddViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Please submit valid form!");
            }

            try
            {
                var newClient = await _clientService.CreateAsync(vm.Name);

                return Ok($"Succesfully added {newClient.Name}!");


            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);

            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateClientAccount(AccountAddViewModel vm)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest("Please submit valid form!");
            }

            try
            {

                var client = await _clientService.FindByNameAsync(vm.Name);

                var account = await _accountService.CreateAsync(client, vm.CurrentBalance);

                return Ok($"Succesfully added account {account.AccountNumber} to Client: {client.Name} !");

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpGet]
        public async Task<IActionResult> GetAccounts(int? page, string clientName)
        {
            try
            {

                Client client = await _clientService.FindByNameAsync(clientName);

                int currentPage = page ?? 1;

                var allPages = _accountService.GetAccountPages(5, client.Accounts.Count());

                var accounts = await _accountService.GetListAsync(currentPage, client.Id);

                var accountListVM = new AccountListViewModel()
                {
                    AccountList = accounts.Select(_mapper.Map<AccountViewModel>).ToList(),
                    CurrentPage = currentPage,
                    TotalPages = allPages,
                    ClientName = client.Name
                };

                if (allPages > currentPage)
                {
                    accountListVM.NextPage = currentPage + 1;
                }

                if (currentPage > 1)
                {
                    accountListVM.PreviousPage = currentPage - 1;
                }


                return PartialView("_ClientAccountTablePartial", accountListVM);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpGet]
        public async Task<IActionResult> GetClients(int? currentPage)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }

                var page = currentPage ?? 1;

                var allPages = await _clientService.GetPageCountAsync(5);

                var clients = await _clientService.GetClientsAsync(page);

                var clientListVM = new ClientsListViewModel()
                {
                    ClientList = clients.Select(_mapper.Map<ClientViewModel>).ToList(),
                    CurrentPage = page,
                    TotalPages = allPages
                };

                if (allPages > page)
                {
                    clientListVM.NextPage = page + 1;
                }

                if (currentPage > 1)
                {
                    clientListVM.PreviousPage = page - 1;
                }

                return PartialView("_ClientsTablePartial", clientListVM);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateUser(UserAddViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Please submit valid form!");
            }

            try
            {

                var user = await _userService.CreateAsync(vm.Username, vm.Password,
                                                    vm.ConfirmPassword, vm.FirstName,
                                                    vm.LastName);

                return Ok($"Succesfully created user {vm.Username}!");
            }

            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        public async Task<IActionResult> ListUsers(int? currentPage, string accountName)
        {
            try
            {
                var page = currentPage ?? 1;

                var account = await _accountService.GetByNumberAsync(accountName);

                var allUserInAccount = await _accountService.GetUsersCountAsync(account.Id);

                var allPages = _userService.GetUserPages(5, allUserInAccount);

                var users = await _userService.GetListByPageAsync(page, account.Id);



                var userListVM = new UserListViewModel()
                {
                    AccountViewModel = _mapper.Map<AccountViewModel>(account),
                    UsersList = users.Select(_mapper.Map<UserToShowViewModel>).ToList(),
                    CurrentPage = page,
                    TotalPages = allPages

                };

                if (allPages > page)
                {
                    userListVM.NextPage = page + 1;
                }

                if (currentPage > 1)
                {
                    userListVM.PreviousPage = page - 1;
                }

                return PartialView("_AccountUserPartial", userListVM);


            }

            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AttachUser(string userName, string accountNumber)
        {
            try
            {
                if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(accountNumber))
                {
                    return BadRequest("Username or account number cant be empty");
                }

                var account = await _accountService.GetWithAllUsersAsync(accountNumber);

                var user = await _userService.GetByUsernameAsync(userName);

                await _accountService.AttachUserAsync(account, user);




                return Ok($"Succesfully added {user.Username} to {account.AccountNumber}!");


            }

            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        public async Task<IActionResult> UnattachUser(string userName, string accountNumber)
        {
            try
            {
                if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(accountNumber))
                {
                    return BadRequest("You try to unattach unexisting user!");
                }

                var account = await _accountService.GetWithAllUsersAsync(accountNumber);

                var user = await _userService.GetByUsernameAsync(userName);

                await _accountService.UnattachUserAsync(account, user);


                return Ok($"Succesfully unattach {user.Username} from {account.AccountNumber}!");


            }

            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpGet("/Administration/Home/AutoCompleteClient/{term}")]
        public async Task<JsonResult> AutoCompleteClient([FromRoute]string term)
        {
            IEnumerable<string> result = (await _clientService
                .ListAllAsync(m => m.StartsWith(term)));


            return Json(result);
        }

        public async Task<JsonResult> AutoCompleteUser(string term, string accountId)
        {
            IEnumerable<string> result = (await _userService
                .ListAllavailableUsersAsync(accountId, m => m.StartsWith(term)));

            return Json(result);
        }

        public async Task<JsonResult> AutoCompleteAccount(string term, string clientName)
        {
            IEnumerable<string> result = (await _accountService
                .ListAllNumbersAsync(clientName, m => m.StartsWith(term)));

            return Json(result);
        }


    }


}