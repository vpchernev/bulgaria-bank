﻿using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Areas.Administration.Utils
{
    public static class SaveFileToRoot
    {
        public static async Task<string> SaveFileAsync(IFormFile file)
        {
            if (file == null || file.Length == 0)
            {
                throw new ArgumentNullException("File not selected.");
            }

            var path = Path.Combine(
                  Directory.GetCurrentDirectory(), "wwwroot/BannersImage",
                  file.FileName);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            return file.FileName;
        }
    }
}
