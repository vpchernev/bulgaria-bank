﻿using AutoMapper;
using Bulgaria_Bank.Areas.Administration.Models;
using Bulgaria_Bank.Data.Models.Identity;

namespace Bulgaria_Bank.Areas.Administration.Mappers
{
    public class UserModelMapper : Profile
    {
        public UserModelMapper()
        {
            CreateMap<User, UserViewModel>();
        }
    }
}
