﻿using AutoMapper;
using Bulgaria_Bank.Areas.Administration.Models;
using Bulgaria_Bank.Data.Models;
using System.Linq;

namespace Bulgaria_Bank.Areas.Administration.Mappers
{
    public class ClientModelMapper : Profile
    {
        public ClientModelMapper() 
        {
            CreateMap<Client, ClientViewModel>()
                .ForMember(c => c.AcountsCount, src => src.MapFrom(c => c.Accounts.Count))
                .ForMember(c => c.TotalAmount, src => src.MapFrom(c => c.Accounts.Sum(a => a.CurrentBalance)));

        }
    }
}
