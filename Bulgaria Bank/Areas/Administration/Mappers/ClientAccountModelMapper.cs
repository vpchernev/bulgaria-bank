﻿using AutoMapper;
using Bulgaria_Bank.Areas.Administration.Models;
using Bulgaria_Bank.Data.Models;
using System.Linq;

namespace Bulgaria_Bank.Areas.Administration.Mappers
{
    public class ClientAccountModelMapper : Profile
    {
        public ClientAccountModelMapper()
        {
            CreateMap<Account, AccountViewModel>()
                .ForMember(a => a.Client, src => src.MapFrom(c => c.Client.Name))
                .ForMember(a => a.Users, src => src.MapFrom(c => c.UserAccounts.Count))
                .ForMember(a => a.UsersList, src => src.MapFrom(a => a.UserAccounts.Select(u => u.User)));

        }

    }
}
