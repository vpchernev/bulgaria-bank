﻿using AutoMapper;
using Bulgaria_Bank.Areas.Administration.Models;
using Bulgaria_Bank.Data.Models;

namespace Bulgaria_Bank.Areas.Administration.Mappers
{
    public class BannerModelMapper : Profile
    {
        public BannerModelMapper()
        {
            CreateMap<Banner, BannerToShowViewModel>();
        }
    }
}
