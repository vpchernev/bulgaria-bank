﻿using AutoMapper;
using Bulgaria_Bank.Areas.Identity.Models;
using Bulgaria_Bank.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Mappers
{
    public class UserAccountMapper : Profile
    {
        public UserAccountMapper()
        {
            CreateMap<UserAccount, UserAccountViewModel>()
                .ForMember(u => u.CurrentBalance, src => src.MapFrom(u => u.Account.CurrentBalance))
                .ForMember(u=>u.AccountId, src=>src.MapFrom(u=>u.AccountId))
                .ForMember(u=>u.AccountNumber, src => src.MapFrom(u=>u.Account.AccountNumber))
                .ForMember(u => u.Nickname, src => src.MapFrom(u => u.Nickname))
                .ForMember(u=>u.ClientName,src=>src.MapFrom(u=>u.Account.Client.Name));
        }
    }
}
