﻿using AutoMapper;
using Bulgaria_Bank.Areas.Identity.Models;
using Bulgaria_Bank.Data.Models;

namespace Bulgaria_Bank.Mappers
{
    public class RenameAccountMapper : Profile
    {
        public RenameAccountMapper()
        {
            CreateMap<UserAccount, RenameAccountViewModel>()
                .ForMember(x => x.AccountId, src => src.MapFrom(x => x.AccountId))
                .ForMember(x => x.Nickname, src => src.MapFrom(x => x.Nickname));
        }
    }
}
