﻿using AutoMapper;
using Bulgaria_Bank.Areas.Identity.Models;
using Bulgaria_Bank.Data.Models;
using System.Linq;

namespace Bulgaria_Bank.Mappers
{
    public class EditTransactionMapper : Profile
    {
        public EditTransactionMapper()
        {
            CreateMap<Transaction, EditTransactionViewModel>()
                .ForMember(x => x.Id, src => src.MapFrom(x => x.Id))
                .ForMember(x => x.SenderAccountNumber, src => src.MapFrom(x => x.SenderAccount.AccountNumber))
                .ForMember(x => x.ReceiverAccountNumber, src => src.MapFrom(x => x.ReceiverAccount.AccountNumber))
                .ForMember(x => x.Description, src => src.MapFrom(x => x.Description))
                .ForMember(x => x.Amount, src => src.MapFrom(x => x.Amount))
                .ForMember(x => x.ReceiverClientName, src => src.MapFrom(x => x.ReceiverAccount.Client.Name))
                .ForMember(x => x.SenderClientName, src => src.MapFrom(x => x.SenderAccount.Client.Name))
                .ForMember(x => x.SenderNickname, src => src.MapFrom(x => x.SenderAccount.UserAccounts.FirstOrDefault().Nickname));
        }
    }
}
