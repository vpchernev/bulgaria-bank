﻿using AutoMapper;
using Bulgaria_Bank.Areas.Identity.Models;
using Bulgaria_Bank.Data.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Mappers
{
    public class UserLoginMapper : Profile
    {
        public UserLoginMapper()
        {
            CreateMap<User, UserLoginViewModel>();
        }
    }
}
