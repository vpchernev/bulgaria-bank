﻿using AutoMapper;
using Bulgaria_Bank.Areas.Identity.Models;
using Bulgaria_Bank.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Mappers
{
    public class CreateTransactionMapper : Profile
    {
        public CreateTransactionMapper()
        {
            CreateMap<Account, CreateTransactionViewModel>()
                .ForMember(x => x.SenderAccountNumber, src => src.MapFrom(x => x.AccountNumber))
                .ForMember(x => x.SenderClientName, src => src.MapFrom(x => x.Client.Name))
                .ForMember(x=>x.SenderNickname,src=>src.MapFrom(x=>x.UserAccounts.FirstOrDefault().Nickname));
        }
    }
}
