﻿using AutoMapper;
using Bulgaria_Bank.Areas.Identity.Models;
using Bulgaria_Bank.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Mappers
{
    public class TransactionMapper : Profile
    {
        public TransactionMapper()
        {
            CreateMap<Transaction, TransactionViewModel>()
                .ForMember(x => x.Id, src => src.MapFrom(x => x.Id))
                .ForMember(x => x.ReceiverAccountNumber, src => src.MapFrom(x => x.ReceiverAccount.AccountNumber))
                .ForMember(x => x.ReceiverClientName, src => src.MapFrom(x => x.ReceiverAccount.Client.Name))
                .ForMember(x => x.ReceiverId, src => src.MapFrom(x => x.ReceiverId))
                .ForMember(x => x.SenderAccountNumber, src => src.MapFrom(x => x.SenderAccount.AccountNumber))
                .ForMember(x => x.SenderClientName, src => src.MapFrom(x => x.SenderAccount.Client.Name))
                .ForMember(x => x.SenderId, src => src.MapFrom(x => x.SenderId))
                .ForMember(x => x.Status, src => src.MapFrom(x => x.Status))
                .ForMember(x => x.Amount, src => src.MapFrom(x => x.Amount))
                .ForMember(x => x.CreatedOn, src => src.MapFrom(x => x.CreatedOn))
                .ForMember(x => x.Description, src => src.MapFrom(x => x.Description));
        }
    }
}
