﻿using AutoMapper;
using Bulgaria_Bank.Areas.Administration.Models;
using Bulgaria_Bank.Data.Models;

namespace Bulgaria_Bank.Mappers
{
    public class BannerMapper : Profile
    {
        public BannerMapper()
        {
            CreateMap<Banner, BannerViewModel>();
        }

    }
}
