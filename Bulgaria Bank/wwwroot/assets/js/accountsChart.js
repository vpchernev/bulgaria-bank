﻿$(document).ready(function () {

    var backgroundColors = [
        'rgb(255, 99, 132)',
        'rgb(54, 162, 235)',
        'rgb(255, 206, 86)',
        'rgb(75, 192, 192)',
        'rgb(153, 102, 255)',
        'rgb(255, 159, 64)',
        'rgb(100, 59, 64)',
        'rgb(130, 18, 243)',
        'rgb(150, 100, 64)',
    ]

    $.get("/Identity/BankAccounts/GetAccountBalances", function (data) {
        var accountNumbers = data.map(function (current) {
            return current.accountNumber;
        })
        var currentBalances = data.map(function (current) {
            return current.currentBalance;
        })
        var nickNames = data.map(function (current) {
            return current.nickname;
        })



        var ctx = document.getElementById('chart');
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: nickNames,
                datasets: [{
                    data: currentBalances,
                    backgroundColor: backgroundColors,
                    borderColor: backgroundColors,
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false

            }

        });

        var getData = function () {
            $.get("/Identity/BankAccounts/GetAccountBalances", function (data) {
                var accountNumbers = data.map(function (current) {
                    return current.accountNumber;
                })
                var currentBalances = data.map(function (current) {
                    return current.currentBalance;
                })
                var nickNames = data.map(function (current) {
                    return current.nickname;
                })

                // process your data to pull out what you plan to use to update the chart
                // e.g. new label and a new data point

                // add new label and data point to chart's underlying data structures
                myChart.data.labels = nickNames;
                myChart.data.datasets[0].data = currentBalances;

                // re-render the chart
                myChart.update();
            });
        };
        // get new data every 5 seconds
        setInterval(getData, 5000);
    });
})

