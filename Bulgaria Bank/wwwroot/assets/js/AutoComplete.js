﻿//Original

function attachAutoComplete() {

    $("#client-input-for-clients-dashboard").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "AutoCompleteClient/" + request.term,
                type: "GET",
                contentType: 'application/json',
                dataType: "json",
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }))
                }
            })
        },

        minLength: 3,
        delay: 500,
    });
}

$(document).on('click', "#client-input-for-clients-dashboard", function () {
    attachAutoComplete();
});

$(document).ready(function () {
    $("#client-input-for-index-page").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "AutoCompleteClient/" + request.term,
                type: "GET",
                contentType: 'application/json',
                dataType: "json",
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }))
                }
            })
        },

        minLength: 3,
        delay: 500,
    });
});



function attachAutoCompleteAccountDashboard() {

    var $this = $('#account-input-for-user-dashboard');
    
    var clientName = $this[0].attributes[2].value

    $("#account-input-for-user-dashboard").autocomplete({

        source: function (request, response) {
            $.ajax({
                url: `/Administration/Home/AutoCompleteAccount?term=${request.term}&clientName=${clientName}`,
                type: "GET",
                contentType: 'aplication/json',
                dataType: "json",
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, valure: item };
                    }))
                }
            })
        },

        minLength: 3,
        delay: 500,
    });
}

$(document).on('click', "#account-input-for-user-dashboard", function () {
    attachAutoCompleteAccountDashboard();
});
