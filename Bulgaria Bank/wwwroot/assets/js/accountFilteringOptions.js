﻿$(document).ready(function () {
    $('#available-accounts-options').select2({        
        ajax: {
            url: '/Identity/BankAccounts/GetAvailableUserAccounts',
            dataType: 'json',
            delay: 350
        }

    }).on('change', function () {
        var accountSelected = $('#available-accounts-options').val();
        console.log(accountSelected);
        var url = '/Identity/Transactions/Index?accountId=' + accountSelected

        window.location.href = url;
    })
});