﻿$('#target-dashboard').on('click', '.next-pagination-button-table', function (event) {
    var $this = $(this);
    event.preventDefault();

    var targetPage = parseInt($this.attr('next'));
    var urlPagination = "/Administration/Home/GetClients?=" + targetPage;

    $.get(urlPagination)
        .done(function (data) {
            $('#target-dashboard').empty();

            $('#target-dashboard').append(data);
        }).fail(function (failResponse) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(failResponse.responseText);
        });
});

$('#target-dashboard').on('click', '.previous-pagination-button-table', function (event) {
    event.preventDefault();
    var $this = $(this);
    var targetPage = parseInt($this.attr('previous'));

    var prevUrlPagination = "/Administration/Home/GetClients?=" + targetPage;
    $.get(prevUrlPagination)
        .done(function (data) {
            $('#target-dashboard').empty();
            $('#target-dashboard').append(data);
        }).fail(function (failResponse) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(failResponse.responseText);
        });
});

$('#target-dashboard').on('click', '.next-account-pagination-button-table', function (event) {
    var $this = $(this);
    event.preventDefault();

    var targetPage = $this.attr('next');
    var name = $this.attr('name')
    var urlPagination = `/Administration/Home/GetAccounts?page=${targetPage}&clientName=${name}`;

    $.get(urlPagination)
        .done(function (data) {
            $('#target-dashboard').empty();

            $('#target-dashboard').append(data);
        }).fail(function (failResponse) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(failResponse.responseText);
        });
});

$('#target-dashboard').on('click', '.previous-account-pagination-button-table', function (event) {
    event.preventDefault();
    var $this = $(this);

    var targetPage = parseInt($this.attr('previous'));
    var name = $this.attr('name')
    var prevUrlPagination = `/Administration/Home/GetAccounts?page=${targetPage}&clientName=${name}`;

    $.get(prevUrlPagination)
        .done(function (data) {
            $('#target-dashboard').empty();
            $('#target-dashboard').append(data);
        }).fail(function (failResponse) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(failResponse.responseText);
        });
});


$('#target-dashboard').on('click', '.next-users-pagination-button-table', function (event) {
    var $this = $(this);
    event.preventDefault();

    var targetPage = $this.attr('next');
    var name = $this.attr('name')
    var urlPagination = `/Administration/Home/ListUsers?currentPage=${targetPage}&accountName=${name}`;

    $.get(urlPagination)
        .done(function (data) {
            $('#target-dashboard').empty();

            $('#target-dashboard').append(data);
        }).fail(function (failResponse) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(failResponse.responseText);
        });
});

$('#target-dashboard').on('click', '.previous-users-pagination-button-table', function (event) {
    event.preventDefault();
    var $this = $(this);

    var targetPage = parseInt($this.attr('previous'));
    var name = $this.attr('name')
    var prevUrlPagination = `/Administration/Home/ListUsers?currentPage=${targetPage}&accountName=${name}`;

    $.get(prevUrlPagination)
        .done(function (data) {
            $('#target-dashboard').empty();
            $('#target-dashboard').append(data);
        }).fail(function (failResponse) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(failResponse.responseText);
        });
});

$('#target-dashboard').on('click', '.next-banners-pagination-button-table', function (event) {
    var $this = $(this);
    event.preventDefault();

    var targetPage = parseInt($this.attr('next'));
    var urlPagination = "/Administration/Banners/GetBannersList?=" + targetPage;

    $.get(urlPagination)
        .done(function (data) {
            $('#target-dashboard').empty();

            $('#target-dashboard').append(data);
        }).fail(function (failResponse) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(failResponse.responseText);
        });
});

$('#target-dashboard').on('click', '.previous-banners-pagination-button-table', function (event) {
    event.preventDefault();
    var $this = $(this);
    var targetPage = parseInt($this.attr('previous'));

    var prevUrlPagination = "/Administration/Banners/GetBannersList?=" + targetPage;
    $.get(prevUrlPagination)
        .done(function (data) {
            $('#target-dashboard').empty();
            $('#target-dashboard').append(data);
        }).fail(function (failResponse) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(failResponse.responseText);
        });
});

$('#target-dashboard').on('click', '.goToAccount', function (ev) {

    ev.preventDefault();

    var $this = $(this);
    var url = $this.attr('href');

    $.get(url)
        .done(function (data) {
      
            $('#target-dashboard').empty();


            $('#target-dashboard').append(data);

        })

        .fail(function (failResponse) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(failResponse.responseText);
        });


});

$('#target-dashboard').on('click', '.Go-to-account-users', function (ev) {

    ev.preventDefault();

    var $this = $(this);
    var url = $this.attr('href');

    $.get(url)
        .done(function (data) {

            $('#target-dashboard').empty();

            $('#target-dashboard').append(data);

        })

        .fail(function (failResponse) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(failResponse.responseText);
        });

});