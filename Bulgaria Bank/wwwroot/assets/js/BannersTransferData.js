﻿$(document).ready(function () {


    $('#submit-banner-form').submit(function (event) {
        event.preventDefault();
        var $this = $(this);

        var urlToGo = "/Administration/Banners/AddBanner";
        var data = $this.find('input');

        var file = data[0]
        var bannerFile = file.files[0]

        var bannerName = data[1].value

        var url = data[2].value

        var activeDays = data[4].value

        var startDate = data[3].value

        var token = data[5].attributes[2].value

        var newData = new FormData();
        newData.append("LinkUrl", url);
        newData.append("BannerName", bannerName);
        newData.append("DaysToBeActive", activeDays);
        newData.append("BannerFile", bannerFile);
        newData.append("StartDate", startDate)
        newData.append("__RequestVerificationToken", token);

        $.ajax({
            type: 'post',
            url: urlToGo,
            data: newData,
            processData: false,
            contentType: false
        }).done(function (responseBanner) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }

            $this[0].reset();


            $('#addBanner').modal('hide');
            toastr.success(responseBanner);

            $.get("/Administration/Banners/GetBannersList")
                .done(function (data) {

                    $('#target-dashboard').empty();
                    $('#target-dashboard').append(data);
                })
                .fail(function (failResponseBanner) {
                    toastr.options = {
                        "debug": false,
                        "positionClass": "toast-top-center",
                        "onclick": null,
                        "fadeIn": 300,
                        "fadeOut": 1000,
                        "timeOut": 3000,
                        "extendedTimeOut": 3000,
                        "closeButton": true
                    }
                    toastr.error(failResponseBanner.responseText);
                });

        }).fail(function (errorResponse) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(errorResponse.responseText);
        });
    })
});

$("#show-all-banners").click(function () {
    $.get("/Administration/Banners/GetBannersList")

        .done(function (data) {
            $('#target-dashboard').empty();

            $('#target-dashboard').append(data);
        })


        .fail(function (failResponse) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(failResponse.responseText);
        });
})

$(document).ready(function () {

    $('#target-dashboard').on('click', '.button-remove-banner', function () {

        attachConfirmRemoveBanner()
    });

    function attachConfirmRemoveBanner() {

        $('#confirm-banner-remove').on('click', '#button-remove-banner-confirm', function (ev) {

            ev.preventDefault();

            var $this = $(this);
            var url = $this.attr('href');

            $('#confirm-banner-remove').modal('hide');
            $('.modal-backdrop').remove();
            $.get(url)
                .done(function (responseBanner) {
                    toastr.options = {
                        "debug": false,
                        "positionClass": "toast-top-center",
                        "onclick": null,
                        "fadeIn": 300,
                        "fadeOut": 1000,
                        "timeOut": 3000,
                        "extendedTimeOut": 3000,
                        "closeButton": true
                    }



                    toastr.success(responseBanner);
                    $.get(`/Administration/Banners/GetBannersList`)

                        .done(function (data) {
                            $('#target-dashboard').empty();

                            $('#target-dashboard').append(data);

                        })

                        .fail(function (failResponse) {
                            toastr.options = {
                                "debug": false,
                                "positionClass": "toast-top-center",
                                "onclick": null,
                                "fadeIn": 300,
                                "fadeOut": 1000,
                                "timeOut": 3000,
                                "extendedTimeOut": 3000,
                                "closeButton": true
                            }
                            toastr.error(failResponse.responseText);
                        });
                })
                .fail(function (response) {
                    toastr.options = {
                        "debug": false,
                        "positionClass": "toast-top-center",
                        "onclick": null,
                        "fadeIn": 300,
                        "fadeOut": 1000,
                        "timeOut": 3000,
                        "extendedTimeOut": 3000,
                        "closeButton": true
                    }
                    toastr.error(response.responseText);
                });
        });
    }
})

