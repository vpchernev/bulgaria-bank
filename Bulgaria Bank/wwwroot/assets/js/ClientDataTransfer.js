﻿$('#submit-client-form').submit(function (event) {
    event.preventDefault();
    var $this = $(this);


    var url = "/Administration/Home/CreateClient";
    var name = $this.find('input');
    var serializedName = name.serialize();
    var token = name[0].attributes[2].value 
    

    $.post(url, serializedName, token)
        .done(function (response) {

            $this[0].reset();

            $('#addClientModal').modal('hide');

            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }

            toastr.success(response);

            $.get("/Administration/Home/GetClients")

                .done(function (data) {
                    $('#target-dashboard').empty();

                    $('#target-dashboard').append(data);
                })


                .fail(function (failResponse) {
                    toastr.options = {
                        "debug": false,
                        "positionClass": "toast-top-center",
                        "onclick": null,
                        "fadeIn": 300,
                        "fadeOut": 1000,
                        "timeOut": 3000,
                        "extendedTimeOut": 3000,
                        "closeButton": true
                    }
                    toastr.error(failResponse.responseText);
                });


        }).fail(function (response) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(response.responseText);
        });
});

$("#show-all-clients").click(function () {
    $.get("/Administration/Home/GetClients")

        .done(function (data) {
            $('#target-dashboard').empty();

            $('#target-dashboard').append(data);
        })


        .fail(function (failResponse) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(failResponse.responseText);
        }); 
})