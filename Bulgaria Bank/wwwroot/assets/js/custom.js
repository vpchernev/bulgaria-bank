﻿
/* Rename account*/

$(document).ready(function () {
    $('#dashboard-partial').on('click', '.button-rename-account', function () {
        var $this = $(this);
        var accountId = $this.data('id');
        console.log(accountId);
        var toaster = getTostr();

        $("#renameContainer").load("/Identity/BankAccounts/RenameAccount?accountId=" + accountId, function () {
            $("#rename").modal('show');
            var $renameForm = $("#rename-form");
            $renameForm.removeData('validator');
            $renameForm.removeData('unobtrusiveValidation');
            $.validator.unobtrusive.parse($renameForm);

            $renameForm.submit(function (event) {
                event.preventDefault();

                $.post({
                    url: "/Identity/BankAccounts/RenameAccount",
                    data: $renameForm.serialize()
                })
                    .done(function (response) {

                        $renameForm[0].reset();

                        $('#rename').modal('hide');


                        $('#rename').on('hidden.bs.modal', function (e) {
                            $('#dashboard-partial').load('/Identity/BankAccounts/DashboardPartial')
                        })

                        toaster.success(response);

                    }).fail(function (failResponse) {
                        toaster.error(failResponse.responseText);
                    });
            })
        })
    });
});

/* Submit payment*/
$(document).ready(function () {
    $('#dashboard-partial').on('click', '.button-make-preloaded-payment', function () {
        var $this = $(this);
        var accountNumber = $this.data('account-number');
        console.log(accountNumber)
        var toaster = getTostr();

        $("#paymentContainer").load("/Identity/Transactions/MakePayment?senderAccountNumber=" + accountNumber, function () {

            $("#preloadedPayment").modal('show');
            var $paymentForm = $('#create-payment-form');
            $paymentForm.removeData('validator');
            $paymentForm.removeData('unobtrusiveValidation');
            $.validator.unobtrusive.parse($paymentForm);
            accountOptions();

            $paymentForm.submit(function (event) {
                event.preventDefault();

                $.post({
                    url: "/Identity/Transactions/MakePayment",
                    data: $paymentForm.serialize()
                })
                    .done(function (response) {

                        $paymentForm[0].reset();

                        $('#preloadedPayment').modal('hide');

                        $('#preloadedPayment').on('hidden.bs.modal', function (e) {
                            $('#dashboard-partial').load('/Identity/BankAccounts/DashboardPartial')
                        })


                        toaster.success(response);

                    }).fail(function (failResponse) {
                        toaster.error(failResponse.responseText);
                    });
            });

            $("#save-payment-button").click(function () {
                event.preventDefault();

                var $paymentForm = $('#create-payment-form')

                $.post({
                    url: "/Identity/Transactions/Save",
                    data: $paymentForm.serialize()
                })
                    .done(function (response) {

                        $paymentForm[0].reset();

                        $('#preloadedPayment').modal('hide');

                        toaster.success(response);

                    }).fail(function (response) {
                        response.responseJSON.forEach(function (entry) {
                            toaster.error(entry)
                        })
                    });
            });
        });
    });
});

/* Normal Payment */
$(document).ready(function () {
    $('#transaction-partial').on('click', '.button-make-preloaded-payment', function () {
        var $this = $(this);
        var accountNumber = $this.data('account-number');
        console.log(accountNumber)
        var toaster = getTostr();

        $("#paymentContainer").load("/Identity/Transactions/MakePayment?senderAccountNumber=" + accountNumber, function () {

            $("#preloadedPayment").modal('show');
            accountOptions() 

            $('#submit-payment-button').click(function (event) {
                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();
                

                var paymentForm = $('#create-payment-form')


                $.post({
                    url: "/Identity/Transactions/MakePayment",
                    data: paymentForm.serialize()
                })
                    .done(function (response) {

                        paymentForm[0].reset();

                        $('#preloadedPayment').modal('hide');

                        $('#preloadedPayment').on('hidden.bs.modal', function (e) {
                            $('#transaction-partial').load('/Identity/Transactions/IndexPartial')
                        })


                        toaster.success(response);

                    }).fail(function (failResponse) {
                        toaster.error(failResponse.responseText);
                    });
            });

            $("#save-payment-button").click(function () {
                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();

                var paymentForm = $('#create-payment-form')


                $.post({
                    url: "/Identity/Transactions/Save",
                    data: paymentForm.serialize()
                })
                    .done(function (response) {

                        paymentForm[0].reset();

                        $('#preloadedPayment').modal('hide');

                        $('#preloadedPayment').on('hidden.bs.modal', function (e) {
                            $('#transaction-partial').load('/Identity/Transactions/IndexPartial')
                        })

                        toaster.success(response);

                    }).fail(function (response) {
                        toaster.error(response.responseText);
                    });
            });
        });
    });
});

/*Edit transaction*/
$(document).ready(function () {
    $('#transaction-partial').on('click', '.button-edit-transaction', function () {
        var $this = $(this);
        var transactionId = $this.data('id');
        console.log(transactionId)
        var toaster = getTostr();

        $("#editContainer").load("/Identity/Transactions/Edit?transactionId=" + transactionId, function () {

            $("#editModal").modal('show');
            accountOptions(); 

            $('#submit-edit-transaction-button').click(function (event) {
                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();

                var $editForm = $('#edit-transaction-form')
                $editForm.removeData('validator');
                $editForm.removeData('unobtrusiveValidation');
                $.validator.unobtrusive.parse($editForm);


                $.post({
                    url: "/Identity/Transactions/Edit",
                    data: $editForm.serialize()
                })
                    .done(function (response) {

                        $editForm[0].reset();

                        $('#editModal').modal('hide');

                        $('#editModal').on('hidden.bs.modal', function (e) {
                            location.reload();
                        })


                        toaster.success(response);

                    }).fail(function (response) {
                        toaster.error(response.responseText);
                    });
            });
        });
    });
});

/* Send saved transaction */
$(document).ready(function () {
    $('#transaction-partial').on('click', '.button-send-saved-transaction', function () {
        var $this = $(this);
        var transactionId = $this.data('id');
        console.log(transactionId)
        var toaster = getTostr();

        $("#sendContainer").load("/Identity/Transactions/SendSavedTransaction?id=" + transactionId, function () {

            $("#submitSavedModal").modal('show');
            var $submitForm = $('#send-saved-transaction-form');
            $submitForm.removeData('validator');
            $submitForm.removeData('unobtrusiveValidation');
            $.validator.unobtrusive.parse($submitForm);

            $submitForm.submit(function (event) {
                event.preventDefault();

                $.post({
                    url: "/Identity/Transactions/SendSavedTransaction",
                    data: $submitForm.serialize()
                })
                    .done(function (response) {

                        $submitForm[0].reset();

                        $('#submitSavedModal').modal('hide');

                        $('#submitSavedModal').on('hidden.bs.modal', function (e) {
                            setTimeout(function () {
                                location.reload();
                            },2500)
                            
                        })

                        toaster.success(response);

                    }).fail(function (response) {
                        toaster.error(response.responseText);
                    });
            });
        });
    });
});


/*Toastr options*/
function getTostr() {
    toastr.options = {
        "debug": false,
        "positionClass": "toast-top-center",
        "onclick": null,
        "fadeIn": 300,
        "fadeOut": 1000,
        "timeOut": 3000,
        "extendedTimeOut": 3000,
        "closeButton": true
    };
    return toastr;
}


/* View Transaction For Single Account*/