﻿//Original

$(document).ready(function () {
    $("#client-input-for-index-page").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "AutoCompleteAccount/" + request.term,
                type: "GET",
                contentType: 'aplication/json',
                dataType: "json",
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, valure: item };
                    }))
                }
            })
        },

        minLength: 3,
        delay: 500,
    });
});

function AutoCompleteUserAttach() {

    var target = $("#acount-number-to-attach-user")

    var account = target[0].attributes[4].value

    $("#user-target-search").autocomplete({

        source: function (request, response) {
            $.ajax({
                url: `/Administration/Home/AutoCompleteUser?term=${request.term}&accountId=${account}`,
                type: "GET",
                contentType: 'aplication/json',
                dataType: "json",
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, valure: item };
                    }))
                }
            })
        },

        minLength: 3,
        delay: 500,
    });
};

$(document).on('click', "#user-target-search", function () {
    AutoCompleteUserAttach();
});