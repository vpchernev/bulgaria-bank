﻿$('#submit-account-form').submit(function (event) {
    event.preventDefault();
    var $this = $(this);


    if ($this.valid()) {

        var url = "/Administration/Home/CreateClientAccount";
        var name = $this.find('input');
        var clientName = name[0].value;
        var token = name[0].attributes[2].value

        var serializedName = name.serialize();

        $.post(url, serializedName, token)
            .done(function (response) {

                $this[0].reset();

                $('#addAccountModal').modal('hide');

                toastr.options = {
                    "debug": false,
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000,
                    "closeButton": true
                }

                toastr.success(response);

                $.get("/Administration/Home/GetAccounts?clientName=" + clientName)

                    .done(function (data) {
                        $('#target-dashboard').empty();

                        $('#target-dashboard').append(data);

                    })

                    .fail(function (failResponse) {
                        toast.error(failResponse.responseText);
                    });


            }).fail(function (response) {
                toastr.options = {
                    "debug": false,
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000,
                    "closeButton": true
                }
                toastr.error(response.responseText);
            });
    };
});

$('#target-dashboard').on('submit', '#submit-client-search-form', function (ev) {
    ev.preventDefault()
    var $this = $(this);

    var name = $this.find('input');
    var clientName = name[0].value;

    $.get(`/Administration/Home/GetAccounts?clientName=${clientName}`)

        .done(function (data) {
            $('#target-dashboard').empty();

            $('#target-dashboard').append(data);

        })

        .fail(function (failResponse) {
            toast.error(failResponse.responseText);
        });
})

