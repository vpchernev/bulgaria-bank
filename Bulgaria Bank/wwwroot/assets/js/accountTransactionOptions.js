﻿function accountOptions() {
    $('#sender-account-options').select2({
        ajax: {
            url: '/Identity/BankAccounts/GetAvailableSenderAccounts',
            dataType: 'json',
            delay: 350
        }

    }).on('change', function () {
        var accountSelected = $('#sender-account-options').val();

        console.log(accountSelected);

        $.get({
            url: '/Identity/BankAccounts/GetClientName?=' + accountSelected,
            dataType: 'json'
        }).done(function (response) {
            console.log(response)
            $('#selected-sender-client').val(response)
        })
    })

    $('#receiver-account-options').select2({
        minimumInputLength: 2,
        ajax: {
            url: '/Identity/BankAccounts/GetAvailableReceiverAccounts',
            dataType: 'json',
            delay: 350
        }
    }).on('change', function () {
        var accountSelected = $('#receiver-account-options').val();

        $.get({
            url: '/Identity/BankAccounts/GetClientName?=' + accountSelected,
            dataType: 'json'
        }).done(function (response) {
            $('#selected-receiver-client').val(response)
        })
    });
};