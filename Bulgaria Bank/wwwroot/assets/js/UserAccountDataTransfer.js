﻿$('#submit-user-account-form').submit(function (event) {
    event.preventDefault();
    var $this = $(this);

    if ($($this).valid()) {

        var url = "/Administration/Home/CreateUser";
        var name = $this.find('input');
        var token = name[0].attributes[2].value

        var serializedName = name.serialize();

        $.post(url, serializedName, token)
            .done(function (response) {

                $this[0].reset();

                $('#createUser').modal('hide');

                toastr.options = {
                    "debug": false,
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000,
                    "closeButton": true
                }
                toastr.success(response);

            }).fail(function (response) {
                toastr.options = {
                    "debug": false,
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000,
                    "closeButton": true
                }
                toastr.error(response.responseText);
            });
    };
});

$('#target-dashboard').on('submit', '#submit-accounts-search-form', function (ev) {
    ev.preventDefault()

    var $this = $(this);

    var name = $this.find('input');
    var accountName = name[0].value;

    $.get(`/Administration/Home/ListUsers?accountName=${accountName}`)

        .done(function (data) {
            $('#target-dashboard').empty();

            $('#target-dashboard').append(data);

        })

        .fail(function (failResponse) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(failResponse.responseText);
        });
})


$('#target-dashboard').on('submit', '#user-attach-to-account', function (submitSearchEvent) {

    submitSearchEvent.preventDefault();
    $this = $(this);

    var data = $this.serialize();
   
    var givenAccount = $('#acount-number-to-attach-user');


    $.post("/Administration/Home/AttachUser", data)
        .done(function (response) {



            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.success(response);

            $.get(`/Administration/Home/ListUsers?accountName=${givenAccount[0].value}`)

                .done(function (data) {
                    $('#target-dashboard').empty();

                    $('#target-dashboard').append(data);

                })

                .fail(function (failResponse) {
                    toastr.options = {
                        "debug": false,
                        "positionClass": "toast-top-center",
                        "onclick": null,
                        "fadeIn": 300,
                        "fadeOut": 1000,
                        "timeOut": 3000,
                        "extendedTimeOut": 3000,
                        "closeButton": true
                    }
                    toastr.error(failResponse.responseText);
                });
        })
        .fail(function (response) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(response.responseText);
        });

});
$('#target-dashboard').on('click', '.user-UNATTACH-to-account', function (submitSearchEvent) {

    submitSearchEvent.preventDefault();
    $thisSubmit = $(this);

    var $url = $thisSubmit.attr('href');

    var $accountNumber = $('#remove-user-from-account')

    var accN = $accountNumber[0].attributes[0].value


    $.get($url)
        .done(function (response) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.success(response);

            $.get(`/Administration/Home/ListUsers?accountName=${accN}`)

                .done(function (data) {
                    $('#target-dashboard').empty();

                    $('#target-dashboard').append(data);

                })

                .fail(function (failResponse) {
                    toastr.options = {
                        "debug": false,
                        "positionClass": "toast-top-center",
                        "onclick": null,
                        "fadeIn": 300,
                        "fadeOut": 1000,
                        "timeOut": 3000,
                        "extendedTimeOut": 3000,
                        "closeButton": true
                    }
                    toastr.error(failResponse.responseText);
                });
        })
        .fail(function (response) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(response.responseText);
        });

});