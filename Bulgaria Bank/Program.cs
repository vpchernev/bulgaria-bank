﻿using Bulgaria_Bank.Configuration;
using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Services.Data.Utils;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Bulgaria_Bank
{
    public class Program
    {
        public static void Main(string[] args)
        {
           var host = CreateWebHostBuilder(args).Build();

            DataSeed(host).GetAwaiter().GetResult();

            host.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();


        public static async Task DataSeed(IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;

                try
                {
                    var _databaseContext = services.GetService<DatabaseContext>();
                    var _hashPassword = services.GetService<ICryptoWrapper>();


                    await Seeder.SeedDataAsync(host);
                }
                catch (Exception ex)
                {
                    var _logger = services.GetRequiredService<ILogger<Program>>();
                    _logger.LogError(ex, "An error occured while seeding the database.");
                }
            }
        }
    }
}
