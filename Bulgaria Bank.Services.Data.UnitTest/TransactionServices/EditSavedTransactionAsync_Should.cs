﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Services.Data.UnitTest.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.TransactionServices
{
    [TestClass]
    public class EditSavedTransactionAsync_Should
    {
        [TestMethod]
        public async Task Correctly_Updates_Passed_Transaction()
        {
            var options = TestUtils.GetOptions(nameof(Correctly_Updates_Passed_Transaction));

            var account1 = new Account();
            var account2 = new Account();
            var account3 = new Account();

            
            using (var arrangeContext = new DatabaseContext(options))
            {
                await arrangeContext.Accounts.AddAsync(account1);
                await arrangeContext.Accounts.AddAsync(account2);
                await arrangeContext.Accounts.AddAsync(account3);
                await arrangeContext.SaveChangesAsync();

            }

            var transaction = new Transaction()
            {
                Amount = 10,
                Description = "abc",
                ReceiverId = account1.Id,
                SenderId = account2.Id
            };

            using (var arrangeContext = new DatabaseContext(options))
            {
                await arrangeContext.Transactions.AddAsync(transaction);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new DatabaseContext(options))
            {
                transaction.Amount = 20;
                transaction.Description = "def";
                transaction.SenderId = account3.Id;
                transaction.ReceiverId = account2.Id;

                var sut = new TransactionService(assertContext);

                var edittedTransaction= await sut.EditSavedTransactionAsync(transaction);

                Assert.AreEqual(account2.Id, edittedTransaction.ReceiverId);
                Assert.AreEqual(account3.Id, edittedTransaction.SenderId);
                Assert.AreEqual("def", edittedTransaction.Description);
                Assert.AreEqual(20, edittedTransaction.Amount);
            }

        }

    }
}
