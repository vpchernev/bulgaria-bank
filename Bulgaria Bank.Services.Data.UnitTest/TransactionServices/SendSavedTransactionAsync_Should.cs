﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Services.Data.Exceptions;
using Bulgaria_Bank.Services.Data.UnitTest.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.TransactionServices
{
    [TestClass]
    public class SendSavedTransactionAsync_Should
    {
        [TestMethod]
        public async Task Throws_Argument_Null_Exception_When_Passed_Id_Is_Null()
        {
            var options = TestUtils.GetOptions(nameof(Throws_Argument_Null_Exception_When_Passed_Id_Is_Null));

            using (var assertContext = new DatabaseContext(options))
            {
                var sut = new TransactionService(assertContext);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(async ()=> await sut.SendSavedTransactionAsync(null));
            }
        }

        [TestMethod]
        public async Task Throws_Entity_Not_Found_Exception_When_Passed_Transaction_Was_Not_Found()
        {
            var options = TestUtils.GetOptions(nameof(Throws_Entity_Not_Found_Exception_When_Passed_Transaction_Was_Not_Found));

            using (var assertContext = new DatabaseContext(options))
            {
                var sut = new TransactionService(assertContext);

                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await sut.SendSavedTransactionAsync("2"));
            }
        }

        [TestMethod]
        public async Task Throws_Argument_Out_Of_Range_Exception_When_Senders_Balance_Is_Lower_Than_Transaction_Amount()
        {
            var options = TestUtils.GetOptions(nameof(Throws_Argument_Out_Of_Range_Exception_When_Senders_Balance_Is_Lower_Than_Transaction_Amount));

            var sender = new Account()
            {
                CurrentBalance = 0
            };
            var receiver = new Account();

            var listOfAccounts = new List<Account>()
            {
                sender,
                receiver
            };

            var client = new Client()
            {
                Accounts = listOfAccounts
            };

            using (var arrangeContext = new DatabaseContext(options))
            {
                await arrangeContext.Clients.AddAsync(client);
                await arrangeContext.SaveChangesAsync();
            }

            using (var arrangeContext = new DatabaseContext(options))
            {
                await arrangeContext.Transactions.AddAsync(new Transaction() { Amount=10,SenderId=sender.Id,ReceiverId=receiver.Id});
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new DatabaseContext(options))
            {
                var sut = new TransactionService(assertContext);

                var transactionId = await assertContext.Transactions.FirstOrDefaultAsync();

                await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(async () => await sut.SendSavedTransactionAsync(transactionId.Id));
            }
        }

        [TestMethod]
        public async Task Correctly_Updates_Balances_Of_Sender_And_Receiver_When_Valid_Data_Is_Passed()
        {
            var options = TestUtils.GetOptions(nameof(Correctly_Updates_Balances_Of_Sender_And_Receiver_When_Valid_Data_Is_Passed));


            var sender = new Account()
            {
                CurrentBalance = 10
            };
            var receiver = new Account()
            {
                CurrentBalance=1
            };

            var listOfAccounts = new List<Account>()
            {
                sender,
                receiver
            };

            var client = new Client()
            {
                Accounts = listOfAccounts
            };

            using (var arrangeContext = new DatabaseContext(options))
            {
                await arrangeContext.Clients.AddAsync(client);
                await arrangeContext.SaveChangesAsync();
            }

            using (var arrangeContext = new DatabaseContext(options))
            {
                await arrangeContext.Transactions.AddAsync(new Transaction() { Amount = 10, SenderId = sender.Id, ReceiverId = receiver.Id });
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new DatabaseContext(options))
            {
                var sut = new TransactionService(assertContext);

                var transaction = await assertContext.Transactions.FirstOrDefaultAsync();
                
                await sut.SendSavedTransactionAsync(transaction.Id);

                var savedSender = await assertContext.Accounts.FirstOrDefaultAsync(x => x.Id == sender.Id);
                var savedReceiver = await assertContext.Accounts.FirstOrDefaultAsync(x => x.Id == receiver.Id);


                Assert.AreEqual(0, savedSender.CurrentBalance);
                Assert.AreEqual(11, savedReceiver.CurrentBalance);
            }
        }

        [TestMethod]
        public async Task Correctly_Updates_Status_Of_Transaction_To_Sent()
        {
            var options = TestUtils.GetOptions(nameof(Correctly_Updates_Status_Of_Transaction_To_Sent));

            var sender = new Account()
            {
                CurrentBalance = 10
            };
            var receiver = new Account()
            {
                CurrentBalance = 1
            };

            var listOfAccounts = new List<Account>()
            {
                sender,
                receiver
            };

            var client = new Client()
            {
                Accounts = listOfAccounts
            };

            using (var arrangeContext = new DatabaseContext(options))
            {
                await arrangeContext.Clients.AddAsync(client);
                await arrangeContext.SaveChangesAsync();
            }

            using (var arrangeContext = new DatabaseContext(options))
            {
                await arrangeContext.Transactions.AddAsync(new Transaction() { Amount = 10, SenderId = sender.Id, ReceiverId = receiver.Id });
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new DatabaseContext(options))
            {
                var sut = new TransactionService(assertContext);

                var transactionId = (await assertContext.Transactions.FirstOrDefaultAsync()).Id;

                var transactionToBeSent = await sut.SendSavedTransactionAsync(transactionId);

                Assert.AreEqual("Sent", transactionToBeSent.Status.ToString());
            }
        }


    }
}
