﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Data.Models.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.TransactionServices
{
    [TestClass]
    public class SaveTransactionAsync_Should
    {
        [TestMethod]
        public async Task Assigns_Correctly_When_Valid_Data_Is_Passed()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Assigns_Correctly_When_Valid_Data_Is_Passed")
                .Options;

            var receiver = new Account();
            var sender = new Account();

            using (var arrangeContext = new DatabaseContext(contextOptions))
            {
                arrangeContext.Accounts.Add(receiver);
                arrangeContext.Accounts.Add(sender);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new DatabaseContext(contextOptions))
            {
                var sut = new TransactionService(assertContext);

                var transaction = await sut.SaveTransactionAsync(sender, receiver, "asd",12);

                Assert.AreEqual(sender.Id, transaction.SenderId);
                Assert.AreEqual(receiver.Id, transaction.ReceiverId);
                Assert.AreEqual("asd", transaction.Description);
                Assert.AreEqual(12, transaction.Amount);
            }
        }

        [TestMethod]
        public async Task Saves_Transaction_Correctly_In_The_Database()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Saves_Transaction_Correctly_In_The_Database")
                .Options;

            var receiver = new Account();
            var sender = new Account();

            using (var arrangeContext = new DatabaseContext(contextOptions))
            {
                arrangeContext.Accounts.Add(receiver);
                arrangeContext.Accounts.Add(sender);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new DatabaseContext(contextOptions))
            {
                var sut = new TransactionService(assertContext);

                await sut.SaveTransactionAsync(sender, receiver, "asd", 12);

                var transaction = await assertContext.Transactions.FirstOrDefaultAsync();

                Assert.IsNotNull(transaction);
                Assert.AreEqual(sender.Id, transaction.SenderId);
                Assert.AreEqual(receiver.Id, transaction.ReceiverId);
                Assert.AreEqual("asd", transaction.Description);
                Assert.AreEqual(12, transaction.Amount);
            }
        }
    }
}
