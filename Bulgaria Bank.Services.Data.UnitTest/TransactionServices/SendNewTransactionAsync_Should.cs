﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Services.Data.UnitTest.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.TransactionServices
{
    [TestClass]
    public class SendNewTransactionAsync_Should
    {
        [TestMethod]
        public async Task Throws_Argument_Null_Exception_When_Passed_Description_Is_Empty()
        {
            var options = TestUtils.GetOptions(nameof(Throws_Argument_Null_Exception_When_Passed_Description_Is_Empty));

            var account1 = new Account();
            var account2 = new Account();


            using (var assertContext = new DatabaseContext(options))
            {
                var sut = new TransactionService(assertContext);

               var result = await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await sut.SendNewTransactionAsync(account1, account2, null, 1));

                var expectedMessage = "Description cannot be null.";

                Assert.AreEqual(expectedMessage, result.ParamName);
            }
        }

        

        [TestMethod]
        public async Task Throws_Argument_Out_Of_Range_Exception_When_Passed_Amount_Is_Invalid()
        {
            var options = TestUtils.GetOptions(nameof(Throws_Argument_Out_Of_Range_Exception_When_Passed_Amount_Is_Invalid));

            var account1 = new Account();
            var account2 = new Account();


            using (var assertContext = new DatabaseContext(options))
            {
                var sut = new TransactionService(assertContext);

                var result = await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(async () => await sut.SendNewTransactionAsync(account1, account2, "abc", -1));

                var expectedMessage = "Amount cannot be 0 or negative.";

                Assert.AreEqual(expectedMessage, result.ParamName);
            }
        }

        [TestMethod]
        public async Task Throws_Argument_Out_Of_Range_Exception_When_Passed_Amount_Exceeds_Current_Balance()
        {
            var options = TestUtils.GetOptions(nameof(Throws_Argument_Out_Of_Range_Exception_When_Passed_Amount_Exceeds_Current_Balance));

            var account1 = new Account() {
                CurrentBalance=1
            };
            var account2 = new Account();


            using (var assertContext = new DatabaseContext(options))
            {
                var sut = new TransactionService(assertContext);

                var result = await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(async () => await sut.SendNewTransactionAsync(account1, account2, "abc", 2));

                var expectedMessage = "Amount cannot exceed the current balance of the account.";

                Assert.AreEqual(expectedMessage, result.ParamName);
            }
        }

        [TestMethod]
        public async Task Correctly_Updates_The_Balances_Of_Sender_And_Receiver_Accounts()
        {
            var options = TestUtils.GetOptions(nameof(Correctly_Updates_The_Balances_Of_Sender_And_Receiver_Accounts));

            var account1 = new Account()
            {
                CurrentBalance = 1
            };
            var account2 = new Account()
            {
                CurrentBalance = 13
            };

            using (var arrangeContext = new DatabaseContext(options))
            {
                await arrangeContext.Accounts.AddAsync(account1);
                await arrangeContext.Accounts.AddAsync(account2);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new DatabaseContext(options))
            {
                var sut = new TransactionService(assertContext);

                await sut.SendNewTransactionAsync(account1, account2, "abc", 1);

                Assert.AreEqual(0, account1.CurrentBalance);
                Assert.AreEqual(14, account2.CurrentBalance);
            }
        }

        [TestMethod]
        public async Task Correctly_Adds_The_New_Transaction_In_The_Database()
        {
            var options = TestUtils.GetOptions(nameof(Correctly_Adds_The_New_Transaction_In_The_Database));

            var account1 = new Account()
            {
                CurrentBalance = 1
            };
            var account2 = new Account()
            {
                CurrentBalance = 13
            };

            using (var arrangeContext = new DatabaseContext(options))
            {
                await arrangeContext.Accounts.AddAsync(account1);
                await arrangeContext.Accounts.AddAsync(account2);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new DatabaseContext(options))
            {
                var sut = new TransactionService(assertContext);

                await sut.SendNewTransactionAsync(account1, account2, "abc", 1);

                var transaction = await assertContext.Transactions.FirstOrDefaultAsync(); 

                Assert.AreEqual(account1.Id, transaction.SenderAccount.Id);
                Assert.AreEqual(account2.Id, transaction.ReceiverAccount.Id);
                Assert.AreEqual("abc", transaction.Description);
                Assert.AreEqual(1, transaction.Amount);
            }
        }
    }
}
