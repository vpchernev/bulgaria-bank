﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Services.Data.Exceptions;
using Bulgaria_Bank.Services.Data.UnitTest.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.TransactionServices
{
    [TestClass]
    public class GetAsync_Should
    {
        [TestMethod]
        public async Task Throws_Argument_Null_Exception_When_Passed_Id_Is_Null()
        {
            var options = TestUtils.GetOptions(nameof(Throws_Argument_Null_Exception_When_Passed_Id_Is_Null));

            using (var assertContext = new DatabaseContext(options))
            {
                var sut = new TransactionService(assertContext);

                var result = await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await sut.SendSavedTransactionAsync(null));

                var expectedMessage = "Transaction Id cannot be null.";

                Assert.AreEqual(expectedMessage, result.ParamName);
            }
        }

        [TestMethod]
        public async Task Throws_Entity_Not_Found_Exception_When_Passed_Transaction_Was_Not_Found()
        {
            var options = TestUtils.GetOptions(nameof(Throws_Entity_Not_Found_Exception_When_Passed_Transaction_Was_Not_Found));

            using (var assertContext = new DatabaseContext(options))
            {
                var sut = new TransactionService(assertContext);

                var result = await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await sut.SendSavedTransactionAsync("2"));

                var expectedMessage = "Requested transaction does not exist.";

                Assert.AreEqual(expectedMessage, result.Message);
            }
        }
    }
}
