﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Services.Data.UnitTest.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.UserAccountServices
{
    [TestClass]
    public class RenameAsync_Should
    {
        [TestMethod]
        public async Task Throw_ArgumentNullException_When_Passed_New_Name_Is_Null()
        {
            var options = TestUtils.GetOptions(nameof(Throw_ArgumentNullException_When_Passed_New_Name_Is_Null));

            var userAccount = new UserAccount()
            {
                Nickname = "abc"
            };

            using (var arrangeContext = new DatabaseContext(options))
            {
                await arrangeContext.UserAccounts.AddAsync(userAccount);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new DatabaseContext(options))
            {
                var sut = new UserAccountService(assertContext);

                var result = await Assert.ThrowsExceptionAsync<ArgumentNullException>(async()=>await sut.RenameAsync(userAccount, null));

                Assert.AreEqual("Name cannot be null.", result.ParamName);
            }

        }

        [TestMethod]
        public async Task Correctly_Updates_The_New_Name_To_UserAccount_Entity()
        {
            var options = TestUtils.GetOptions(nameof(Correctly_Updates_The_New_Name_To_UserAccount_Entity));

            var userAccount = new UserAccount()
            {
                Nickname = "abc"
            };

            using (var arrangeContext = new DatabaseContext(options))
            {
                await arrangeContext.UserAccounts.AddAsync(userAccount);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new DatabaseContext(options))
            {
                var sut = new UserAccountService(assertContext);

                var renamedUserAccount = await sut.RenameAsync(userAccount, "def");

                Assert.AreEqual("def", renamedUserAccount.Nickname);
            }

        }
    }
}
