﻿//using Bulgaria_Bank.Data.Context;
//using Bulgaria_Bank.Services.Data.Exceptions;
//using Bulgaria_Bank.Services.Data.UnitTest.Utils;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Threading.Tasks;

//namespace Bulgaria_Bank.Services.Data.UnitTest.UserAccountServices
//{
//    [TestClass]
//    public class GetUserAccountsPages_Should
//    {
//        [TestMethod]
//        public async Task Throw_EntityNotFoundException_When_UserAccount_Does_Not_Exist()
//        {
//            var options = TestUtils.GetOptions(nameof(Throw_EntityNotFoundException_When_UserAccount_Does_Not_Exist));

//            using (var assertContext = new DatabaseContext(options))
//            {
//                var sut = new UserAccountService(assertContext);

//                var result = await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await sut.GetUserAccountsPages("1", 1, "a", "a", "a"));

//                Assert.AreEqual("Requested account does not exist.", result.Message);
//            }
//        }
//    }
//}
