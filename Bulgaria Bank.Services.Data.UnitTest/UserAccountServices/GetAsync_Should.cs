﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Data.Models.Identity;
using Bulgaria_Bank.Services.Data.Exceptions;
using Bulgaria_Bank.Services.Data.UnitTest.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.UserAccountServices
{
    [TestClass]
    public class GetAsync_Should
    {
        [TestMethod]
        public async Task Throw_EntityNotFoundException_Entity_Is_Not_In_The_Database()
        {
            var options = TestUtils.GetOptions(nameof(Throw_EntityNotFoundException_Entity_Is_Not_In_The_Database));

            using (var assertContext = new DatabaseContext(options))
            {
                var sut = new UserAccountService(assertContext);

                var result = await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await sut.GetAsync("1", "2"));

                Assert.AreEqual("Requested account does not exist.", result.Message);
            }

        }

        [TestMethod]
        public async Task Returns_Correctly_Requested_UserAccount()
        {
            var options = TestUtils.GetOptions(nameof(Throw_EntityNotFoundException_Entity_Is_Not_In_The_Database));

            var user = new User();
            var account = new Account();
            var userAccount = new UserAccount()
            {
                User=user,
                Account=account
            };

            using (var arrangeContext = new DatabaseContext(options))
            {
                await arrangeContext.UserAccounts.AddAsync(userAccount);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new DatabaseContext(options))
            {
                var sut = new UserAccountService(assertContext);

                var result = await sut.GetAsync(user.Id, account.Id);

                Assert.AreEqual(userAccount.UserId, result.UserId);
                Assert.AreEqual(userAccount.AccountId, result.AccountId);
            }

        }
    }
}
