﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Data.Models.Identity;
using Bulgaria_Bank.Services.Data.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.UserServices
{
    [TestClass]
    public class GetUsers_Should
    {
        [TestMethod]
        public async Task Return_Correct_List_Of_Users()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
               .UseInMemoryDatabase(databaseName: "Return_Correct_List_Of_Users")
               .Options;

            var cryptoWrapperMock = new Mock<ICryptoWrapper>();

            var currentPage = 1;

            var user3 = new User() { FirstName = "userFirst3", LastName = "userLast3", Username = "userUser3", CreatedOn = DateTime.Now.AddHours(1) };
            var user1 = new User() { FirstName = "userFirst1", LastName = "userLast1", Username = "userUser1", CreatedOn = DateTime.Now.AddHours(5) };
            var user2 = new User() { FirstName = "userFirst2", LastName = "userLast2", Username = "userUser2", CreatedOn = DateTime.Now.AddHours(10) };

            string accountNumber1 = "1000000001";
            
            var account1 = new Account() { AccountNumber = accountNumber1};

            var client = new Client() { Accounts = { account1 } };

            ICollection<User> result;
            List<User> resultList = new List<User>();

            var userAccount1 = new UserAccount()
            {
                User = user1,
                UserId = user1.Id,
                Account = account1,
                AccountId = account1.Id
            };

            var userAccount2 = new UserAccount()
            {
                User = user2,
                UserId = user2.Id,
                Account = account1,
                AccountId = account1.Id
            };
            var userAccount3 = new UserAccount()
            {
                User = user3,
                UserId = user3.Id,
                Account = account1,
                AccountId = account1.Id
            };

            using (var arrangeContext = new DatabaseContext(contextOptions))
            {
                await arrangeContext.UserAccounts.AddAsync(userAccount1);
                await arrangeContext.UserAccounts.AddAsync(userAccount2);
                await arrangeContext.UserAccounts.AddAsync(userAccount3);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new DatabaseContext(contextOptions))
            {
                var SUT = new UserService(assertContext, cryptoWrapperMock.Object);

                result = await SUT.GetListByPageAsync(currentPage, account1.Id);
            }

            foreach (var user in result)
            {
                resultList.Add(user);
            }

            Assert.IsTrue(resultList.Count != 0);
            Assert.IsTrue(resultList.Count == 3);
            Assert.IsTrue(resultList[0].FirstName == user2.FirstName && resultList[0].LastName == user2.LastName && resultList[0].Username == user2.Username);
            Assert.IsTrue(resultList[1].FirstName == user1.FirstName && resultList[1].LastName == user1.LastName && resultList[1].Username == user1.Username);
            Assert.IsTrue(resultList[2].FirstName == user3.FirstName && resultList[2].LastName == user3.LastName && resultList[2].Username == user3.Username);
        }
    }
}
