﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models.Common;
using Bulgaria_Bank.Data.Models.Identity;
using Bulgaria_Bank.Services.Data.Exceptions;
using Bulgaria_Bank.Services.Data.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.UserServices
{
    [TestClass]
    public class AuthenticateAsync_Should
    {
        static string username = "UserName";
        static string password = "Password";
        static string confirmPassword = "Password";
        static string InvalidConfirmPass = "Pass;";
        static string firstName = "FirstName";
        static string lastname = "LastName";

        [TestMethod]
        public async Task Return_PasswordDoesNotMatchException_If_Password_Do_Not_Match()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
               .UseInMemoryDatabase(databaseName: "Return_DuplicateEntityException_If_User_With_The_Same_UserName_Exist")
               .Options;

            var cryptoWrapperMock = new Mock<ICryptoWrapper>();
            cryptoWrapperMock.Setup(c => c.VerifyHashedPassword(password, InvalidConfirmPass)).Throws(new PasswordDoesNotMatchException("lele"));


            var user = new User() { Username = username, PasswordHash = password };
            var role = new Role() { Name = RoleType.User.ToString(), Users = { user } };

            using (var arrangeContext = new DatabaseContext(contextOptions))
            {
                await arrangeContext.Roles.AddAsync(role);
                await arrangeContext.SaveChangesAsync();
            }


            using (var assertContext = new DatabaseContext(contextOptions))
            {
                var SUT = new UserService(assertContext, cryptoWrapperMock.Object);

                await Assert.ThrowsExceptionAsync<PasswordDoesNotMatchException>(
                                   () => SUT.AuthenticateAsync(username, password));
            }

        }

        [TestMethod]
        public async Task Return_EntityNotFoundException_If_User_Does_Not_Exist()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
               .UseInMemoryDatabase(databaseName: "Return_EntityNotFoundException_If_User_Does_Not_Exist")
               .Options;

            var cryptoWrapperMock = new Mock<ICryptoWrapper>();

            using (var assertContext = new DatabaseContext(contextOptions))
            {
                var SUT = new UserService(assertContext, cryptoWrapperMock.Object);

                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                                   () => SUT.AuthenticateAsync(username, password));
            }

        }

        [TestMethod]
        public async Task Return_User_If_All_Data_Pass()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
               .UseInMemoryDatabase(databaseName: "Return_User_If_All_Data_Pass")
               .Options;

            var cryptoWrapperMock = new Mock<ICryptoWrapper>();
            cryptoWrapperMock.Setup(c => c.VerifyHashedPassword(password, password)).Returns(true);

            User result;

            var user = new User() { Username = username, PasswordHash = password };
            var role = new Role() { Name = RoleType.User.ToString(), Users = { user } };

            using (var arrangeContext = new DatabaseContext(contextOptions))
            {
                await arrangeContext.Roles.AddAsync(role);
                await arrangeContext.SaveChangesAsync();
            }


            using (var assertContext = new DatabaseContext(contextOptions))
            {
                var SUT = new UserService(assertContext, cryptoWrapperMock.Object);

                result = await SUT.AuthenticateAsync(username, password);

                Assert.IsNotNull(result);
                Assert.IsTrue(await assertContext.Users.AnyAsync(u => u.FirstName == result.FirstName && u.Id == result.Id));
            }

        }
    }
}
