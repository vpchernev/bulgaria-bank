﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Services.Data.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bulgaria_Bank.Services.Data.UnitTest.UserServices
{
    [TestClass]
    public class GetUserPages_Should
    {
        [TestMethod]
        public static void GetUsersPages_Should_Return_Correct_Page_Numbers()
        {
            int count = 25;
            int onPageUsers = 5;

            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
               .UseInMemoryDatabase(databaseName: "GetUsersPages_Should_Return_Correct_Page_Numbers")
               .Options;

            var cryptoWrapperMock = new Mock<ICryptoWrapper>();

            using (DatabaseContext assertContext = new DatabaseContext(contextOptions))
            {
                var SUT = new UserService(assertContext, cryptoWrapperMock.Object);

                var result = SUT.GetUserPages(onPageUsers, count);

                Assert.AreEqual(result, 4);
            }
        }
    }
}
