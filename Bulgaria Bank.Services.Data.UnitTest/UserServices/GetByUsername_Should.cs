﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models.Identity;
using Bulgaria_Bank.Services.Data.Exceptions;
using Bulgaria_Bank.Services.Data.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.UserServices
{
    [TestClass]
    public class GetByUsername_Should
    {
        [TestMethod]
        public async Task Return_ArgumentNullException_When_UserName_Is_Null()
        {

            var databaseContextMock = new Mock<DatabaseContext>();
            var cryptoWrapperMock = new Mock<ICryptoWrapper>();

            var SUT = new UserService(databaseContextMock.Object, cryptoWrapperMock.Object);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(
                                   () => SUT.GetByUsernameAsync(null));
        }


        [TestMethod]
        public async Task Return_EntityNotFoundException_When_UserName_Is_Null()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                            .UseInMemoryDatabase(databaseName: "Return_EntityNotFoundException_When_UserName_Is_Null")
                            .Options;

            var cryptoWrapperMock = new Mock<ICryptoWrapper>();

            using (var assertContext = new DatabaseContext(contextOptions))
            {
                var SUT = new UserService(assertContext, cryptoWrapperMock.Object);

                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                                       () => SUT.GetByUsernameAsync("asdasdd"));

            }
        }

        [TestMethod]
        public async Task Return_User_With_CorrectId_Whent_All_Data_Passed()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                            .UseInMemoryDatabase(databaseName: "Return_User_With_CorrectId_Whent_All_Data_Passed")
                            .Options;

            var cryptoWrapperMock = new Mock<ICryptoWrapper>();
            string userName = "UserName";

            var user = new User() {Username = userName };
            User result;

            using (var arrangeContext = new DatabaseContext(contextOptions))
            {
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new DatabaseContext(contextOptions))
            {
                var SUT = new UserService(assertContext, cryptoWrapperMock.Object);

                result = await SUT.GetByUsernameAsync(userName);

                Assert.IsNotNull(result);
                Assert.IsTrue(await assertContext.Users.AnyAsync(u => u.Id == result.Id && u.Username == result.Username));

            }
        }
    }
}
