﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models.Common;
using Bulgaria_Bank.Data.Models.Identity;
using Bulgaria_Bank.Services.Data.Exceptions;
using Bulgaria_Bank.Services.Data.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.UserServices
{
    [TestClass]
    public class CreateAsync_Should
    {
        static string username = "UserName";
        static string password = "Password";
        static string confirmPassword = "Password";
        static string InvalidConfirmPass = "Pass;";
        static string firstName = "FirstName";
        static string lastname = "LastName";

        [TestMethod]
        public async Task Return_ArgumentNullException_If_Username_Is_Null()
        {
            var databaseContextMock = new Mock<DatabaseContext>();
            var cryptoWrapperMock = new Mock<ICryptoWrapper>();

            var SUT = new UserService(databaseContextMock.Object, cryptoWrapperMock.Object);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(
                                   () => SUT.CreateAsync(null, password, confirmPassword, firstName, lastname));
        }

        [TestMethod]
        public async Task Return_ArgumentNullException_If_Password_Is_Null()
        {
            var databaseContextMock = new Mock<DatabaseContext>();
            var cryptoWrapperMock = new Mock<ICryptoWrapper>();

            var SUT = new UserService(databaseContextMock.Object, cryptoWrapperMock.Object);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(
                                   () => SUT.CreateAsync(username, null, confirmPassword, firstName, lastname));
        }

        [TestMethod]
        public async Task Return_ArgumentNullException_If_ConfirmPassword_Is_Null()
        {
            var databaseContextMock = new Mock<DatabaseContext>();
            var cryptoWrapperMock = new Mock<ICryptoWrapper>();

            var SUT = new UserService(databaseContextMock.Object, cryptoWrapperMock.Object);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(
                                   () => SUT.CreateAsync(username, password, null, firstName, lastname));
        }

        [TestMethod]
        public async Task Return_ArgumentNullException_If_FirstName_Is_Null()
        {
            var databaseContextMock = new Mock<DatabaseContext>();
            var cryptoWrapperMock = new Mock<ICryptoWrapper>();

            var SUT = new UserService(databaseContextMock.Object, cryptoWrapperMock.Object);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(
                                   () => SUT.CreateAsync(username, password, confirmPassword, null, lastname));
        }

        [TestMethod]
        public async Task Return_ArgumentNullException_If_LastName_Is_Null()
        {
            var databaseContextMock = new Mock<DatabaseContext>();
            var cryptoWrapperMock = new Mock<ICryptoWrapper>();

            var SUT = new UserService(databaseContextMock.Object, cryptoWrapperMock.Object);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(
                                   () => SUT.CreateAsync(username, password, confirmPassword, firstName, null));
        }

        [TestMethod]
        public async Task Return_ArgumentException_If_Paswword_And_ConfirmPassword_Are_Not_Equal()
        {
            var databaseContextMock = new Mock<DatabaseContext>();
            var cryptoWrapperMock = new Mock<ICryptoWrapper>();

            var SUT = new UserService(databaseContextMock.Object, cryptoWrapperMock.Object);

            await Assert.ThrowsExceptionAsync<ArgumentException>(
                                   () => SUT.CreateAsync(username, password, InvalidConfirmPass, firstName, lastname));
        }

        [TestMethod]
        public async Task Return_DuplicateEntityException_If_User_With_The_Same_UserName_Exist()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Return_DuplicateEntityException_If_User_With_The_Same_UserName_Exist")
                .Options;

            var user = new User() { Username = username };
            var cryptoWrapperMock = new Mock<ICryptoWrapper>();

            using (var arrangeContext = new DatabaseContext(contextOptions))
            {
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new DatabaseContext(contextOptions))
            {

                var SUT = new UserService(actContext, cryptoWrapperMock.Object);

                await Assert.ThrowsExceptionAsync<DuplicateEntityException>(
                                   () => SUT.CreateAsync(username, password, confirmPassword, firstName, lastname));

            }

        }

        [TestMethod]
        public async Task Return_New_Client_When_All_Values_Are_Correct()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Return_New_Client_When_All_Values_Are_Correct")
                .Options;

            User result;

            var role = new Role() { Name = RoleType.User.ToString() };

            var cryptoWrapperMock = new Mock<ICryptoWrapper>();

            cryptoWrapperMock.Setup(c => c.HashPassword(password)).Returns(password);

            using (var arrangeContext = new DatabaseContext(contextOptions))
            {
                arrangeContext.Roles.Add(role);
                await arrangeContext.SaveChangesAsync();

            }

            using (var actContext = new DatabaseContext(contextOptions))
            {
                var SUT = new UserService(actContext, cryptoWrapperMock.Object);

                result = await SUT.CreateAsync(username, password, confirmPassword, firstName, lastname);

            }

            using (var assertContext = new DatabaseContext(contextOptions))
            {
                Assert.IsNotNull(result);
                Assert.IsTrue(await assertContext.Users.AnyAsync
                    (u => u.FirstName == result.FirstName &&
                          u.LastName == result.LastName   &&
                          u.Username == result.Username   &&
                          u.FirstName == result.FirstName));
            }

        }

    }
}
