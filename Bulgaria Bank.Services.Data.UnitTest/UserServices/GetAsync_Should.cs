﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Data.Models.Common;
using Bulgaria_Bank.Data.Models.Identity;
using Bulgaria_Bank.Services.Data.Exceptions;
using Bulgaria_Bank.Services.Data.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.UserServices
{
    [TestClass]
    public class GetAsync_Should
    {
        static string userInvalidId = "sdfsdf123123";


        [TestMethod]
        public async Task Return_ArgumentNullException_If_UserId_Is_Null()
        {
            var databaseContextMock = new Mock<DatabaseContext>();
            var cryptoWrapperMock = new Mock<ICryptoWrapper>();

            var SUT = new UserService(databaseContextMock.Object, cryptoWrapperMock.Object);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(
                                   () => SUT.GetAsync(null));
        }

        [TestMethod]
        public async Task Return_ArgumentNullException_If_User_Dosent_Exist()
        {

            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Return_DuplicateEntityException_If_User_With_The_Same_UserName_Exist")
                .Options;

            var cryptoWrapperMock = new Mock<ICryptoWrapper>();


            using (var assertContext = new DatabaseContext(contextOptions))
            {
                var SUT = new UserService(assertContext, cryptoWrapperMock.Object);

                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(() => SUT.GetAsync(userInvalidId));
            }

        }

        [TestMethod]
        public async Task Return_User_When_UserId_Is_Correct()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Return_New_Client_When_All_Values_Are_Correct")
                .Options;

            User result;

            var account = new Account();

            var client = new Client() { Accounts = { account } };

            var role = new Role() { Name = RoleType.User.ToString() };

            var user = new User() { Role = role, RoleId = role.Id };

            

            var cryptoWrapperMock = new Mock<ICryptoWrapper>();

            using (var arrangeContext = new DatabaseContext(contextOptions))
            {
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Clients.AddAsync(client);
                await arrangeContext.SaveChangesAsync();

            }

            var userAccount = new UserAccount()
            {
                AccountId = account.Id,
                UserId = user.Id
            };

            using (var arrange2Context = new DatabaseContext(contextOptions))
            {
                await arrange2Context.UserAccounts.AddAsync(userAccount);
                await arrange2Context.SaveChangesAsync();
            }

            using (var assertContext = new DatabaseContext(contextOptions))
            {
                var SUT = new UserService(assertContext, cryptoWrapperMock.Object);


                result = await SUT.GetAsync(user.Id);

                Assert.IsNotNull(result);
                Assert.IsTrue(result.UserAccounts.Any(u => u.AccountId == account.Id));
                Assert.IsTrue(result.UserAccounts.Count() == 1);
                Assert.IsTrue(assertContext.UserAccounts.Where(a => a.AccountId == account.Id && a.UserId == result.Id).Any(a => a.Account.ClientId == client.Id));
            }

        }

    }
}
