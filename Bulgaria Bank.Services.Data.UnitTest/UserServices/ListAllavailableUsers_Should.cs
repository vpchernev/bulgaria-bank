﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Data.Models.Identity;
using Bulgaria_Bank.Services.Data.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.UserServices
{
    [TestClass]
    public class ListAllavailableUsers_Should
    {
       static Account account;

        [TestMethod]
        public async Task Return_Correct_Collection_Of_Usernames()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
              .UseInMemoryDatabase(databaseName: "Return_Correct_Collection_Of_Usernames")
              .Options;

            var cryptoWrapperMock = new Mock<ICryptoWrapper>();

            IEnumerable<string> result;

            string userName1 = "user1";
            string userName2 = "user2";
            string userName3 = "Unknown";

            string term = "use";

            var user1 = new User() { Username = userName1 };
            var user2 = new User() { Username = userName2 };
            var user3 = new User() { Username = userName3 };

            account = new Account();

      

            using (var arrangeContext = new DatabaseContext(contextOptions))
            {
                await arrangeContext.Users.AddAsync(user1);
                await arrangeContext.Users.AddAsync(user2);
                await arrangeContext.Users.AddAsync(user3);
                await arrangeContext.Accounts.AddAsync(account);

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new DatabaseContext(contextOptions))
            {
                var SUT = new UserService(actContext, cryptoWrapperMock.Object);

                result = await SUT.ListAllavailableUsersAsync(account.Id, m => m.StartsWith(term));
            }

            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count(), 2);
            Assert.IsTrue(result.Contains(userName1));
            Assert.IsTrue(result.Contains(userName2));
        }
    }
}
