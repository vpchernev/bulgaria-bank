﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models.Common;
using Bulgaria_Bank.Data.Models.Identity;
using Bulgaria_Bank.Services.Data.Contracts;
using Bulgaria_Bank.Services.Data.Exceptions;
using Bulgaria_Bank.Services.Data.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace Bulgaria_Bank.Services.Data.UnitTest.AdminServices
{
    [TestClass]
    public class AuthenticateAsync_Should
    {
        [TestMethod]
        public async Task Return_EntityNotFoundException_When_UserName_Dosent_Found()
        {

            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Return_EntityNotFoundException_When_UserName_Dosent_Found")
                .Options;

            var cryptoWrapperMock = new Mock<ICryptoWrapper>();

            string userName = "user";
            string password = "password";

            using (var assertContext = new DatabaseContext(contextOptions))
            {
                var SUT = new AdminService(assertContext, cryptoWrapperMock.Object);
                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(() => SUT.AuthenticateAsync(userName, password));
            }
        }

        [TestMethod]
        public async Task Return_EntityNotFoundException_When_UserPassword_Is_Inccorect()
        {

            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Return_EntityNotFoundException_When_UserName_Dosent_Found")
                .Options;
            string password = "password";
            string userName = "user";


            var cryptoWrapperMock = new Mock<ICryptoWrapper>();
            cryptoWrapperMock.Setup(c => c.VerifyHashedPassword(password, password)).Throws(new EntityNotFoundException(message: "blq blq"));

            var user = new User() { Username = userName };

            using (var arrangeContext = new DatabaseContext(contextOptions))
            {
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new DatabaseContext(contextOptions))
            {
                var SUT = new AdminService(assertContext, cryptoWrapperMock.Object);
                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(() => SUT.AuthenticateAsync(userName, password));
            }
        }

        [TestMethod]
        public async Task Return_Correct_Admin_User_When_All_Data_Is_Correct()
        {

            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Return_Correct_Admin_User_When_All_Data_Is_Correct")
                .Options;

            string password = "password";
            string userName = "user";
            RoleType roleName = RoleType.Administrator;
            Admin result;

            var role = new Role() { Name = roleName.ToString() };
            var admin = new Admin() { Username = userName, PasswordHash = password, Role = role };

            role.Admins.Add(admin);

            var cryptoWrapperMock = new Mock<ICryptoWrapper>();
            cryptoWrapperMock.Setup(c => c.VerifyHashedPassword(password, password)).Returns(true);

            using (var arrangeContext = new DatabaseContext(contextOptions))
            {
                await arrangeContext.Roles.AddAsync(role);
                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new DatabaseContext(contextOptions))
            {

                var SUT = new AdminService(actContext, cryptoWrapperMock.Object);

                result = await SUT.AuthenticateAsync(userName, password);

                Assert.IsNotNull(result);
                Assert.AreEqual(result.RoleId, role.Id);
                Assert.AreEqual(result.PasswordHash, password);
                Assert.AreEqual(result.Username, userName);
            }

        }
    }
}
