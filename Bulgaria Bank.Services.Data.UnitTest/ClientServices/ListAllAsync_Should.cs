﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.ClientServices
{
    [TestClass]
    public class ListAllAsync_Should
    {
        [TestMethod]
        public async Task Return_Correct_Collection_Of_Strings()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
              .UseInMemoryDatabase(databaseName: "Return_Correct_Collection_Of_Strings")
              .Options;

            IEnumerable<string> result;

            string clientName1 = "client1";
            string clientName2 = "client2";
            string clientName3 = "Unknown";

            string term = "cli";


            var client1 = new Client() { Name = clientName1 };
            var client2 = new Client() { Name = clientName2 };
            var client3 = new Client() { Name = clientName3 };


            using (var arrangeContext = new DatabaseContext(contextOptions))
            {
                await arrangeContext.Clients.AddAsync(client1);
                await arrangeContext.Clients.AddAsync(client2);
                await arrangeContext.Clients.AddAsync(client3);

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new DatabaseContext(contextOptions))
            {
                var SUT = new ClientService(actContext);

                result = await SUT.ListAllAsync(m => m.StartsWith(term));
            }

            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count(), 2);
            Assert.IsTrue(result.Contains(clientName1));
            Assert.IsTrue(result.Contains(clientName2));
        }
    }

}
