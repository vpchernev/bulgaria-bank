﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.ClientServices
{
    [TestClass]
    public class GetPageCount_Should
    {
        [TestMethod]
        public async Task Return_Correct_Clients_Count()
        {

            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Return_Correct_UserAccounts_Count")
                .Options;
            string clientName1 = "clentName1";
            string clientName2 = "clentName2";
            string clientName3 = "clentName3";

            int onepage = 5;
            int allPage = 1;

            var client1 = new Client() { Name = clientName1};
            var client2 = new Client() { Name = clientName2 };
            var client3 = new Client() { Name = clientName3 };

            
            using (DatabaseContext arrangetContext = new DatabaseContext(contextOptions))
            {
                
                await arrangetContext.Clients.AddAsync(client1);
                await arrangetContext.Clients.AddAsync(client2);
                await arrangetContext.Clients.AddAsync(client3);
               
                await arrangetContext.SaveChangesAsync();

            }

            using (DatabaseContext assertContext = new DatabaseContext(contextOptions))
            {
                var SUT = new ClientService(assertContext);

                var result = await SUT.GetPageCountAsync(onepage);

                Assert.AreEqual(result, allPage);
            }
        }
    }
}
