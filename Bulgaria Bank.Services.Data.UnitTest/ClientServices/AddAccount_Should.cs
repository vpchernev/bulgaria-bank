﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Services.Data.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.ClientServices
{
    [TestClass]
    public class AddAccount_Should
    {
        [TestMethod]
        public async Task Return_ArgumentNullException_If_Account_Is_Null()
        {
            var databaseContextMock = new Mock<DatabaseContext>();

            var client = new Client();

            var SUT = new ClientService(databaseContextMock.Object);

            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(() => SUT.AddAccountAsync(client, null));
        }

        [TestMethod]
        public async Task Return_ArgumentNullException_If_Client_Is_Null()
        {
            var databaseContextMock = new Mock<DatabaseContext>();

            var account = new Account();

            var SUT = new ClientService(databaseContextMock.Object);

            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(() => SUT.AddAccountAsync(null, account));
        }


        [TestMethod]
        public async Task Return_New_Client_With_Attach_Account_If_Data_Is_Correct()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Return_New_Client_With_Attach_Account_If_Data_Is_Correct")
                .Options;

            var client = new Client();
            var account = new Account();

            Client result;

            using (var actContext = new DatabaseContext(contextOptions))
            {
                var SUT = new ClientService(actContext);

                result = await SUT.AddAccountAsync(client, account);
                await actContext.SaveChangesAsync();
            }

            Assert.IsNotNull(result);
            Assert.AreEqual(result.Accounts.Count(), 1);
            Assert.AreEqual(result, client);
            Assert.IsTrue(result.Accounts.Contains(account));
        }
    }
}
