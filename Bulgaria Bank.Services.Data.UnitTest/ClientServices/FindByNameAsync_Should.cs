﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Services.Data.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.ClientServices
{
    [TestClass]
    public class FindByNameAsync_Should
    {
        [TestMethod]
        public async Task Return_ArgumentNullException_If_Client_Name_Is_Null()
        {
            var databaseContextMock = new Mock<DatabaseContext>();

            var SUT = new ClientService(databaseContextMock.Object);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => SUT.FindByNameAsync(null));
        }
        [TestMethod]
        public async Task Return_ArgumentNullException_If_Client_Dosent_Exist_In_DatabaseContext()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Return_ArgumentNullException_If_Client_Is_Null")
                .Options;

            using (var assertContext = new DatabaseContext(contextOptions))
            {
                var SUT = new ClientService(assertContext);

                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(() => SUT.FindByNameAsync("asdsadjgij"));

            }
        }

        [TestMethod]
        public async Task Return_Client_If_Client_Exist_In_Database()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Return_Client_If_Client_Exist_In_Database")
                .Options;

            string clientName = "ClientName";

            var client = new Client() { Name = clientName };

            Client result;

            using (var actContext = new DatabaseContext(contextOptions))
            {
                await actContext.Clients.AddAsync(client);
                await actContext.SaveChangesAsync();
            }


            using (var assertContext = new DatabaseContext(contextOptions))
            {
                var SUT = new ClientService(assertContext);

                result = await SUT.FindByNameAsync(clientName);

            }

            Assert.IsNotNull(result);
            Assert.AreEqual(result.Name, clientName);

        }

    }
}
