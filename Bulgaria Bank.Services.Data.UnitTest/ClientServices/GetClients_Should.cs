﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.ClientServices
{
    [TestClass]
    public class GetClients_Should
    {
       [TestMethod]
        public async Task Return_Five_Client_OrderByDescending_By_Create_On()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
               .UseInMemoryDatabase(databaseName: "Return_Five_Acount_OrderBy_By_Name")
               .Options;

            string clientName1 = "Angel";
            string clientName2 = "Banana";
            string clientName3 = "Cruzer";
            string clientName4 = "Didoo";
            string clientName5 = "Evel";
            string clientName6 = "Fruty";

            var client1 = new Client() { Name = clientName1 };
            var client2 = new Client() { Name = clientName2 };
            var client3 = new Client() { Name = clientName3 };
            var client4 = new Client() { Name = clientName4 };
            var client5 = new Client() { Name = clientName5 };
            var client6 = new Client() { Name = clientName6 };
            
            int clientCountList = 5;

            int page = 1;

            ICollection<Client> result;

            using (var arrangeAContext = new DatabaseContext(contextOptions))
            {
                await arrangeAContext.Clients.AddAsync(client1);
                await arrangeAContext.Clients.AddAsync(client2);
                await arrangeAContext.Clients.AddAsync(client3);
                await arrangeAContext.Clients.AddAsync(client4);
                await arrangeAContext.Clients.AddAsync(client5);
                await arrangeAContext.Clients.AddAsync(client6);
                await arrangeAContext.SaveChangesAsync();
            }

            List<Client> resultList = new List<Client>();

            using (var assertContext = new DatabaseContext(contextOptions))
            {
                var SUT = new ClientService(assertContext);

                result = await SUT.GetClientsAsync(page);

                foreach (var client in result)
                {
                    resultList.Add(client);
                }

                Assert.IsNotNull(result);
                Assert.AreEqual(result.Count, clientCountList);
                Assert.AreEqual(resultList[0].Name, clientName1);
                Assert.AreEqual(resultList[1].Name, clientName2);
                Assert.AreEqual(resultList[2].Name, clientName3);
                Assert.AreEqual(resultList[3].Name, clientName4);
                Assert.AreEqual(resultList[4].Name, clientName5);
               
            }
        }
    }
}
