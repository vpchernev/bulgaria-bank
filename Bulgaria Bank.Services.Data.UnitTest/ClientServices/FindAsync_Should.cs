﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Services.Data.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.ClientServices
{
    [TestClass]
    public class FindAsync_Should
    {
        [TestMethod]
        public async Task Throw_ArgumentNullException_When_ClientId_Is_Null()
        {
            var databaseContextMock = new Mock<DatabaseContext>();

            var SUT = new ClientService(databaseContextMock.Object);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => SUT.FindAsync(null));

        }

        [TestMethod]
        public async Task Throw_ArgumentNullException_When_Client_Is_Null()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Throw_ArgumentNullException_When_Client_Is_Null")
                .Options;

            using (var assertContext = new DatabaseContext(contextOptions))
            {
                var SUT = new ClientService(assertContext);

                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(() => SUT.FindAsync("asdsadjgij"));

            }

        }

        [TestMethod]
        public async Task Return_Client_If_ClientId_Is_Correct()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Return_Client_If_ClientId_Is_Correct")
                .Options;

            var client = new Client();

            Client result;

            using (var actContext = new DatabaseContext(contextOptions) )
            {
                await actContext.Clients.AddAsync(client);
                await actContext.SaveChangesAsync();
            }


            using (var assertContext = new DatabaseContext(contextOptions))
            {
                var SUT = new ClientService(assertContext);

                result = await SUT.FindAsync(client.Id);

            }

            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, client.Id);

        }
    }
}
