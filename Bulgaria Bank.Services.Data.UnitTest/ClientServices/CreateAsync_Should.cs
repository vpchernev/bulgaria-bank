﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Data.Models.Identity;
using Bulgaria_Bank.Services.Data.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;


namespace Bulgaria_Bank.Services.Data.UnitTest.ClientServices
{
    [TestClass]
    public class CreateAsync_Should
    {
        [TestMethod]
        public async Task Throw_ArgumentNullException_When_Name_Is_Null()
        {
            var userName = "user1";

            var databaseContext = new Mock<DatabaseContext>();

            var SUT = new ClientService(databaseContext.Object);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => SUT.CreateAsync(userName));
        }

        [TestMethod]
        public async Task Throw_Exeption_If_Client_Already_Exist()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Throw_Exeption_If_Client_Already_Exist")
                .Options;

            string name = "clentName";

            var client = new Client() { Name = name};

           

            using (var assertContext = new DatabaseContext(contextOptions))
            {
                await assertContext.Clients.AddAsync(client);
                await assertContext.SaveChangesAsync();
            }

            using (var assertContext = new DatabaseContext(contextOptions))
            {
                var SUT = new ClientService(assertContext);

                await Assert.ThrowsExceptionAsync<DuplicateEntityException>(() => SUT.CreateAsync(name));
            }

        }

        [TestMethod]
        public async Task Return_New_Client_If_All_Data_Is_Correct()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Return_New_Client_If_All_Data_Is_Correct")
                .Options;

            string name = "clentName";
            Client result;

            using (var actContext = new DatabaseContext(contextOptions))
            {
                var SUT = new ClientService(actContext);

                result = await SUT.CreateAsync(name);
               
            }

            using (var assertContext = new DatabaseContext(contextOptions))
            {
                Assert.IsNotNull(result);
                Assert.IsTrue(await assertContext.Clients.AnyAsync(a => a.Name == result.Name));
            }

        }

    }
}
