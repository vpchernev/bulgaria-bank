﻿using Bulgaria_Bank.Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bulgaria_Bank.Services.Data.UnitTest.Utils
{
    public static class TestUtils
    {
        public static DbContextOptions<DatabaseContext> GetOptions(string databaseName)
        {
            var provider = new ServiceCollection()
                          .AddEntityFrameworkInMemoryDatabase()
                          .BuildServiceProvider();

            return new DbContextOptionsBuilder<DatabaseContext>()
                 .UseInMemoryDatabase(databaseName)
                 .UseInternalServiceProvider(provider)
                 .Options;

        }
    }
}
