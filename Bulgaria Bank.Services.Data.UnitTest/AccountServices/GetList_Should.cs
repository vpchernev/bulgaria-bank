﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Data.Models.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.AccountServices
{
    [TestClass]
    public class GetList_Should
    {
        [TestMethod]
        public async Task Return_Five_Acount_OrderByDescending_By_Create_On()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
               .UseInMemoryDatabase(databaseName: "Return_Five_Acount_OrderByDescending_By_Create_On")
               .Options;


            var client = new Client();
            var user = new User();
            int acountCountList = 5;
            string accountNumber2 = "1000000002";
            string accountNumber3 = "1000000003";
            string accountNumber4 = "1000000004";
            string accountNumber5 = "1000000005";
            string accountNumber6 = "1000000006";


            using (var arrangeAContext = new DatabaseContext(contextOptions))
            {
                await arrangeAContext.Clients.AddAsync(client);
                await arrangeAContext.Users.AddAsync(user);
                await arrangeAContext.SaveChangesAsync();
            }

            int page = 1;
            
            ICollection<Account> result;
            
            var account1 = new Account() { ClientId = client.Id };
            var account2 = new Account() { ClientId = client.Id, AccountNumber = accountNumber2, CreatedOn = DateTime.Parse("12/12/2015") };
            var account3 = new Account() { ClientId = client.Id, AccountNumber = accountNumber3,  CreatedOn = DateTime.Parse("12/12/2016") };
            var account4 = new Account() { ClientId = client.Id, AccountNumber = accountNumber4, CreatedOn = DateTime.Parse("12/12/2017") };
            var account5 = new Account() { ClientId = client.Id, AccountNumber = accountNumber5, CreatedOn = DateTime.Parse("12/12/2018") };
            var account6 = new Account() { ClientId = client.Id, AccountNumber = accountNumber6, CreatedOn = DateTime.Parse("12/12/2019") };

            using (var arrangeBContext = new DatabaseContext(contextOptions))
            {
                await arrangeBContext.Accounts.AddAsync(account1);
                await arrangeBContext.Accounts.AddAsync(account2);
                await arrangeBContext.Accounts.AddAsync(account3);
                await arrangeBContext.Accounts.AddAsync(account4);
                await arrangeBContext.Accounts.AddAsync(account5);
                await arrangeBContext.Accounts.AddAsync(account6);
                await arrangeBContext.SaveChangesAsync();

            }

            var userAcount1 = new UserAccount();
            var userAcount2 = new UserAccount() { UserId = user.Id, AccountId = account2.Id };
            var userAcount3 = new UserAccount() { UserId = user.Id, AccountId = account3.Id };
            var userAcount4 = new UserAccount() { UserId = user.Id, AccountId = account4.Id };
            var userAcount5 = new UserAccount();
            var userAcount6 = new UserAccount() { UserId = user.Id, AccountId = account6.Id };

            using (var actContext = new DatabaseContext(contextOptions))
            {
               
                await actContext.UserAccounts.AddAsync(userAcount1);
                await actContext.UserAccounts.AddAsync(userAcount2);
                await actContext.UserAccounts.AddAsync(userAcount3);
                await actContext.UserAccounts.AddAsync(userAcount4);
                await actContext.UserAccounts.AddAsync(userAcount5);
                await actContext.UserAccounts.AddAsync(userAcount6);

                await actContext.SaveChangesAsync();

            }

            List<Account> resultList = new List<Account>();
            using (var assertContext = new DatabaseContext(contextOptions))
            {
                var SUT = new AccountService(assertContext);

                result = await SUT.GetListAsync(page, client.Id);

                foreach (var account in result)
                {
                    resultList.Add(account);
                }

                Assert.IsNotNull(result);
                Assert.AreEqual(result.Count, acountCountList);
                Assert.AreEqual(resultList[0].AccountNumber, accountNumber6);
                Assert.AreEqual(resultList[1].AccountNumber, accountNumber5);
                Assert.AreEqual(resultList[2].AccountNumber, accountNumber4);
                Assert.AreEqual(resultList[3].AccountNumber, accountNumber3);
                Assert.AreEqual(resultList[4].AccountNumber, accountNumber2);
                Assert.IsTrue(resultList[2].AccountNumber == accountNumber4 && resultList[2].UserAccounts.Count == 1);
                Assert.IsTrue(resultList[3].AccountNumber == accountNumber3 && resultList[3].UserAccounts.Count == 1);
                Assert.IsTrue(resultList[4].AccountNumber == accountNumber2 && resultList[4].UserAccounts.Count == 1);
                Assert.IsTrue(resultList[1].AccountNumber == accountNumber5 && resultList[1].UserAccounts.Count == 0);
            }
        }
    }
}