﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Services.Data.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.AccountServices
{
    [TestClass]
    public class CreateAsync_Should
    {
        [TestMethod]
        public async Task ThrowArugmentNullException_When_Client_Is_Null()
        {
           
            Mock<DatabaseContext> dataContext = new Mock<DatabaseContext>();

            var SUT = new AccountService (dataContext.Object);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(
                () => SUT.CreateAsync(null, 20));
        }


        [TestMethod]
        public async Task ReturnAccount_WhenPassedValidParameters()
        {
            
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "ReturnAccount_WhenPassedValidParameters")
                .Options;

            var client = new Client();

            Account result;

            decimal validDouble = 200.20M;
           
            using (DatabaseContext actContext = new DatabaseContext(contextOptions))
            {
                await actContext.Clients.AddAsync(client);
                await actContext.SaveChangesAsync();

                AccountService SUT = new AccountService(actContext);

                 result = await SUT.CreateAsync(client, validDouble);
            }

            
            using (DatabaseContext assertContext = new DatabaseContext(contextOptions))
            {
                Assert.IsNotNull(result);

                Assert.IsTrue(assertContext.Accounts.Any(a => a.Id.Equals(result.Id)));
                Assert.IsTrue(assertContext.Accounts.Any(a => a.AccountNumber.Equals(result.AccountNumber)));
                Assert.IsTrue(assertContext.Accounts.Any(a => a.ClientId.Equals(result.ClientId)));
                Assert.IsTrue(assertContext.Accounts.Any(a => a.CurrentBalance.Equals(result.CurrentBalance)));
                Assert.IsTrue(assertContext.Accounts.Any(a => a.CreatedOn.Equals(result.CreatedOn)));

                Assert.IsTrue(assertContext.Accounts.Any(a => a.CurrentBalance.Equals(validDouble)));
                Assert.IsTrue(assertContext.Clients.Any(c => c.Accounts.Contains(result)));
              
            }
        }

    }


}
