﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Data.Models.Identity;
using Bulgaria_Bank.Services.Data.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.AccountServices
{
    [TestClass]
    public class AttachUser_Should
    {
        [TestMethod]
        public async Task Return_Exeption_When_Account_Is_Null()
        {
            Mock<DatabaseContext> dataContext = new Mock<DatabaseContext>();

            var user = new User();

            var SUT = new AccountService(dataContext.Object);

            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                () => SUT.AttachUserAsync(null, user));
        }

        [TestMethod]
        public async Task Return_Exeption_When_User_Is_Null()
        {
            Mock<DatabaseContext> dataContext = new Mock<DatabaseContext>();

            var account = new Account();

            var SUT = new AccountService(dataContext.Object);

            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                () => SUT.AttachUserAsync(account, null));
        }

        [TestMethod]
        public async Task Throw_Exeption_If_User_Is_Already_Attach_To_Account()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Throw_Exeption_If_User_Is_Already_Attach_To_Account")
                .Options;

            string user1 = "user1";

            var user = new User() { Username = user1 };

            var account = new Account();

            var userAccount = new UserAccount()
            {
                User = user,
                UserId = user.Id,
                Account = account,
                AccountId = account.Id
            };

            using (var actContext = new DatabaseContext(contextOptions))
            {
                await actContext.UserAccounts.AddAsync(userAccount);
                await actContext.SaveChangesAsync();
            }

            using (var assertContext = new DatabaseContext(contextOptions))
            {
                var SUT = new AccountService(assertContext);
                await Assert.ThrowsExceptionAsync<DuplicateEntityException>(() => SUT.AttachUserAsync(account, user));
            }

        }


        [TestMethod]
        public async Task Correctly_Attach_User_To_Account()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Correctly_Attach_User_To_Account")
                .Options;

            var user = new User() {};

            Account result;
            var account = new Account() {};

            using (var actContext = new DatabaseContext(contextOptions))
            {
                var SUT = new AccountService(actContext);

                result = await SUT.AttachUserAsync(account, user);
            }

            using (var assertContext = new DatabaseContext(contextOptions))
            {
                Assert.IsNotNull(result);
                Assert.IsTrue(await assertContext.UserAccounts.AnyAsync(ua => ua.AccountId == result.Id && ua.UserId == user.Id));
            }

        }
    }
}
