﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Data.Models.Identity;
using Bulgaria_Bank.Services.Data.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.AccountServices
{
    [TestClass]
    public class UnattachUser_Should
    {

        [TestMethod]
        public async Task Return_Exeption_When_Account_Is_Null()
        {
            Mock<DatabaseContext> dataContext = new Mock<DatabaseContext>();

            var user = new User();

            var SUT = new AccountService(dataContext.Object);

            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                () => SUT.UnattachUserAsync(null, user));
        }

        [TestMethod]
        public async Task Return_Exeption_When_User_Is_Null()
        {
            Mock<DatabaseContext> dataContext = new Mock<DatabaseContext>();

            var account = new Account();

            var SUT = new AccountService(dataContext.Object);

            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                () => SUT.UnattachUserAsync(account, null));
        }

        [TestMethod]
        public async Task Throw_Exeption_If_There_Is_No_Relation_Between_User_And_Account()
        {
            Mock<DatabaseContext> dataContext = new Mock<DatabaseContext>();

            var account = new Account();

            var user = new User();

            var SUT = new AccountService(dataContext.Object);

            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                () => SUT.UnattachUserAsync(account, user));

        }


        [TestMethod]
        public async Task Correctly_UnattachAttach_User_To_Account()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Correctly_UnattachAttach_User_To_Account")
                .Options;

            Account result;

            var account = new Account();
            var user = new User();

            var userAccount = new UserAccount()
            {
                User = user,
                UserId = user.Id,
                Account = account,
                AccountId = account.Id
            };

            using (var assertContext = new DatabaseContext(contextOptions))
            {
               
                await assertContext.UserAccounts.AddAsync(userAccount);
                await assertContext.SaveChangesAsync();
            }

            using (var actContext = new DatabaseContext(contextOptions))
            {
                var SUT = new AccountService(actContext);

                result = await SUT.UnattachUserAsync(account, user);
            }

            using (var assertContext = new DatabaseContext(contextOptions))
            {
                Assert.IsNotNull(result);
                Assert.IsFalse(await assertContext.UserAccounts.AnyAsync(ua => ua.AccountId == result.Id && ua.UserId == user.Id));
            }

        }

    }
}
