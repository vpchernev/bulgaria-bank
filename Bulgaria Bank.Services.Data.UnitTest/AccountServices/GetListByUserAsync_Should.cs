﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Data.Models.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.AccountServices
{
    [TestClass]
    public class GetListByUserAsync_Should
    {
        [TestMethod]
        public async Task Return_Correct_List_Of_Accounts()
        {

            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Return_Correct_List_Of_Accounts")
                .Options;

            var user = new User() {FirstName = "userFirst", LastName = "userLast", Username = "userUser" };

            string accountNumber1 = "1000000000";
            string accountNumber2 = "1000000000";
            string accountNumber3 = "1000000000";

            var account1 = new Account() { AccountNumber = accountNumber1, CurrentBalance = 500M};
            var account2 = new Account() { AccountNumber = accountNumber2, CurrentBalance = 200M };
            var account3 = new Account() { AccountNumber = accountNumber3, CurrentBalance = 300M };

            var client = new Client() { Accounts = { account1, account2, account3}};

            ICollection<Account> result;

            var userAccount1 = new UserAccount()
            {
                User = user,
                UserId = user.Id,
                Account = account1,
                AccountId = account1.Id
            };

            var userAccount2 = new UserAccount()
            {
                User = user,
                UserId = user.Id,
                Account = account2,
                AccountId = account2.Id
            };
            var userAccount3 = new UserAccount()
            {
                User = user,
                UserId = user.Id,
                Account = account3,
                AccountId = account3.Id
            };

            using (DatabaseContext arrangetContext = new DatabaseContext(contextOptions))
            {
                await arrangetContext.Clients.AddAsync(client);
                await arrangetContext.Users.AddAsync(user);
                await arrangetContext.UserAccounts.AddAsync(userAccount1);
                await arrangetContext.UserAccounts.AddAsync(userAccount2);
                await arrangetContext.UserAccounts.AddAsync(userAccount3);
                await arrangetContext.SaveChangesAsync();
                
            }

            using (DatabaseContext actContext = new DatabaseContext(contextOptions))
            {
                var SUT = new AccountService(actContext);

                result = await SUT.GetListByUserAsync(user.Id);

                Assert.AreEqual(result.Count, 3);
                Assert.IsTrue(result.Any(r => r.AccountNumber == accountNumber1));
                Assert.IsTrue(result.Any(r => r.AccountNumber == accountNumber2));
                Assert.IsTrue(result.Any(r => r.AccountNumber == accountNumber3));

            }

        }
    
    }
}
