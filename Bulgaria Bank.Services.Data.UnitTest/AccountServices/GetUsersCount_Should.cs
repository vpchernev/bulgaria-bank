﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Data.Models.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.AccountServices
{
    [TestClass]
    public class GetUsersCount_Should
    {
        [TestMethod]
        public async Task Return_Correct_UserAccounts_Count()
        {

            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Return_Correct_UserAccounts_Count")
                .Options;

            var account = new Account();

            var user1 = new User();
            var user2 = new User();
            var user3 = new User();

            var userAcount1 = new UserAccount() { User = user1, UserId = user1.Id, Account = account, AccountId = account.Id };
            var userAcount2 = new UserAccount() { User = user2, UserId = user2.Id, Account = account, AccountId = account.Id };
            var userAcount3 = new UserAccount() { User = user3, UserId = user3.Id, Account = account, AccountId = account.Id };

            using (DatabaseContext arrangetContext = new DatabaseContext(contextOptions))
            {
                await arrangetContext.Accounts.AddAsync(account);
                await arrangetContext.Users.AddAsync(user1);
                await arrangetContext.Users.AddAsync(user2);
                await arrangetContext.Users.AddAsync(user3);
                await arrangetContext.UserAccounts.AddAsync(userAcount1);
                await arrangetContext.UserAccounts.AddAsync(userAcount2);
                await arrangetContext.UserAccounts.AddAsync(userAcount3);
                await arrangetContext.SaveChangesAsync();

            }

            using (DatabaseContext assertContext = new DatabaseContext(contextOptions))
            {
                var SUT = new AccountService(assertContext);

                var result = await SUT.GetUsersCountAsync(account.Id);

                Assert.AreEqual(result, 3);
            }
        }

        [TestMethod]
        public static void GetAccountPages_Should_Return_Correct_Page_Numbers()
        {
            int count = 25;
            int onPageAccount = 5;

            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
               .UseInMemoryDatabase(databaseName: "GetAccountPages_Should_Return_Correct_Page_Numbers")
               .Options;

            using (DatabaseContext assertContext = new DatabaseContext(contextOptions))
            {
                var SUT = new AccountService(assertContext);

                var result = SUT.GetAccountPages(onPageAccount, count);

                Assert.AreEqual(result, 4);
            }
        }

    }
}
