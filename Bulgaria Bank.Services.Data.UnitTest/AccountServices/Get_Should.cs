﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Services.Data.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.AccountServices
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public async Task Return_Correct_Account_If_Found_It()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Return__Account_With_Correct_Data")
                .Options;

            string accountNumber = "1000000000";
            decimal currentBalance = 500M;
            Account result;

            var account = new Account() { AccountNumber = accountNumber, CurrentBalance = currentBalance };

            using (var arrangeContext = new DatabaseContext(contextOptions))
            {
                await arrangeContext.Accounts.AddAsync(account);
                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new DatabaseContext(contextOptions))
            {
                var SUT = new AccountService(actContext);

                result = await SUT.GetByNumberAsync(accountNumber);
            }

            Assert.IsNotNull(result);
            Assert.AreEqual(result.AccountNumber, accountNumber);
            Assert.AreEqual(result.CurrentBalance, currentBalance);
        }

        [TestMethod]
        public async Task Throw_Exeption_If_AccountNumber_Is_Null()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Return__Account_With_Correct_Data")
                .Options;

            using (var assertContext = new DatabaseContext(contextOptions))
            {
                var SUT = new AccountService(assertContext);
                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(() => SUT.GetByNumberAsync(null)); 

            }
        }

    }
}
