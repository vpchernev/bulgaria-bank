﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Data.Models.Identity;
using Bulgaria_Bank.Services.Data.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.AccountServices
{
    [TestClass]
    public class GetWithAllUsers_Should
    {
        [TestMethod]
        public async Task Trow_Exeption_When_Account_Was_Not_Found()
        {

            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Trow_Exeption_When_Account_Was_Not_Found")
                .Options;

            string invalidAccountNumber = "123adsad-23123fsdf";

            using (DatabaseContext actContext = new DatabaseContext(contextOptions))
            {
                var SUT = new AccountService(actContext);

                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(() => SUT.GetAsync(invalidAccountNumber));

            }
        }

        [TestMethod]
        public async Task Return_Account_With_All_User_That_Managed_Him_When_Input_Correct_Id()
        {

            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                  .UseInMemoryDatabase(databaseName: "Return_Account_With_All_User_That_Managed_Him_When_Input_Correct_Id")
                  .Options;

            string userFirst1 = "user1First", userFirst2 = "user2First", userFirst3 = "user3First";
            string userLast1 = "user1Last", userLast2 = "user2Last", userLast3 = "user3Last";
            string userUser1 = "user1User", userUser2 = "user2User", userUser3 = "user3User";

            string accountNumber = "1000000000";
            decimal balance = 20.30M;

            Account result;

            var user1 = new User() { FirstName = userFirst1, LastName = userLast1, Username = userUser1 };
            var user2 = new User() { FirstName = userFirst2, LastName = userLast2, Username = userUser2 };
            var user3 = new User() { FirstName = userFirst3, LastName = userLast3, Username = userUser3 };

            var account1 = new Account() { AccountNumber = accountNumber, CurrentBalance = balance };

            var userAccount1 = new UserAccount()
            {
                User = user1,
                UserId = user1.Id,
                Account = account1,
                AccountId = account1.Id
            };
            var userAccount2 = new UserAccount()
            {
                User = user2,
                UserId = user2.Id,
                Account = account1,
                AccountId = account1.Id
            };
            var userAccount3 = new UserAccount()
            {
                User = user3,
                UserId = user3.Id,
                Account = account1,
                AccountId = account1.Id
            };

            using (DatabaseContext assertContext = new DatabaseContext(contextOptions))
            {
                await assertContext.Accounts.AddAsync(account1);
                await assertContext.Users.AddAsync(user1);
                await assertContext.Users.AddAsync(user2);
                await assertContext.Users.AddAsync(user3);
                await assertContext.UserAccounts.AddAsync(userAccount1);
                await assertContext.UserAccounts.AddAsync(userAccount2);
                await assertContext.UserAccounts.AddAsync(userAccount3);
                await assertContext.SaveChangesAsync();

            }

            using (DatabaseContext actContext = new DatabaseContext(contextOptions))
            {
                AccountService SUT = new AccountService(actContext);

                result = await SUT.GetWithAllUsersAsync(accountNumber);

                Assert.IsNotNull(result);
                Assert.AreEqual(result.AccountNumber, account1.AccountNumber);
                Assert.AreEqual(result.CurrentBalance, balance);
                Assert.IsTrue(actContext.UserAccounts.Any(u => u.Account.Id == result.Id && u.User.Id == user1.Id));
                Assert.IsTrue(actContext.UserAccounts.Any(u => u.Account.Id == result.Id && u.User.Id == user2.Id));
                Assert.IsTrue(actContext.UserAccounts.Any(u => u.Account.Id == result.Id && u.User.Id == user3.Id));

            }
        }
    }
}
