﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Data.Models.Identity;
using Bulgaria_Bank.Services.Data.Exceptions;
using Bulgaria_Bank.Services.Data.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.AccountServices
{
    [TestClass]
    public class GetAsync_Should
    {
        [TestMethod]
        public async Task Return__Account_With_Correct_Data()
        {

            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Return__Account_With_Correct_Data")
                .Options;

            var accountReceiver = new Account() { AccountNumber = "1000000000", CurrentBalance = 500M };

            var transact = new Transaction() { ReceiverAccount = accountReceiver };

            var transactionCollection = new List<Transaction>() { transact };

            var client = new Client() { Name = "Client1" };

            var finalAccount = new Account { AccountNumber = "1000000009", CurrentBalance = 1.5M, ReceiverTransactions = transactionCollection, Client = client, ClientId = client.Id };

            var user = new User() { FirstName = "userFirst", LastName = "userLast", Username = "userUser" };

            Account result;

            var userAcount = new UserAccount()
            {
                Account = finalAccount,
                AccountId = finalAccount.Id,
                User = user,
                UserId = user.Id
            };


            using (DatabaseContext arrangetContext = new DatabaseContext(contextOptions))
            {
                await arrangetContext.Users.AddAsync(user);
                await arrangetContext.Accounts.AddAsync(accountReceiver);
                await arrangetContext.Transactions.AddAsync(transact);
                await arrangetContext.Clients.AddAsync(client);
                await arrangetContext.Accounts.AddAsync(finalAccount);
                await arrangetContext.UserAccounts.AddAsync(userAcount);
                await arrangetContext.SaveChangesAsync();

            }

            using (DatabaseContext actContext = new DatabaseContext(contextOptions))
            {
                var SUT = new AccountService(actContext);

                result = await SUT.GetAsync(finalAccount.Id);

                Assert.IsNotNull(result);
                Assert.IsInstanceOfType(result, typeof(Account));
                Assert.AreEqual(result.AccountNumber, "1000000009");
                Assert.AreEqual(result.CurrentBalance, 1.5M);
                Assert.AreEqual(result.ClientId, client.Id);
                Assert.AreEqual(result.UserAccounts.Count, 1);
                Assert.AreEqual(result.ReceiverTransactions.Count, 1);

            }

        }

        [TestMethod]
        public async Task Trow_Exeption_When_Account_Was_Not_Found()
        {

            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Trow_Exeption_When_Account_Was_Not_Found")
                .Options;

            string invalidId = "123adsad";

            using (DatabaseContext actContext = new DatabaseContext(contextOptions))
            {
                var SUT = new AccountService(actContext);

                 await Assert.ThrowsExceptionAsync<EntityNotFoundException>(() => SUT.GetAsync(invalidId));

            }
        }
    }
}
