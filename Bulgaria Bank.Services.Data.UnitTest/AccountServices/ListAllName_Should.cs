﻿using Bulgaria_Bank.Data.Context;
using Bulgaria_Bank.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.Services.Data.UnitTest.AccountServices
{
    [TestClass]
    public class ListAllName_Should
    {
        [TestMethod]
        public async Task Returnt_Correct_List_Of_Existing_Account_Number_That_Match_The_Expression_Rule()
        {
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
               .UseInMemoryDatabase(databaseName: "Returnt_Correct_List_Of_Existing_Account_Number_That_Match_The_Expression_Rule")
               .Options;

            IEnumerable<string> result;

            string clientName = "client1";
            string accountNumber1 = "1001111111";
            string accountNumber2 = "1002222222";
            string accountNumber3 = "2544444444";
            string term = "100";

            
            var account1 = new Account() { AccountNumber = accountNumber1 };
            var account2 = new Account() { AccountNumber = accountNumber2 };
            var account3 = new Account() { AccountNumber = accountNumber3 };

            var client = new Client() { Name = clientName, Accounts = new List<Account>() { account1, account2, account2 } };

            using (var arrangeContext = new DatabaseContext(contextOptions))
            {
                await arrangeContext.Clients.AddAsync(client);
                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new DatabaseContext(contextOptions))
            {
                var SUT = new AccountService(actContext);

                result = await SUT.ListAllNumbersAsync(clientName, m => m.StartsWith(term));
            }

            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count(), 2);
            Assert.IsTrue(result.Contains(accountNumber1));
            Assert.IsTrue(result.Contains(accountNumber2));
        }
    }
}
