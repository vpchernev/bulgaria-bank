﻿using Bulgaria_Bank.Areas.Administration.Contorllers;
using Bulgaria_Bank.Areas.Administration.Models;
using Bulgaria_Bank.Data.Models.Identity;
using Bulgaria_Bank.Services.Data.Contracts;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.UnitTest.Areas.Administration.Controllers.Accounts
{
    [TestClass]
    public class Login_Should
    {
        [TestMethod]
        public async Task Redirect_Correct_To_Index_()
        {
            var adminServiceMock = new Mock<IAdminService>();
            var claimServiceMoCk = new Mock<IClaimService>();

            string validUserName = "validUserName";
            string validPassword = "validPassword";
            string validId = "validId";

            var admin = new Admin()
            {
                Username = validUserName,
                PasswordHash = validPassword,
                Id = validId

            };

            var claims = new List<Claim>
            {
                    new Claim(ClaimTypes.NameIdentifier, admin.Id),
                    new Claim(ClaimTypes.Name, admin.Username),
                    new Claim(ClaimTypes.Role, "Administrator")
            };

            var claimsIdentity = new ClaimsIdentity(
                    claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var authProperty = new AuthenticationProperties()
            {
                IsPersistent = true,
                ExpiresUtc = DateTime.UtcNow.AddMinutes(20)
            };

            var validLoginViewModel = new AdminLoginModel()
            {
                UserName = validUserName,
                PasswordHash = validPassword,

            };

            adminServiceMock.Setup(a => a.AuthenticateAsync(
                validUserName, validPassword)).ReturnsAsync(admin);

            claimServiceMoCk.Setup(c => c.CreateAdminClaims(admin)).Returns(claims);
            claimServiceMoCk.Setup(c => c.CreateClaimsIdentity(claims)).Returns(claimsIdentity);
            claimServiceMoCk.Setup(c => c.CreateAuthenticationProperties()).Returns(authProperty);

            var SUT = new AccountController(
                adminServiceMock.Object,
                claimServiceMoCk.Object);

            SUT.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    RequestServices = GetServiceProviderMock().Object
                }
            };

            var actionResult = await SUT.Login(validLoginViewModel);

            var result = (RedirectToActionResult)actionResult;


            Assert.AreEqual("Index", result.ActionName);
            Assert.AreEqual("Home", result.ControllerName);
            Assert.IsInstanceOfType(result, typeof(IActionResult));
        }

        private Mock<IServiceProvider> GetServiceProviderMock()
        {
            var authServiceMock = new Mock<IAuthenticationService>();
            authServiceMock
                .Setup(_ => _.SignInAsync(It.IsAny<HttpContext>(), It.IsAny<string>(), It.IsAny<ClaimsPrincipal>(), It.IsAny<AuthenticationProperties>()))
                .Returns(Task.FromResult((object)null));

            var urlHelperFactory = new Mock<IUrlHelperFactory>();            

            var serviceProviderMock = new Mock<IServiceProvider>();
            serviceProviderMock
                .Setup(s => s.GetService(typeof(IUrlHelperFactory)))
                .Returns(urlHelperFactory.Object);

            serviceProviderMock
                .Setup(_ => _.GetService(typeof(IAuthenticationService)))
                .Returns(authServiceMock.Object);

            return serviceProviderMock;
        }
    }

    
}

