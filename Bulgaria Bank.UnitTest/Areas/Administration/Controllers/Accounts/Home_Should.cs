﻿using AutoMapper;
using Bulgaria_Bank.Areas.Administration.Contorllers;
using Bulgaria_Bank.Areas.Administration.Models;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Data.Models.Identity;
using Bulgaria_Bank.Services.Data.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bulgaria_Bank.UnitTest.Areas.Administration.Controllers.Accounts
{
    [TestClass]
    public class Home_Should
    {
        static string clientName1 = "Name";
        static string clientName2 = "Name2";

        static string clientInvalidName = "A";
        static decimal currentBalance = 550.20M;

        static string vmName = "vmName";
        static int negative = -1;

        static Client clientValid1 = new Client() { Name = clientName1 };
        static Client clientValid2 = new Client() { Name = clientName2 };

        static User user = new User();

        static Account account = new Account() { CurrentBalance = currentBalance };
        static Account account2 = new Account() { CurrentBalance = currentBalance };
        static Account account3 = new Account() { CurrentBalance = currentBalance };

        static List<Account> accountList = new List<Account>() { account, account2, account3 };

        static List<Client> clientList = new List<Client>() { clientValid1, clientValid2 };

        static ClientAddViewModel clientValidVM = new ClientAddViewModel() { Name = vmName };
        static ClientAddViewModel clientInvalidVM = new ClientAddViewModel() { Name = clientInvalidName };

        static AccountAddViewModel accountValidVM = new AccountAddViewModel { };
        static AccountViewModel accountValidModel = new AccountViewModel();

        static AdminHomeViewModel adminValidVM = new AdminHomeViewModel();
        static AdminHomeViewModel adminInvalidVM = new AdminHomeViewModel();

        static UserAddViewModel userAddViewModel = new UserAddViewModel();

        [TestMethod]
        public async Task Index_Return_Correct_View()
        {

            var clientList = new List<Client>() { clientValid1, clientValid2 };

            var vm = new ClientViewModel() { Name = vmName };

            var clientServiceMock = new Mock<IClientService>();
            var mapperMock = new Mock<IMapper>();
            var userServiceMock = new Mock<IUserService>();
            var accountServiceMock = new Mock<IAccountService>();

            mapperMock.Setup(m => m.Map<ClientViewModel>(clientName1)).Returns(vm);

            clientServiceMock.Setup(c => c.GetClientsAsync(1)).ReturnsAsync(clientList);

            var SUT = new HomeController(clientServiceMock.Object, mapperMock.Object, accountServiceMock.Object, userServiceMock.Object);

            var result = await SUT.Index(null);

            Assert.IsInstanceOfType(result, typeof(ViewResult));

            var viewResult = (ViewResult)result;

            Assert.IsInstanceOfType(viewResult.Model, typeof(AdminHomeViewModel));

        }

        [TestMethod]
        public async Task CreateClient_Return_Success_OK_If_No_Exeptions_Was_Throw()
        {
            var clientServiceMock = new Mock<IClientService>();
            var mapperMock = new Mock<IMapper>();
            var userServiceMock = new Mock<IUserService>();
            var accountServiceMock = new Mock<IAccountService>();

            clientServiceMock.Setup(u => u.CreateAsync(It.IsAny<string>())).ReturnsAsync(clientValid1);

            var SUT = new HomeController(clientServiceMock.Object, mapperMock.Object, accountServiceMock.Object, userServiceMock.Object);

            var result = await SUT.CreateClient(clientValidVM);

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
        }

        [TestMethod]
        public async Task CreateClient_Return_Exeption_ClientService_Throw_Exeption()
        {
            var clientServiceMock = new Mock<IClientService>();
            var mapperMock = new Mock<IMapper>();
            var userServiceMock = new Mock<IUserService>();
            var accountServiceMock = new Mock<IAccountService>();

            clientServiceMock.Setup(u => u.CreateAsync(clientInvalidName)).Throws(new ArgumentException());

            var SUT = new HomeController(clientServiceMock.Object, mapperMock.Object, accountServiceMock.Object, userServiceMock.Object);

            var result = await SUT.CreateClient(clientInvalidVM);

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public async Task CreateClientAccount_Return_Exeption_If_ClientService_Throw_One()
        {
            var clientServiceMock = new Mock<IClientService>();
            var mapperMock = new Mock<IMapper>();
            var userServiceMock = new Mock<IUserService>();
            var accountServiceMock = new Mock<IAccountService>();

            clientServiceMock.Setup(u => u.FindByNameAsync(clientInvalidName)).Throws(new ArgumentException());

            var SUT = new HomeController(clientServiceMock.Object, mapperMock.Object, accountServiceMock.Object, userServiceMock.Object);

            var result = await SUT.CreateClientAccount(accountValidVM);

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public async Task CreateClientAccount_Return_Exeption_If_AccountService_Throw_One()
        {
            var clientServiceMock = new Mock<IClientService>();
            var mapperMock = new Mock<IMapper>();
            var userServiceMock = new Mock<IUserService>();
            var accountServiceMock = new Mock<IAccountService>();

            accountServiceMock.Setup(u => u.CreateAsync(null, negative)).Throws(new ArgumentException());

            var SUT = new HomeController(clientServiceMock.Object, mapperMock.Object, accountServiceMock.Object, userServiceMock.Object);

            var result = await SUT.CreateClientAccount(accountValidVM);

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public async Task CreateClientAccount_Return_Success_OK_If_No_Exeptions_Was_Throw()
        {
            var clientServiceMock = new Mock<IClientService>();
            var mapperMock = new Mock<IMapper>();
            var userServiceMock = new Mock<IUserService>();
            var accountServiceMock = new Mock<IAccountService>();

            clientServiceMock.Setup(u => u.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(clientValid1);
            accountServiceMock.Setup(u => u.CreateAsync(It.IsAny<Client>(), It.IsAny<decimal>())).ReturnsAsync(account);

            var SUT = new HomeController(clientServiceMock.Object, mapperMock.Object, accountServiceMock.Object, userServiceMock.Object);

            var result = await SUT.CreateClientAccount(accountValidVM);

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
        }

        [TestMethod]
        public async Task GetAccounts_Return_ClientAccountTablePartial_If_No_Exeptions_Was_Throw()
        {
            var clientServiceMock = new Mock<IClientService>();
            var mapperMock = new Mock<IMapper>();
            var userServiceMock = new Mock<IUserService>();
            var accountServiceMock = new Mock<IAccountService>();
              
            clientServiceMock.Setup(c => c.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(clientValid1);

            accountServiceMock.Setup(a => a.GetAccountPages(It.IsAny<int>(), It.IsAny<int>())).Returns(5);
            accountServiceMock.Setup(a => a.GetListAsync(It.IsAny<int>(), It.IsAny<string>())).ReturnsAsync(accountList);

            var SUT = new HomeController(clientServiceMock.Object, mapperMock.Object, accountServiceMock.Object, userServiceMock.Object);

            var result = await SUT.GetAccounts(null, clientName1);

            Assert.IsInstanceOfType(result, typeof(PartialViewResult));

            var viewResult = (PartialViewResult)result;

            Assert.IsInstanceOfType(viewResult.Model, typeof(AccountListViewModel));
        }

        [TestMethod]
        public async Task GetClients_Return_ClientsTablePartial_If_No_Exeptions_Was_Throw()
        {
            var clientServiceMock = new Mock<IClientService>();
            var mapperMock = new Mock<IMapper>();
            var userServiceMock = new Mock<IUserService>();
            var accountServiceMock = new Mock<IAccountService>();

            clientServiceMock.Setup(c => c.GetPageCountAsync(It.IsAny<int>())).ReturnsAsync(5);
            clientServiceMock.Setup(c => c.GetClientsAsync(It.IsAny<int>())).ReturnsAsync(clientList);

            var SUT = new HomeController(clientServiceMock.Object, mapperMock.Object, accountServiceMock.Object, userServiceMock.Object);

            var result = await SUT.GetClients(null);

            Assert.IsInstanceOfType(result, typeof(PartialViewResult));

            var viewResult = (PartialViewResult)result;

            Assert.IsInstanceOfType(viewResult.Model, typeof(ClientsListViewModel));
        }

        [TestMethod]
        public async Task CreateUser_Return_Success_OK_If_No_Exeptions_Was_Throw()
        {
            var clientServiceMock = new Mock<IClientService>();
            var mapperMock = new Mock<IMapper>();
            var userServiceMock = new Mock<IUserService>();
            var accountServiceMock = new Mock<IAccountService>();

            userServiceMock.Setup(u => u.CreateAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(user);

            var SUT = new HomeController(clientServiceMock.Object, mapperMock.Object, accountServiceMock.Object, userServiceMock.Object);

            var result = await SUT.CreateUser(userAddViewModel);

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
        }
    }

}
