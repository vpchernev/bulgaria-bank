﻿using AutoMapper;
using Bulgaria_Bank.Areas.Identity.Controllers;
using Bulgaria_Bank.Areas.Identity.Models;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Data.Models.Identity;
using Bulgaria_Bank.Services.Data.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Bulgaria_Bank.UnitTest.Areas.Identity.Controllers.AccountControllerTests
{
    [TestClass]
    public class LoginAction_Should
    {
        [TestMethod]
        public async Task Redirects_To_Other_Page_If_User_Is_Logged_In()
        {
            var identity = new Mock<IIdentity>();
            identity.SetupGet(i => i.IsAuthenticated).Returns(true);

            var mockPrincipal = new Mock<ClaimsPrincipal>();
            mockPrincipal.Setup(x => x.Identity).Returns(identity.Object);

            var mockHttpContext = new Mock<HttpContext>();
            mockHttpContext.Setup(m => m.User).Returns(mockPrincipal.Object);

            Mock<IBannerService> bannerServiceMock = new Mock<IBannerService>();
            Mock<IUserService> userServiceMock = new Mock<IUserService>();
            Mock<IMapper> mapperMock = new Mock<IMapper>();
            Mock<IClaimService> claimServiceMock = new Mock<IClaimService>();
            Mock<IMemoryCache> memoryCacheMock = new Mock<IMemoryCache>();

            AccountController sut = new AccountController(userServiceMock.Object, claimServiceMock.Object, bannerServiceMock.Object, mapperMock.Object, memoryCacheMock.Object);

            sut.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = mockPrincipal.Object }
            };

            var result = await sut.Login();

            var redirectToActionResult = result as RedirectToActionResult;

            Assert.IsTrue(result is RedirectToActionResult);
            Assert.IsTrue(redirectToActionResult != null);
            Assert.AreEqual(redirectToActionResult.ActionName, "Dashboard");
        }
    }
}
