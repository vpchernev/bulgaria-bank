﻿using System;

namespace Bulgaria_Bank.Data.Models.Contracts
{
    public interface IEntity
    {
        string Id { get; set; }
    }
}
