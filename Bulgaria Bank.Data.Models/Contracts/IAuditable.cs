﻿using System;

namespace Bulgaria_Bank.Data.Models.Contracts
{
    public interface IAuditable
    {
        DateTime? CreatedOn { get; set; }
    }
}
