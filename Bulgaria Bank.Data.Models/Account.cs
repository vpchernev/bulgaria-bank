﻿using Bulgaria_Bank.Data.Models.Abstract;
using Bulgaria_Bank.Data.Models.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Bulgaria_Bank.Data.Models
{
    public class Account : BaseEntity
    {
        public Account()
        {
            this.SenderTransactions = new List<Transaction>();
            this.ReceiverTransactions = new List<Transaction>();
            this.UserAccounts = new List<UserAccount>();
        }

        [Required]
        public string AccountNumber { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Ballance must be positive number")]
        public decimal CurrentBalance { get; set; }

        public string ClientId { get; set; }

        [Required]
        public Client Client { get; set; }

        public ICollection<Transaction> SenderTransactions { get; set; }

        public ICollection<Transaction> ReceiverTransactions { get; set; }
        
        public ICollection<UserAccount> UserAccounts { get; set; }
    }
}
