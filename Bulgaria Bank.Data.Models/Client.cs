﻿using Bulgaria_Bank.Data.Models.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Bulgaria_Bank.Data.Models
{
    public class Client : BaseEntity
    {
        public Client()
        {
            this.Accounts = new List<Account>();
        }

        [Required(ErrorMessage = "Please enter name of client.")]
        [StringLength(35)]
        public string Name { get; set; }

        public ICollection<Account> Accounts { get; set; }
    }
}