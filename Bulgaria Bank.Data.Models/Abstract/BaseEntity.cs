﻿using Bulgaria_Bank.Data.Models.Contracts;
using System;
using System.ComponentModel.DataAnnotations;

namespace Bulgaria_Bank.Data.Models.Abstract
{
    public abstract class BaseEntity : IEntity, IAuditable
    {
        [Key]
        public string Id { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? CreatedOn { get; set; }
    }
}
