﻿using Bulgaria_Bank.Data.Models.Abstract;
using Bulgaria_Bank.Data.Models.Common;
using Bulgaria_Bank.Data.Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Bulgaria_Bank.Data.Models
{
    public class Transaction : BaseEntity
    {
        [Required(ErrorMessage ="Sender's account cannot be empty.")]
        public string SenderId { get; set; }

        public Account SenderAccount { get; set; }

        [Required(ErrorMessage ="Receiver's account field cannot be empty.")]
        public string ReceiverId { get; set; }

        public Account ReceiverAccount { get; set; }

        [Required(ErrorMessage ="Description field cannot be empty.")]
        [StringLength(35,MinimumLength =1,ErrorMessage ="Description should be between 1 and 35 characters long.")]
        public string Description { get; set; }

        [Required(ErrorMessage ="Amount field cannot be empty.")]
        [Range(0.1,15000d,ErrorMessage ="Minimum amount is 0,10 and maximum is 15 000,00 for one transaction.")]
        public decimal Amount { get; set; }

        public TransactionStatus Status { get; set; }
    }
}
