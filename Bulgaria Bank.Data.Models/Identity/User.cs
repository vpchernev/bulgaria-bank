﻿using Bulgaria_Bank.Data.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Bulgaria_Bank.Data.Models.Identity
{
    public class User : BaseEntity
    {
        public User()
        {
            this.UserAccounts = new List<UserAccount>();
        }

        [Required]
        [StringLength(16, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 4)]
        public string Username { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 2)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 2)]
        public string LastName { get; set; }

        public string PasswordHash { get; set; }

        public string RoleId { get; set; }

        public Role Role { get; set; }

        public ICollection<UserAccount> UserAccounts { get; set; }
    }
}
