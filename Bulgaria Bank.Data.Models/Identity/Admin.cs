﻿using Bulgaria_Bank.Data.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Bulgaria_Bank.Data.Models.Identity
{
    public class Admin : BaseEntity
    {
        [Required]
        [StringLength(16, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 4)]
        public string Username { get; set; }

        [Required]
        public string PasswordHash { get; set; }

        public string RoleId { get; set; }

        public Role Role { get; set; }
    }
}
