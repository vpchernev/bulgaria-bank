﻿using Bulgaria_Bank.Data.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bulgaria_Bank.Data.Models.Identity
{
    public class Role : BaseEntity
    {
        public Role()
        {
            this.Users = new List<User>();
            this.Admins = new List<Admin>();
        }

        public string Name { get; set; }

        public ICollection<User> Users { get; set; }

        public ICollection<Admin> Admins { get; set; }

    }
}
