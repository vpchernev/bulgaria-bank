﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bulgaria_Bank.Data.Models.Common
{
    public enum TransactionStatus
    {
        Saved,
        Sent
    }
}
