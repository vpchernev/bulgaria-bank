﻿using Bulgaria_Bank.Data.Models.Abstract;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Bulgaria_Bank.Data.Models
{
    public class Banner : BaseEntity
    {
        public string ImagePath { get; set; }
        [Required]
        [StringLength(35)]
        public string BannerName { get; set; }

        [Required]
        [Url]
        public string LinkUrl { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }
    }
}
