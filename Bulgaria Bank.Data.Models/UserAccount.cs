﻿using Bulgaria_Bank.Data.Models.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bulgaria_Bank.Data.Models
{
    public class UserAccount
    {
        public string UserId { get; set; }
        
        public string AccountId { get; set; }

        public User User { get; set; }

        public Account Account { get; set; }

        public string Nickname { get; set; }
    }
}
