﻿using Bulgaria_Bank.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bulgaria_Bank.Data.Configurations
{
    public class TransactionConfiguration : IEntityTypeConfiguration<Transaction>
    {
        public void Configure(EntityTypeBuilder<Transaction> builder)
        {
            builder
                    .HasOne(x => x.SenderAccount)
                    .WithMany(x => x.SenderTransactions)
                    .HasForeignKey(x => x.SenderId)
                    .OnDelete(deleteBehavior: 0);

            builder
                    .HasOne(x => x.ReceiverAccount)
                    .WithMany(x => x.ReceiverTransactions)
                    .HasForeignKey(x => x.ReceiverId)
                    .OnDelete(deleteBehavior: 0);
        }
    }
}
