﻿using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Data.Models.Identity;
using Bulgaria_Bank.Data.Utils;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.IO;

namespace Bulgaria_Bank.Data
{
    public static class LoadJson
    {
        public static void Load(ModelBuilder builder)
        {
            var adminsPath = @"..\Bulgaria Bank.Data\Data\Admins.json";
            var accountsPath = @"..\Bulgaria Bank.Data\Data\Accounts.json";
            var bannersPath = @"..\Bulgaria Bank.Data\Data\Banners.json";
            var clientsPath = @"..\Bulgaria Bank.Data\Data\Clients.json";
            var transactionsPath = @"..\Bulgaria Bank.Data\Data\Transactions.json";
            var userAccountPath = @"..\Bulgaria Bank.Data\Data\UserAccount.json";
            var rolesPath = @"..\Bulgaria Bank.Data\Data\Roles.json";
            var usersPath = @"..\Bulgaria Bank.Data\Data\Users.json";

            try
            {
                var adminsJson = File.ReadAllText(adminsPath);
                var accountsJson = File.ReadAllText(accountsPath);
                var bannersJson = File.ReadAllText(bannersPath);
                var clientsJson = File.ReadAllText(clientsPath);
                var transactionsJson = File.ReadAllText(transactionsPath);
                var userAccountJson = File.ReadAllText(userAccountPath);
                var rolesJson = File.ReadAllText(rolesPath);
                var usersJson = File.ReadAllText(usersPath);

                var format = "dd/MM/yyyy";
                var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };

                var admins = JsonConvert.DeserializeObject<Admin[]>(adminsJson, dateTimeConverter);
                var accounts = JsonConvert.DeserializeObject<Account[]>(accountsJson, dateTimeConverter);
                var banners = JsonConvert.DeserializeObject<Banner[]>(bannersJson, dateTimeConverter);
                var clients = JsonConvert.DeserializeObject<Client[]>(clientsJson, dateTimeConverter);
                var transactions = JsonConvert.DeserializeObject<Transaction[]>(transactionsJson, dateTimeConverter);
                var userAccounts = JsonConvert.DeserializeObject<UserAccount[]>(userAccountJson, dateTimeConverter);
                var roles = JsonConvert.DeserializeObject<Role[]>(rolesJson, dateTimeConverter);
                var users = JsonConvert.DeserializeObject<User[]>(usersJson, dateTimeConverter);

                admins = HashAdminsPassword(admins);
                users = HashUsersPassword(users);

                builder.Entity<Admin>().HasData(admins);
                builder.Entity<Account>().HasData(accounts);
                builder.Entity<Banner>().HasData(banners);
                builder.Entity<Client>().HasData(clients);
                builder.Entity<Transaction>().HasData(transactions);
                builder.Entity<UserAccount>().HasData(userAccounts);
                builder.Entity<Role>().HasData(roles);
                builder.Entity<User>().HasData(users);
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }
        }

        private static User[] HashUsersPassword(User[] users)
        {

            foreach (var user in users)
            {
                user.PasswordHash = StaticCryptoWrapper.HashPassword(user.PasswordHash);
            }

            return users;

        }

        private static Admin[] HashAdminsPassword(Admin[] admins)
        {

            foreach (var admin in admins)
            {
                admin.PasswordHash = StaticCryptoWrapper.HashPassword(admin.PasswordHash);
            }

            return admins;

        }
    }
}
