﻿using Bulgaria_Bank.Data.Configurations;
using Bulgaria_Bank.Data.Models;
using Bulgaria_Bank.Data.Models.Identity;
using Microsoft.EntityFrameworkCore;

namespace Bulgaria_Bank.Data.Context
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext()
        {

        }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {

        }
        
        public virtual DbSet<User> Users { get; set; }

        public virtual DbSet<Admin> Admins { get; set; }

        public virtual DbSet<Role> Roles { get; set; }

        public virtual DbSet<Account> Accounts { get; set; }

        public virtual DbSet<Banner> Banners { get; set; }

        public virtual DbSet<Client> Clients { get; set; }

        public virtual DbSet<Transaction> Transactions { get; set; }

        public virtual DbSet<UserAccount> UserAccounts { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new UserAccountConfiguration());
            builder.ApplyConfiguration(new ClientConfiguration());
            builder.ApplyConfiguration(new TransactionConfiguration());

            LoadJson.Load(builder);

            base.OnModelCreating(builder);
        }

    }
}
