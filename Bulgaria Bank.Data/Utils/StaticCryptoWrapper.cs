﻿using System.Web.Helpers;

namespace Bulgaria_Bank.Data.Utils
{
    internal static class StaticCryptoWrapper
    {
        public static string HashPassword(string password)
        {
            return Crypto.HashPassword(password);
        }
    }
}
